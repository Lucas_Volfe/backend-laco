FROM debian:volfe

MAINTAINER Cezar Junior de Souza <cezar08@unochapeco.edu.br>

RUN apt-get update
RUN apt-get -y install apt-transport-https lsb-release ca-certificates wget
RUN wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
RUN echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list
RUN apt-get update
RUN apt-get -y upgrade

# Install Apache2 / PHP 7.3 & Co.
RUN apt-get -y install apache2 git php7.3 libapache2-mod-php7.3 sqlite3 php7.3-sqlite php7.3-dev php7.3-curl php7.3-pgsql memcached php7.3-memcached mcrypt php7.3-intl php7.3-mbstring php7.3-xml zip php7.3-zip curl libaio1 

# Install the Oracle Instant Client
ADD docker/oracle/oracle-instantclient12.1-basic_12.1.0.2.0-2_amd64.deb /tmp
ADD docker/oracle/oracle-instantclient12.1-devel_12.1.0.2.0-2_amd64.deb /tmp
ADD docker/oracle/oracle-instantclient12.1-sqlplus_12.1.0.2.0-2_amd64.deb /tmp
RUN dpkg -i /tmp/oracle-instantclient12.1-basic_12.1.0.2.0-2_amd64.deb
RUN dpkg -i /tmp/oracle-instantclient12.1-devel_12.1.0.2.0-2_amd64.deb
RUN dpkg -i /tmp/oracle-instantclient12.1-sqlplus_12.1.0.2.0-2_amd64.deb
RUN rm -rf /tmp/oracle-instantclient12.1-*.deb

# Set up the Oracle environment variables
ENV LD_LIBRARY_PATH /usr/lib/oracle/12.1/client64/lib/
ENV ORACLE_HOME /usr/lib/oracle/12.1/client64/lib/

# Install the OCI8 PHP extension
RUN echo 'instantclient,/usr/lib/oracle/12.1/client64/lib' | pecl install -f oci8
RUN echo "extension=oci8.so" > /etc/php/7.3/apache2/conf.d/30-oci8.ini

# Enable Apache2 modules
ADD docker/sample/000-default.conf /tmp
RUN cp /tmp/000-default.conf /etc/apache2/sites-available/
RUN a2enmod rewrite

# Set up the Apache2 environment variables
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid

#SET ROOT DIRECTORY
#RUN sed -i 's!/var/www/html!/var/www/public!g' /etc/apache2/sites-available/000-default.conf
RUN mv /var/www/html /var/www/public

EXPOSE 80
#RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

# Run Apache2 in Foreground
RUN service apache2 restart
CMD /usr/sbin/apache2 -D FOREGROUND

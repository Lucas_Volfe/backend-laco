<?php
return [
    'service_manager' => [
        'factories' => [
            \TablesAPI\V1\Rest\ContabilidadeTable\ContabilidadeTableResource::class => \TablesAPI\V1\Rest\ContabilidadeTable\ContabilidadeTableResourceFactory::class,
            \TablesAPI\V1\Rest\TodasEmpresasPorContabilidade\TodasEmpresasPorContabilidadeResource::class => \TablesAPI\V1\Rest\TodasEmpresasPorContabilidade\TodasEmpresasPorContabilidadeResourceFactory::class,
            \TablesAPI\V1\Rest\UmRegistroEmpresaPorContabilidade\UmRegistroEmpresaPorContabilidadeResource::class => \TablesAPI\V1\Rest\UmRegistroEmpresaPorContabilidade\UmRegistroEmpresaPorContabilidadeResourceFactory::class,
            \TablesAPI\V1\Rest\UmRegistroUploadPorEmpresa\UmRegistroUploadPorEmpresaResource::class => \TablesAPI\V1\Rest\UmRegistroUploadPorEmpresa\UmRegistroUploadPorEmpresaResourceFactory::class,
            \TablesAPI\V1\Rest\DashboardsAdmin\DashboardsAdminResource::class => \TablesAPI\V1\Rest\DashboardsAdmin\DashboardsAdminResourceFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'tables-api.rest.contabilidade-table' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/contabilidade-table[/:contabilidade_table_id]',
                    'defaults' => [
                        'controller' => 'TablesAPI\\V1\\Rest\\ContabilidadeTable\\Controller',
                    ],
                ],
            ],
            'tables-api.rest.todas-empresas-por-contabilidade' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/todas-empresas-por-contabilidade[/:todas_empresas_por_contabilidade_id]',
                    'defaults' => [
                        'controller' => 'TablesAPI\\V1\\Rest\\TodasEmpresasPorContabilidade\\Controller',
                    ],
                ],
            ],
            'tables-api.rest.um-registro-empresa-por-contabilidade' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/um-registro-empresa-por-contabilidade[/:um_registro_empresa_por_contabilidade_id]',
                    'defaults' => [
                        'controller' => 'TablesAPI\\V1\\Rest\\UmRegistroEmpresaPorContabilidade\\Controller',
                    ],
                ],
            ],
            'tables-api.rest.um-registro-upload-por-empresa' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/um-registro-upload-por-empresa[/:um_registro_upload_por_empresa_id]',
                    'defaults' => [
                        'controller' => 'TablesAPI\\V1\\Rest\\UmRegistroUploadPorEmpresa\\Controller',
                    ],
                ],
            ],
            'tables-api.rest.dashboards-admin' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/dashboards-admin[/:dashboards_admin_id]',
                    'defaults' => [
                        'controller' => 'TablesAPI\\V1\\Rest\\DashboardsAdmin\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'zf-versioning' => [
        'uri' => [
            0 => 'tables-api.rest.contabilidade-table',
            1 => 'tables-api.rest.todas-empresas-por-contabilidade',
            2 => 'tables-api.rest.um-registro-empresa-por-contabilidade',
            3 => 'tables-api.rest.um-registro-upload-por-empresa',
            4 => 'tables-api.rest.dashboards-admin',
        ],
    ],
    'zf-rest' => [
        'TablesAPI\\V1\\Rest\\ContabilidadeTable\\Controller' => [
            'listener' => \TablesAPI\V1\Rest\ContabilidadeTable\ContabilidadeTableResource::class,
            'route_name' => 'tables-api.rest.contabilidade-table',
            'route_identifier_name' => 'contabilidade_table_id',
            'collection_name' => 'contabilidade_table',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \TablesAPI\V1\Rest\ContabilidadeTable\ContabilidadeTableEntity::class,
            'collection_class' => \TablesAPI\V1\Rest\ContabilidadeTable\ContabilidadeTableCollection::class,
            'service_name' => 'ContabilidadeTable',
        ],
        'TablesAPI\\V1\\Rest\\TodasEmpresasPorContabilidade\\Controller' => [
            'listener' => \TablesAPI\V1\Rest\TodasEmpresasPorContabilidade\TodasEmpresasPorContabilidadeResource::class,
            'route_name' => 'tables-api.rest.todas-empresas-por-contabilidade',
            'route_identifier_name' => 'todas_empresas_por_contabilidade_id',
            'collection_name' => 'todas_empresas_por_contabilidade',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \TablesAPI\V1\Rest\TodasEmpresasPorContabilidade\TodasEmpresasPorContabilidadeEntity::class,
            'collection_class' => \TablesAPI\V1\Rest\TodasEmpresasPorContabilidade\TodasEmpresasPorContabilidadeCollection::class,
            'service_name' => 'TodasEmpresasPorContabilidade',
        ],
        'TablesAPI\\V1\\Rest\\UmRegistroEmpresaPorContabilidade\\Controller' => [
            'listener' => \TablesAPI\V1\Rest\UmRegistroEmpresaPorContabilidade\UmRegistroEmpresaPorContabilidadeResource::class,
            'route_name' => 'tables-api.rest.um-registro-empresa-por-contabilidade',
            'route_identifier_name' => 'um_registro_empresa_por_contabilidade_id',
            'collection_name' => 'um_registro_empresa_por_contabilidade',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \TablesAPI\V1\Rest\UmRegistroEmpresaPorContabilidade\UmRegistroEmpresaPorContabilidadeEntity::class,
            'collection_class' => \TablesAPI\V1\Rest\UmRegistroEmpresaPorContabilidade\UmRegistroEmpresaPorContabilidadeCollection::class,
            'service_name' => 'UmRegistroEmpresaPorContabilidade',
        ],
        'TablesAPI\\V1\\Rest\\UmRegistroUploadPorEmpresa\\Controller' => [
            'listener' => \TablesAPI\V1\Rest\UmRegistroUploadPorEmpresa\UmRegistroUploadPorEmpresaResource::class,
            'route_name' => 'tables-api.rest.um-registro-upload-por-empresa',
            'route_identifier_name' => 'um_registro_upload_por_empresa_id',
            'collection_name' => 'um_registro_upload_por_empresa',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \TablesAPI\V1\Rest\UmRegistroUploadPorEmpresa\UmRegistroUploadPorEmpresaEntity::class,
            'collection_class' => \TablesAPI\V1\Rest\UmRegistroUploadPorEmpresa\UmRegistroUploadPorEmpresaCollection::class,
            'service_name' => 'UmRegistroUploadPorEmpresa',
        ],
        'TablesAPI\\V1\\Rest\\DashboardsAdmin\\Controller' => [
            'listener' => \TablesAPI\V1\Rest\DashboardsAdmin\DashboardsAdminResource::class,
            'route_name' => 'tables-api.rest.dashboards-admin',
            'route_identifier_name' => 'dashboards_admin_id',
            'collection_name' => 'dashboards_admin',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \TablesAPI\V1\Rest\DashboardsAdmin\DashboardsAdminEntity::class,
            'collection_class' => \TablesAPI\V1\Rest\DashboardsAdmin\DashboardsAdminCollection::class,
            'service_name' => 'DashboardsAdmin',
        ],
    ],
    'zf-content-negotiation' => [
        'controllers' => [
            'TablesAPI\\V1\\Rest\\ContabilidadeTable\\Controller' => 'Json',
            'TablesAPI\\V1\\Rest\\TodasEmpresasPorContabilidade\\Controller' => 'Json',
            'TablesAPI\\V1\\Rest\\UmRegistroEmpresaPorContabilidade\\Controller' => 'Json',
            'TablesAPI\\V1\\Rest\\UmRegistroUploadPorEmpresa\\Controller' => 'Json',
            'TablesAPI\\V1\\Rest\\DashboardsAdmin\\Controller' => 'Json',
        ],
        'accept_whitelist' => [
            'TablesAPI\\V1\\Rest\\ContabilidadeTable\\Controller' => [
                0 => 'application/vnd.tables-api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'TablesAPI\\V1\\Rest\\TodasEmpresasPorContabilidade\\Controller' => [
                0 => 'application/vnd.tables-api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'TablesAPI\\V1\\Rest\\UmRegistroEmpresaPorContabilidade\\Controller' => [
                0 => 'application/vnd.tables-api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'TablesAPI\\V1\\Rest\\UmRegistroUploadPorEmpresa\\Controller' => [
                0 => 'application/vnd.tables-api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'TablesAPI\\V1\\Rest\\DashboardsAdmin\\Controller' => [
                0 => 'application/vnd.tables-api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'TablesAPI\\V1\\Rest\\ContabilidadeTable\\Controller' => [
                0 => 'application/vnd.tables-api.v1+json',
                1 => 'application/json',
            ],
            'TablesAPI\\V1\\Rest\\TodasEmpresasPorContabilidade\\Controller' => [
                0 => 'application/vnd.tables-api.v1+json',
                1 => 'application/json',
            ],
            'TablesAPI\\V1\\Rest\\UmRegistroEmpresaPorContabilidade\\Controller' => [
                0 => 'application/vnd.tables-api.v1+json',
                1 => 'application/json',
            ],
            'TablesAPI\\V1\\Rest\\UmRegistroUploadPorEmpresa\\Controller' => [
                0 => 'application/vnd.tables-api.v1+json',
                1 => 'application/json',
            ],
            'TablesAPI\\V1\\Rest\\DashboardsAdmin\\Controller' => [
                0 => 'application/vnd.tables-api.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'zf-hal' => [
        'metadata_map' => [
            \TablesAPI\V1\Rest\ContabilidadeTable\ContabilidadeTableEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'tables-api.rest.contabilidade-table',
                'route_identifier_name' => 'contabilidade_table_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \TablesAPI\V1\Rest\ContabilidadeTable\ContabilidadeTableCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'tables-api.rest.contabilidade-table',
                'route_identifier_name' => 'contabilidade_table_id',
                'is_collection' => true,
            ],
            \TablesAPI\V1\Rest\TodasEmpresasPorContabilidade\TodasEmpresasPorContabilidadeEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'tables-api.rest.todas-empresas-por-contabilidade',
                'route_identifier_name' => 'todas_empresas_por_contabilidade_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \TablesAPI\V1\Rest\TodasEmpresasPorContabilidade\TodasEmpresasPorContabilidadeCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'tables-api.rest.todas-empresas-por-contabilidade',
                'route_identifier_name' => 'todas_empresas_por_contabilidade_id',
                'is_collection' => true,
            ],
            \TablesAPI\V1\Rest\UmRegistroEmpresaPorContabilidade\UmRegistroEmpresaPorContabilidadeEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'tables-api.rest.um-registro-empresa-por-contabilidade',
                'route_identifier_name' => 'um_registro_empresa_por_contabilidade_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \TablesAPI\V1\Rest\UmRegistroEmpresaPorContabilidade\UmRegistroEmpresaPorContabilidadeCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'tables-api.rest.um-registro-empresa-por-contabilidade',
                'route_identifier_name' => 'um_registro_empresa_por_contabilidade_id',
                'is_collection' => true,
            ],
            \TablesAPI\V1\Rest\UmRegistroUploadPorEmpresa\UmRegistroUploadPorEmpresaEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'tables-api.rest.um-registro-upload-por-empresa',
                'route_identifier_name' => 'um_registro_upload_por_empresa_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \TablesAPI\V1\Rest\UmRegistroUploadPorEmpresa\UmRegistroUploadPorEmpresaCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'tables-api.rest.um-registro-upload-por-empresa',
                'route_identifier_name' => 'um_registro_upload_por_empresa_id',
                'is_collection' => true,
            ],
            \TablesAPI\V1\Rest\DashboardsAdmin\DashboardsAdminEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'tables-api.rest.dashboards-admin',
                'route_identifier_name' => 'dashboards_admin_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \TablesAPI\V1\Rest\DashboardsAdmin\DashboardsAdminCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'tables-api.rest.dashboards-admin',
                'route_identifier_name' => 'dashboards_admin_id',
                'is_collection' => true,
            ],
        ],
    ],
];

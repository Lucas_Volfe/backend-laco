<?php
namespace TablesAPI\V1\Rest\UmRegistroEmpresaPorContabilidade;

class UmRegistroEmpresaPorContabilidadeResourceFactory
{
    public function __invoke($services)
    {
        return new UmRegistroEmpresaPorContabilidadeResource($services->get('UmRegistroEmpresaPorContabilidadeService'));
    }
}

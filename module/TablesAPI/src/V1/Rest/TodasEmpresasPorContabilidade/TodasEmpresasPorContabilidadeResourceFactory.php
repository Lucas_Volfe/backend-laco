<?php
namespace TablesAPI\V1\Rest\TodasEmpresasPorContabilidade;

class TodasEmpresasPorContabilidadeResourceFactory
{
    public function __invoke($services)
    {
        return new TodasEmpresasPorContabilidadeResource($services->get('TodasEmpresasPorContabilidadeService'));
    }
}

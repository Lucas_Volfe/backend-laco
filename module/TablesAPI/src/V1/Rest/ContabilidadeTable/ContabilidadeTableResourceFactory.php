<?php
namespace TablesAPI\V1\Rest\ContabilidadeTable;

class ContabilidadeTableResourceFactory
{
    public function __invoke($services)
    {
        return new ContabilidadeTableResource($services->get('ContabilidadeTablesService'));
    }
}

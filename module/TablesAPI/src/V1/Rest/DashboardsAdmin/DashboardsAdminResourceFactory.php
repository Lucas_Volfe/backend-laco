<?php
namespace TablesAPI\V1\Rest\DashboardsAdmin;

class DashboardsAdminResourceFactory
{
    public function __invoke($services)
    {
        return new DashboardsAdminResource($services->get('DashboardsAdminService'));
    }
}

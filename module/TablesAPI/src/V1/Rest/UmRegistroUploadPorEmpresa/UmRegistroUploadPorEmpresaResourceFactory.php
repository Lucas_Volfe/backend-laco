<?php
namespace TablesAPI\V1\Rest\UmRegistroUploadPorEmpresa;

class UmRegistroUploadPorEmpresaResourceFactory
{
    public function __invoke($services)
    {
        return new UmRegistroUploadPorEmpresaResource($services->get('UmRegistroUploadPorEmpresaService'));
    }
}

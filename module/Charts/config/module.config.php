<?php

namespace Charts;

use Charts\Service\TodosValoresChartsService;
use Charts\Service\CardsDashboardsService;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'doctrine' => [
        'driver' => [
            'charts_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'Charts\Entity' => 'charts_entities'
                ]
            ]
        ]
    ],
    'service_manager' => [
        'factories' => [        
            'TodosValoresChartsService' => function ($sm) {
                return new TodosValoresChartsService($sm->get('Doctrine\ORM\EntityManager'));
            },
            'CardsDashboardsService' => function ($sm) {
                return new CardsDashboardsService($sm->get('Doctrine\ORM\EntityManager'));
            }
        ]
    ]
];

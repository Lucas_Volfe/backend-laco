<?php
/**
 * Created by PhpStorm.
 * User: 
 * Date: 
 * Time: 
 */

namespace Charts\Service;

use Charts\Entity\ObjtSerie;
use Charts\Entity\ConjuntoObjtSerie;
use Upload\Entity\Uploads;
use Charts\Entity\LucroLiquidoMensal;
use Doctrine\ORM\EntityManager;

class TodosValoresChartsService
{
    const ENTITY = 'Upload\Entity\Uploads';
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function save($data)
    {
        $chartItem = $this->em->find(self::ENTITY, (int) $data['id']);
       
        if (! $chartItem) {
            $chartItem = new LucroLiquidoMensal();
        }

        $chartItem->setData($data);
        $this->em->persist($chartItem);
        $this->em->flush();

    }

    public function fetch($empresa)
    {

        $emp = $this->em->find('Cadastros\Entity\Empresa', (int) $empresa);

        if(!$emp || $emp->tem_upload == 0 )
            return [null];// ['Nenhum registro encontrado'];


        $select = $this->em->createQueryBuilder()->select(
            'Uploads.id',
            'Uploads.Mes',
            'Uploads.Ano',
            'Uploads.Lucro_Prejuizo_Liquido',
            'Uploads.Receitas_Operacionais',

            'Uploads.Deducoes_Da_Receita_Bruta',
            'Uploads.Custos_Operacionais',
            'Uploads.Despesas_Operacionais',
            'Uploads.Despesas_Financeiras',
            'Uploads.Desembolso_Com_Investimentos_E_Emprestimos',

            'Uploads.Impostos_Sobre_Vendas',
            'Uploads.Comissoes_Sobre_Vendas',
            'Uploads.Devolucoes_De_Vendas',
            'Uploads.Outras_Deducoes_1',

        )->from(self::ENTITY, 'Uploads')
        ->where("Uploads.Empresa = $empresa ");    

        $resultados = $select->getQuery()->getArrayResult();               
        // $conjunto_valores_gerar_dashboards['meses'] = $this->constroiArrayMeses($resultados);
        // $conjunto_valores_gerar_dashboards['LucroLiquidoMensal'] = $this->advancedPieChart_LucroLiquidoMensal($resultados);
        // $conjunto_valores_gerar_dashboards['LucroLiquidoAcumulado'] = $this->advancedPieChart_LucroLiquidoAcumulado($resultados);
        // $conjunto_valores_gerar_dashboards['GastosTotais'] = $this->PieChart_GastosTotais($resultados);
        // $conjunto_valores_gerar_dashboards['DeducaoDaReceita'] = $this->PieChart_DeducaoDaReceita($resultados);
        // $conjunto_valores_gerar_dashboards['ReceitasOperacionais'] = $this->VerticalBar_ReceitasOperacionais($resultados);
        // $conjunto_valores_gerar_dashboards['PesoCustoDespesa'] = $this->LineChart_PesoCustoDespesa($resultados);
        $conjunto_valores_gerar_dashboards['mensal']['meses'] = $this->constroiArrayMeses($resultados);
        $conjunto_valores_gerar_dashboards['mensal']['LucroLiquidoMensal'] = $this->advancedPieChart_LucroLiquidoMensal($resultados);
        $conjunto_valores_gerar_dashboards['mensal']['LucroLiquidoAcumulado'] = $this->advancedPieChart_LucroLiquidoAcumulado($resultados);
        $conjunto_valores_gerar_dashboards['mensal']['GastosTotais'] = $this->PieChart_GastosTotais($resultados);
        $conjunto_valores_gerar_dashboards['mensal']['DeducaoDaReceita'] = $this->PieChart_DeducaoDaReceita($resultados);
        $conjunto_valores_gerar_dashboards['mensal']['ReceitasOperacionais'] = $this->VerticalBar_ReceitasOperacionais($resultados);
        $conjunto_valores_gerar_dashboards['mensal']['PesoCustoDespesa'] = $this->LineChart_PesoCustoDespesa($resultados);

        // var_dump($resultados);die;
        $tri_sem = $this->separar_conjunto_por_periodo_de_visualizacao($resultados);

        $conjunto_valores_gerar_dashboards['trimestres'] = $tri_sem['trimestres'];
        $conjunto_valores_gerar_dashboards['semestres'] = $tri_sem['semestres'];

        return $conjunto_valores_gerar_dashboards;
    }

    public function constroiArrayMeses($resultados)
    {
        $meses = array();
        foreach( $resultados as $key => $resultado )
        {
            $meses[] = [
                'id' => $key,
                'mes' => $resultado['Mes'] . '/' . $resultado['Ano']
            ];
        }

        return $meses;
    }

    public function advancedPieChart_LucroLiquidoMensal($resultados)
    {        
        $valores_mensais = array();
        foreach ($resultados as $key => $resultado) {
            $label = $this->selectLabel($resultado); 

            if( $resultado['Lucro_Prejuizo_Liquido'] != null && $resultado['Receitas_Operacionais'] != null ){
                $valor_porcent = round(($resultado['Lucro_Prejuizo_Liquido'] / $resultado['Receitas_Operacionais']), 2 );
                $concat = array();
                $concat[] = new ObjtSerie( $label, $valor_porcent );
                $concat[] = new ObjtSerie( 'Total', 100 - $valor_porcent);
                $valores_mensais[$key] = $concat; 
            }
        }
        return $valores_mensais;
    }

    public function advancedPieChart_LucroLiquidoAcumulado($resultados)
    {
        $total_receitas = 0;
        $total_luc_prj_liquido = 0;        
        foreach ($resultados as $resultado) {
            $total_receitas += $resultado['Receitas_Operacionais'];
            $total_luc_prj_liquido += $resultado['Lucro_Prejuizo_Liquido'];
        }        
        $valor = round( ($total_luc_prj_liquido / $total_receitas), 2);
        $resultado_final = array();
        $resultado_final[] = new ObjtSerie('Lucro Líquido Acumulado', $valor);
        $resultado_final[] = new ObjtSerie('Total', 100 - $valor);
        return $resultado_final;
    }

    public function PieChart_GastosTotais($resultados)
    {

        $conjuntoCompleto = array();
        foreach ($resultados as $resultado) 
        {
            $conjuntoMensal = array();
            $conjuntoMensal[] = new ObjtSerie( 'Deduções da Receita Bruta' , ($resultado['Deducoes_Da_Receita_Bruta'] > 0) ? $resultado['Deducoes_Da_Receita_Bruta'] : $resultado['Deducoes_Da_Receita_Bruta'] * -1 );
            $conjuntoMensal[] = new ObjtSerie( 'Custos Operacionais' , ($resultado['Custos_Operacionais'] > 0) ? $resultado['Custos_Operacionais'] : $resultado['Custos_Operacionais'] * -1 );
            $conjuntoMensal[] = new ObjtSerie( 'Despesas Operacionais' , ($resultado['Despesas_Operacionais'] > 0) ? $resultado['Despesas_Operacionais'] : $resultado['Despesas_Operacionais'] * -1 );
            $conjuntoMensal[] = new ObjtSerie( 'Despesas Financeiras' , ($resultado['Despesas_Financeiras'] > 0) ? $resultado['Despesas_Financeiras'] : $resultado['Despesas_Financeiras'] * -1 );
            $conjuntoMensal[] = new ObjtSerie( 'Despesas com Investimentos e Empréstimos' , ($resultado['Desembolso_Com_Investimentos_E_Emprestimos'] > 0) ? $resultado['Desembolso_Com_Investimentos_E_Emprestimos'] : $resultado['Desembolso_Com_Investimentos_E_Emprestimos'] * -1 );
            $conjuntoCompleto[] = $conjuntoMensal;
        }
        return $conjuntoCompleto;
    }

    public function PieChart_DeducaoDaReceita($resultados)
    {
        $conjuntoCompleto = array();
        foreach ($resultados as $resultado) 
        {
            $conjuntoMensal = array();
            $conjuntoMensal[] = new ObjtSerie( 'Impostos Sobre Vendas' , ($resultado['Impostos_Sobre_Vendas'] > 0) ? $resultado['Impostos_Sobre_Vendas'] : $resultado['Impostos_Sobre_Vendas'] * -1 );
            $conjuntoMensal[] = new ObjtSerie( 'Comissões Sobre Vendas' , ($resultado['Comissoes_Sobre_Vendas'] > 0) ? $resultado['Comissoes_Sobre_Vendas'] : $resultado['Comissoes_Sobre_Vendas'] * -1 );
            $conjuntoMensal[] = new ObjtSerie( 'Devoluções de Vendas' , ($resultado['Devolucoes_De_Vendas'] > 0) ? $resultado['Devolucoes_De_Vendas'] : $resultado['Devolucoes_De_Vendas'] * -1 );
            $conjuntoMensal[] = new ObjtSerie( 'Deduções 1' , ($resultado['Outras_Deducoes_1'] > 0) ? $resultado['Outras_Deducoes_1'] : $resultado['Outras_Deducoes_1'] * -1 );

            $conjuntoCompleto[] = $conjuntoMensal;
        }
        return $conjuntoCompleto;
    }

    public function VerticalBar_ReceitasOperacionais($resultados)
    {
        $conjuntoCompleto = array();
        foreach ($resultados as $resultado) 
        {
            $label = $this->selectLabel($resultado); 
            $conjuntoCompleto[] = new ObjtSerie( $label , ($resultado['Receitas_Operacionais'] > 0) ? $resultado['Receitas_Operacionais'] : $resultado['Receitas_Operacionais'] * -1 );
        }
        return $conjuntoCompleto;
    }

    public function LineChart_PesoCustoDespesa($resultados)
    {
        $conjuntoCompleto = array();
        $conjuntoCusto = array();
        $conjuntoDespesas = array();
        foreach ($resultados as $resultado) 
        {
            $label = $this->selectLabel($resultado); 

            $somaCusto = round($resultado['Custos_Operacionais'] / $resultado['Receitas_Operacionais'], 4);
            $somaDespesa = round($resultado['Despesas_Operacionais'] / $resultado['Receitas_Operacionais'], 4);
      
            $conjuntoCusto[] = new ObjtSerie( $label , ( $somaCusto > 0) ? round($somaCusto * 100,2) : round($somaCusto * -100,2) );
            $conjuntoDespesas[] = new ObjtSerie( $label , ( $somaDespesa > 0) ? round($somaDespesa * 100,2) : round($somaDespesa * -100,2) );
          
        }

        //var_dump($conjuntoCusto);die;
        $conjuntoCompleto[] = new ConjuntoObjtSerie('Peso Custo', $conjuntoCusto );      
        $conjuntoCompleto[] = new ConjuntoObjtSerie('Peso Despesa', $conjuntoDespesas ); 
        return $conjuntoCompleto;
    }

    public function separar_conjunto_por_periodo_de_visualizacao($conjunto){          

        $trimestres=[];
        $semestres=[];

        $lpl = 'Lucro_Prejuizo_Liquido';
        $ro = 'Receitas_Operacionais';
        $drb = 'Deducoes_Da_Receita_Bruta';
        $co = 'Custos_Operacionais';
        $do = 'Despesas_Operacionais';
        $df = 'Despesas_Financeiras';
        $die = 'Desembolso_Com_Investimentos_E_Emprestimos';
        $isv = 'Impostos_Sobre_Vendas';
        $csv = 'Comissoes_Sobre_Vendas';
        $dv = 'Devolucoes_De_Vendas';
        $od = 'Outras_Deducoes_1';


        foreach ($conjunto as $key => $conj_db) {
            $ano = intval($conj_db['Ano']);
            $mes = $conj_db['Mes'];
            $tri = null;

            if ($mes == 'jan' || $mes == 'fev' || $mes == 'mar'){
                $tri = 1;
            }
            elseif($mes == 'abr' || $mes == 'mai' || $mes == 'jun'){
                $tri = 2;   
            }
            elseif($mes == 'jul' || $mes == 'ago' || $mes == 'set'){
                $tri = 3;
            }
            else{                
                $tri = 4;
            }
            $trimestres[$ano][$tri][$lpl] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $lpl);
            $trimestres[$ano][$tri][$ro] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $ro);
            $trimestres[$ano][$tri][$drb] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $drb);
            $trimestres[$ano][$tri][$co] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $co);
            $trimestres[$ano][$tri][$do] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $do);
            $trimestres[$ano][$tri][$df] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $df);
            $trimestres[$ano][$tri][$die] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $die);
            $trimestres[$ano][$tri][$isv] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $isv);
            $trimestres[$ano][$tri][$csv] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $csv);
            $trimestres[$ano][$tri][$dv] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $dv);
            $trimestres[$ano][$tri][$od] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $od);  

            
        } 
        
        foreach ($trimestres as $ano => $trimestre) {
            if( isset($trimestres[$ano][1]) || isset($trimestres[$ano][2])){
                $semestres[$ano][1][$lpl] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $lpl);  
                $semestres[$ano][1][$ro] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $ro);  
                $semestres[$ano][1][$drb] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $drb);  
                $semestres[$ano][1][$co] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $co);  
                $semestres[$ano][1][$do] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $do);  
                $semestres[$ano][1][$df] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $df);  
                $semestres[$ano][1][$die] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $die);  
                $semestres[$ano][1][$isv] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $isv);  
                $semestres[$ano][1][$csv] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $csv);  
                $semestres[$ano][1][$dv] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $dv);  
                $semestres[$ano][1][$od] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $od);  

            }
            if( isset($trimestres[$ano][3]) || isset($trimestres[$ano][4])){
                $semestres[$ano][2][$lpl] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $lpl);  
                $semestres[$ano][2][$ro] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $ro);  
                $semestres[$ano][2][$drb] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $drb);  
                $semestres[$ano][2][$co] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $co);  
                $semestres[$ano][2][$do] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $do);  
                $semestres[$ano][2][$df] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $df);  
                $semestres[$ano][2][$die] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $die);  
                $semestres[$ano][2][$isv] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $isv);  
                $semestres[$ano][2][$csv] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $csv);  
                $semestres[$ano][2][$dv] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $dv);  
                $semestres[$ano][2][$od] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $od); 
            }
        }

        
        $group = [];
        foreach ($trimestres as $ano => $trimestre) {
            foreach ($trimestre as $tri => $valores) {
                $base = [];
                $valores['trimestre'] = $ano;
                $base[0]=$valores;
                $group['trimestres'][$ano][$tri]['LucroLiquidoMensal'] = $this->advancedPieChart_LucroLiquidoMensal( $base );  
                $group['trimestres'][$ano][$tri]['LucroLiquidoAcumulado'] = $this->advancedPieChart_LucroLiquidoAcumulado($base);
                $group['trimestres'][$ano][$tri]['GastosTotais'] = $this->PieChart_GastosTotais($base);
                $group['trimestres'][$ano][$tri]['DeducaoDaReceita'] = $this->PieChart_DeducaoDaReceita($base);
                $group['trimestres'][$ano][$tri]['ReceitasOperacionais'] = $this->VerticalBar_ReceitasOperacionais($base);
                $group['trimestres'][$ano][$tri]['PesoCustoDespesa'] = $this->LineChart_PesoCustoDespesa($base);          
            }            
        }


        foreach ($semestres as $ano => $semestre) {
            foreach ($semestre as $sem => $valores) {
                $base = [];
                $valores['semestres'] = $ano;
                $base[0]=$valores;
                $group['semestres'][$ano][$sem]['LucroLiquidoMensal'] = $this->advancedPieChart_LucroLiquidoMensal( $base );  
                $group['semestres'][$ano][$sem]['LucroLiquidoAcumulado'] = $this->advancedPieChart_LucroLiquidoAcumulado($base);
                $group['semestres'][$ano][$sem]['GastosTotais'] = $this->PieChart_GastosTotais($base);
                $group['semestres'][$ano][$sem]['DeducaoDaReceita'] = $this->PieChart_DeducaoDaReceita($base);
                $group['semestres'][$ano][$sem]['ReceitasOperacionais'] = $this->VerticalBar_ReceitasOperacionais($base);
                $group['semestres'][$ano][$sem]['PesoCustoDespesa'] = $this->LineChart_PesoCustoDespesa($base);          
            }            
        }


        
        return $group;      

    }

    public function somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $value){
        if(isset($trimestres[$ano][$tri][$value]))
            return $trimestres[$ano][$tri][$value] += round($conj_db[$value], 2);

        else
            return $trimestres[$ano][$tri][$value] = round($conj_db[$value], 2);  
    }

    public function somaValoresSemestral($semestres, $trimestres, $ano, $sem, $value){
        
        $pri_tri = isset($trimestres[$ano][1][$value]) ? $trimestres[$ano][1][$value]:0;
        $seg_tri = isset($trimestres[$ano][2][$value]) ? $trimestres[$ano][2][$value]:0 ;
        $ter_tri = isset($trimestres[$ano][3][$value]) ? $trimestres[$ano][3][$value]:0;
        $qua_tri = isset($trimestres[$ano][4][$value]) ? $trimestres[$ano][4][$value]:0;

        if(isset($semestres[$ano][$sem][$value])){
            if($sem == 1)
                return $semestres[$ano][$sem][$value] += round($pri_tri + $seg_tri, 2);
            else
                return $semestres[$ano][$sem][$value] += round($ter_tri + $qua_tri, 2);
        }

        else{
            if($sem == 1)
                return $semestres[$ano][$sem][$value] = round($pri_tri + $seg_tri, 2); 
            else
                return $semestres[$ano][$sem][$value] = round($ter_tri + $qua_tri, 2);

        }
    }

    public function selectLabel($data){
        if(isset($data['Mes'])){
            return $data['Mes'] . '/' . $data['Ano'];
        }
        elseif(isset($data['trimestre'])){
            return 'Trimestre';
        }
        else{
            return 'Semestre';
        }
    }


}

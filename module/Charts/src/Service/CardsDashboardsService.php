<?php
/**
 * Created by PhpStorm.
 * User: 
 * Date: 
 * Time: 
 */

namespace Charts\Service;

use Charts\Entity\ObjtSerie;
use Charts\Entity\ConjuntoObjtSerie;
use Upload\Entity\Uploads;
use Charts\Entity\LucroLiquidoMensal;
use Doctrine\ORM\EntityManager;

class CardsDashboardsService
{
    const ENTITY = 'Upload\Entity\Uploads';
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function save($data)
    {
        $chartItem = $this->em->find(self::ENTITY, (int) $data['id']);
       
        if (! $chartItem) {
            $chartItem = new LucroLiquidoMensal();
        }

        $chartItem->setData($data);
        $this->em->persist($chartItem);
        $this->em->flush();

    }

    public function fetch($empresa)
    {

        $select = $this->em->createQueryBuilder()->select(
            'u'
        )->from(self::ENTITY, 'Uploads')
        ->where("Uploads.Empresa = $empresa ");    

        $resultados = $select->getQuery()->getArrayResult();
        
        
        $result_constroiArrayMeses = $this->constroiArrayMeses($resultados);
        $result_LucroLiquidoMensal = $this->advancedPieChart_LucroLiquidoMensal($resultados);
        $result_LucroLiquidoAcumulado = $this->advancedPieChart_LucroLiquidoAcumulado($resultados);
        $result_PieChart_GastosTotais = $this->PieChart_GastosTotais($resultados);
        $result_PieChart_DeducaoDaReceita = $this->PieChart_DeducaoDaReceita($resultados);
        $result_VerticalBar_ReceitasOperacionais = $this->VerticalBar_ReceitasOperacionais($resultados);
        $result_LineChart_PesoCustoDespesa = $this->LineChart_PesoCustoDespesa($resultados);

        
        
        $conjunto_valores_gerar_dashboards['meses'] = $result_constroiArrayMeses;
        $conjunto_valores_gerar_dashboards['LucroLiquidoMensal'] = $result_LucroLiquidoMensal;
        $conjunto_valores_gerar_dashboards['LucroLiquidoAcumulado'] = $result_LucroLiquidoAcumulado;
        $conjunto_valores_gerar_dashboards['GastosTotais'] = $result_PieChart_GastosTotais;
        $conjunto_valores_gerar_dashboards['DeducaoDaReceita'] = $result_PieChart_DeducaoDaReceita;
        $conjunto_valores_gerar_dashboards['ReceitasOperacionais'] = $result_VerticalBar_ReceitasOperacionais;
        $conjunto_valores_gerar_dashboards['PesoCustoDespesa'] = $result_LineChart_PesoCustoDespesa;

        //var_dump($conjunto_valores_gerar_dashboards );die;
        
        return $conjunto_valores_gerar_dashboards;
    }


    public function fetchAll($params = null){
        $selectEmpresa = $this->em->createQueryBuilder()->select(  
            'Empresa.status',
            'Empresa.data_inscricao'
        )->from('Cadastros\Entity\Empresa', 'Empresa');
        $resultEmpresa = $selectEmpresa->getQuery()->getArrayResult();

        $selectContabilidade = $this->em->createQueryBuilder()->select(            
            'Contabilidade.status',
            'Contabilidade.data_inscricao',            
        )->from('Cadastros\Entity\Contabilidade', 'Contabilidade');
        $resultContabilidade = $selectContabilidade->getQuery()->getArrayResult();
        
        $resultadoContabilidade = $this->resultadosDashboards($resultContabilidade);
        
        return $resultContabilidade;
    }
    

    public function resultadosDashboards($resultContabilidade){
        $ativos = [];
        $teste = [];
        $desabilitados = [];        

        foreach ($resultContabilidade as $key => $cliente) {

            var_dump(  date_format($cliente['data_inscricao'], 'm-y' ) );
            if($cliente['status'] == 'ativo'){

            };
        }

        

            
        
    
    }
    


}

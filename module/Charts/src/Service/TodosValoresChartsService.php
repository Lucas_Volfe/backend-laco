<?php
/**
 * Created by PhpStorm.
 * User: 
 * Date: 
 * Time: 
 */

namespace Charts\Service;

use Charts\Entity\ObjtSerie;
use Charts\Entity\ConjuntoObjtSerie;
use Upload\Entity\Uploads;
use Charts\Entity\LucroLiquidoMensal;
use Doctrine\ORM\EntityManager;

class TodosValoresChartsService
{
    const ENTITY = 'Upload\Entity\Uploads';
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function save($data)
    {
        $chartItem = $this->em->find(self::ENTITY, (int) $data['id']);
       
        if (! $chartItem) {
            $chartItem = new LucroLiquidoMensal();
        }

        $chartItem->setData($data);
        $this->em->persist($chartItem);
        $this->em->flush();

    }

    public function fetch($empresa)
    {

        $emp = $this->em->find('Cadastros\Entity\Empresa', (int) $empresa);

        if(!$emp || $emp->tem_upload == 0 )
            return [null];// ['Nenhum registro encontrado'];


        $select = $this->em->createQueryBuilder()->select(
            'Uploads.id',
            'Uploads.Mes',
            'Uploads.Ano',
            'Uploads.Lucro_Prejuizo_Liquido',
            'Uploads.Lucro_Prejuizo_Operacional',
            'Uploads.Receitas_Operacionais',

            'Uploads.Deducoes_Da_Receita_Bruta',
            'Uploads.Custos_Operacionais',
            'Uploads.Despesas_Operacionais',
            'Uploads.Despesas_Financeiras',
            'Uploads.Desembolso_Com_Investimentos_E_Emprestimos',

            'Uploads.Impostos_Sobre_Vendas',
            'Uploads.Comissoes_Sobre_Vendas',
            'Uploads.Devolucoes_De_Vendas',
            'Uploads.Outras_Deducoes_1',
            'Uploads.Deducoes_Da_Receita_Bruta',

        )->from(self::ENTITY, 'Uploads')
        ->where("Uploads.Empresa = $empresa ");    
        $resultados = $select->getQuery()->getArrayResult(); 

        $conjunto_valores_gerar_dashboards['mensal']['meses'] = $this->constroiArrayMeses($resultados);
        $conjunto_valores_gerar_dashboards['mensal']['LucroLiquidoMensal'] = $this->advancedPieChart_LucroLiquidoMensal($resultados);
        $conjunto_valores_gerar_dashboards['mensal']['LucroLiquidoAcumulado'] = $this->advancedPieChart_LucroLiquidoAcumulado($resultados);
        $conjunto_valores_gerar_dashboards['mensal']['GastosTotais'] = $this->PieChart_GastosTotais($resultados);
        $conjunto_valores_gerar_dashboards['mensal']['DeducaoDaReceita'] = $this->PieChart_DeducaoDaReceita($resultados);
        $conjunto_valores_gerar_dashboards['mensal']['PesoCustoDespesa'] = $this->LineChart_PesoCustoDespesa($resultados);
        $conjunto_valores_gerar_dashboards['mensal']['ReceitasOperacionais'] = $this->VerticalBar_Operacionais($resultados, 'Receitas_Operacionais');
        
        $conjunto_valores_gerar_dashboards['mensal']['CustosOperacionais'] = $this->VerticalBar_Operacionais($resultados, 'Custos_Operacionais');
        $conjunto_valores_gerar_dashboards['mensal']['DespesasOperacionais'] = $this->VerticalBar_Operacionais($resultados, 'Despesas_Operacionais');
        $conjunto_valores_gerar_dashboards['mensal']['PontoEquilibrio'] = $this->base_conjunto($resultados, 'Despesas_Operacionais', 'Lucro_Prejuizo_Operacional', 'Ponto de Equilibrio');
        $conjunto_valores_gerar_dashboards['mensal']['ResultadoApurado'] = $this->base_conjunto($resultados,'Custos_Operacionais', 'Receitas_Operacionais', 'Resultados Apurados');
        $conjunto_valores_gerar_dashboards['mensal']['AnaliseResultadoLiquido'] = $this->barVerticalNegative_AnaliseResultadoLiquido($resultados);
        
        $tri_sem = $this->separar_conjunto_por_periodo_de_visualizacao($resultados);

        $conjunto_valores_gerar_dashboards['trimestres'] = $tri_sem['trimestres'];
        $conjunto_valores_gerar_dashboards['semestres'] = $tri_sem['semestres'];

        return $conjunto_valores_gerar_dashboards;
    }

    public function constroiArrayMeses($resultados)
    {
        $meses = array();
        foreach( $resultados as $key => $resultado )
        {
            $meses[] = [
                'id' => $key,
                'mes' => $resultado['Mes'] . '/' . $resultado['Ano']
            ];
        }

        return $meses;
    }

    public function barVerticalNegative_AnaliseResultadoLiquido($resultados){
        $conjuntoCompleto = array();
        $conjuntoReceitasOperacionais = array();
        $conjuntoGastosTotais = array();
        foreach ($resultados as $resultado) 
        {
            $label = $this->selectLabel($resultado); 

            $gastosTotais = round($resultado['Deducoes_Da_Receita_Bruta'] + $resultado['Custos_Operacionais'] + $resultado['Despesas_Operacionais'], 4);
      
            $conjuntoReceitasOperacionais = new ObjtSerie( 'Receitas Operacionais' , floatval($resultado['Receitas_Operacionais']) );
            $conjuntoGastosTotais = new ObjtSerie( 'Gastos Totais' , $gastosTotais );

            $conjunto = array();
            $conjunto[] = $conjuntoReceitasOperacionais;
            $conjunto[] = $conjuntoGastosTotais;
          
            $conjuntoCompleto[] = new ConjuntoObjtSerie($label, $conjunto );      
        }

        //var_dump($conjuntoCusto);die;
        return $conjuntoCompleto;

       
        
    }
    public function advancedPieChart_LucroLiquidoMensal($resultados)
    {     
        $valores_mensais = array();
        foreach ($resultados as $key => $resultado) {
            $label = $this->selectLabel($resultado); 

            if( $resultado['Lucro_Prejuizo_Liquido'] != null && $resultado['Receitas_Operacionais'] != null ){
                $valor_porcent = (float) number_format((($resultado['Lucro_Prejuizo_Liquido'] / $resultado['Receitas_Operacionais'])*100),2);
                
                $concat = array();
                if($valor_porcent < 0) $valor_porcent = $valor_porcent*-1;
                $concat[] = new ObjtSerie( $label, $valor_porcent );
                $concat[] = new ObjtSerie( 'Total', 100 - $valor_porcent);
                $valores_mensais[$key] = $concat; 
            }
        }
        return $valores_mensais;
    }

    public function advancedPieChart_LucroLiquidoAcumulado($resultados)
    {
        $total_receitas = 0;
        $total_luc_prj_liquido = 0;        
        foreach ($resultados as $resultado) {
            $total_receitas += $resultado['Receitas_Operacionais'];
            $total_luc_prj_liquido += $resultado['Lucro_Prejuizo_Liquido'];
        }        
        $valor = (float) number_format((($total_luc_prj_liquido / $total_receitas)*100), 2);

        if($valor < 0) $valor = $valor*-1;
        $resultado_final = array();
        $resultado_final[] = new ObjtSerie('Lucro Líquido Acumulado', $valor);
        $resultado_final[] = new ObjtSerie('Total', 100 - $valor);
        return $resultado_final;
    }

    public function PieChart_GastosTotais($resultados)
    {

        $conjuntoCompleto = array();
        foreach ($resultados as $resultado) 
        {
            $conjuntoMensal = array();
            $conjuntoMensal[] = new ObjtSerie( 'Deduções da Receita Bruta' , ($resultado['Deducoes_Da_Receita_Bruta'] > 0) ? $resultado['Deducoes_Da_Receita_Bruta'] : $resultado['Deducoes_Da_Receita_Bruta'] * -1 );
            $conjuntoMensal[] = new ObjtSerie( 'Custos Operacionais' , ($resultado['Custos_Operacionais'] > 0) ? $resultado['Custos_Operacionais'] : $resultado['Custos_Operacionais'] * -1 );
            $conjuntoMensal[] = new ObjtSerie( 'Despesas Operacionais' , ($resultado['Despesas_Operacionais'] > 0) ? $resultado['Despesas_Operacionais'] : $resultado['Despesas_Operacionais'] * -1 );
            $conjuntoMensal[] = new ObjtSerie( 'Despesas Financeiras' , ($resultado['Despesas_Financeiras'] > 0) ? $resultado['Despesas_Financeiras'] : $resultado['Despesas_Financeiras'] * -1 );
            $conjuntoMensal[] = new ObjtSerie( 'Despesas com Investimentos e Empréstimos' , ($resultado['Desembolso_Com_Investimentos_E_Emprestimos'] > 0) ? $resultado['Desembolso_Com_Investimentos_E_Emprestimos'] : $resultado['Desembolso_Com_Investimentos_E_Emprestimos'] * -1 );
            $conjuntoCompleto[] = $conjuntoMensal;
        }
        return $conjuntoCompleto;
    }
        

    public function PieChart_DeducaoDaReceita($resultados)
    {
        $conjuntoCompleto = array();
        foreach ($resultados as $resultado) 
        {
            $conjuntoMensal = array();
            $conjuntoMensal[] = new ObjtSerie( 'Impostos Sobre Vendas' , ($resultado['Impostos_Sobre_Vendas'] > 0) ? $resultado['Impostos_Sobre_Vendas'] : $resultado['Impostos_Sobre_Vendas'] * -1 );
            $conjuntoMensal[] = new ObjtSerie( 'Comissões Sobre Vendas' , ($resultado['Comissoes_Sobre_Vendas'] > 0) ? $resultado['Comissoes_Sobre_Vendas'] : $resultado['Comissoes_Sobre_Vendas'] * -1 );
            $conjuntoMensal[] = new ObjtSerie( 'Devoluções de Vendas' , ($resultado['Devolucoes_De_Vendas'] > 0) ? $resultado['Devolucoes_De_Vendas'] : $resultado['Devolucoes_De_Vendas'] * -1 );
            $conjuntoMensal[] = new ObjtSerie( 'Deduções 1' , ($resultado['Outras_Deducoes_1'] > 0) ? $resultado['Outras_Deducoes_1'] : $resultado['Outras_Deducoes_1'] * -1 );

            $conjuntoCompleto[] = $conjuntoMensal;
        }
        return $conjuntoCompleto;
    }  

    public function VerticalBar_Operacionais($resultados, $item)
    {
        $conjuntoCompleto = array();
        foreach ($resultados as $resultado) 
        {
            $label = $this->selectLabel($resultado); 
            $conjuntoCompleto[] = new ObjtSerie( $label , ($resultado[$item] > 0) ? $resultado[$item] : $resultado[$item] * -1 );
        }
        return $conjuntoCompleto;
    }

    public function LineChart_PesoCustoDespesa($resultados)
    {
        $conjuntoCompleto = array();
        $conjuntoCusto = array();
        $conjuntoDespesas = array();
        foreach ($resultados as $resultado) 
        {
            $label = $this->selectLabel($resultado); 

            $somaCusto = round($resultado['Custos_Operacionais'] / $resultado['Receitas_Operacionais'], 4);
            $somaDespesa = round($resultado['Despesas_Operacionais'] / $resultado['Receitas_Operacionais'], 4);
      
            $conjuntoCusto[] = new ObjtSerie( $label , ( $somaCusto > 0) ? round($somaCusto * 100,2) : round($somaCusto * -100,2) );
            $conjuntoDespesas[] = new ObjtSerie( $label , ( $somaDespesa > 0) ? round($somaDespesa * 100,2) : round($somaDespesa * -100,2) );
          
        }

        //var_dump($conjuntoCusto);die;
        $conjuntoCompleto[] = new ConjuntoObjtSerie('Peso Custo', $conjuntoCusto );      
        $conjuntoCompleto[] = new ConjuntoObjtSerie('Peso Despesa', $conjuntoDespesas ); 
        return $conjuntoCompleto;
    }
    
    public function base_conjunto($resultados, $v1, $v2, $title)
    {
        $conjuntoCompleto = array();
        $conjunto = array();
        foreach ($resultados as $resultado) 
        {            
            $label = $this->selectLabel($resultado); 
            $soma = round($resultado[$v1] / $resultado[$v2], 4);
            $conjunto[] = new ObjtSerie( $label , ( $soma > 0) ? round($soma * 100,2) : round($soma * -100,2) );
        }     
        $conjuntoCompleto[] = new ConjuntoObjtSerie($title, $conjunto ); 
        return $conjuntoCompleto;
    }

    public function separar_conjunto_por_periodo_de_visualizacao($conjunto){          
        $trimestres=[];
        $semestres=[];

        $lpl = 'Lucro_Prejuizo_Liquido';
        $ro = 'Receitas_Operacionais';
        $drb = 'Deducoes_Da_Receita_Bruta';
        $co = 'Custos_Operacionais';
        $do = 'Despesas_Operacionais';
        $df = 'Despesas_Financeiras';
        $die = 'Desembolso_Com_Investimentos_E_Emprestimos';
        $isv = 'Impostos_Sobre_Vendas';
        $csv = 'Comissoes_Sobre_Vendas';
        $dv = 'Devolucoes_De_Vendas';
        $od = 'Outras_Deducoes_1';
        $lpo = 'Lucro_Prejuizo_Operacional';
        $gt = 'Gastos_Totais';
        foreach ($conjunto as $key => $conj_db) {
            $ano = intval($conj_db['Ano']);
            $mes = $conj_db['Mes'];
            $tri = $this->trimestreReferente($mes);

            $gastosTotais = ($conj_db['Deducoes_Da_Receita_Bruta'] + $conj_db['Custos_Operacionais'] + $conj_db['Despesas_Operacionais'])*-1;
            $conj_db[$gt] = $gastosTotais;

            $trimestres[$ano][$tri][$lpl] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $lpl);
            $trimestres[$ano][$tri][$ro] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $ro);
            $trimestres[$ano][$tri][$drb] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $drb);
            $trimestres[$ano][$tri][$co] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $co);
            $trimestres[$ano][$tri][$do] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $do);
            $trimestres[$ano][$tri][$df] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $df);
            $trimestres[$ano][$tri][$die] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $die);
            $trimestres[$ano][$tri][$isv] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $isv);
            $trimestres[$ano][$tri][$csv] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $csv);
            $trimestres[$ano][$tri][$dv] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $dv);
            $trimestres[$ano][$tri][$od] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $od);  
            $trimestres[$ano][$tri][$lpo] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $lpo);  
            $trimestres[$ano][$tri][$gt] = $this->somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $gt);  

            
        } 
        
        foreach ($trimestres as $ano => $trimestre) {
            if( isset($trimestres[$ano][1]) || isset($trimestres[$ano][2])){            

                $semestres[$ano][1][$lpl] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $lpl);  
                $semestres[$ano][1][$ro] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $ro);  
                $semestres[$ano][1][$drb] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $drb);  
                $semestres[$ano][1][$co] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $co);  
                $semestres[$ano][1][$do] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $do);  
                $semestres[$ano][1][$df] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $df);  
                $semestres[$ano][1][$die] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $die);  
                $semestres[$ano][1][$isv] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $isv);  
                $semestres[$ano][1][$csv] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $csv);  
                $semestres[$ano][1][$dv] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $dv);  
                $semestres[$ano][1][$od] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $od);  
                $semestres[$ano][1][$lpo] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $lpo);  
                $semestres[$ano][1][$gt] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 1, $gt);  

            }
            if( isset($trimestres[$ano][3]) || isset($trimestres[$ano][4])){
                $semestres[$ano][2][$lpl] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $lpl);  
                $semestres[$ano][2][$ro] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $ro);  
                $semestres[$ano][2][$drb] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $drb);  
                $semestres[$ano][2][$co] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $co);  
                $semestres[$ano][2][$do] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $do);  
                $semestres[$ano][2][$df] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $df);  
                $semestres[$ano][2][$die] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $die);  
                $semestres[$ano][2][$isv] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $isv);  
                $semestres[$ano][2][$csv] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $csv);  
                $semestres[$ano][2][$dv] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $dv);  
                $semestres[$ano][2][$od] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $od); 
                $semestres[$ano][2][$lpo] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $lpo); 
                $semestres[$ano][2][$gt] = $this->somaValoresSemestral($semestres, $trimestres, $ano, 2, $gt); 
            }
        }

        
        $group = [];
        foreach ($trimestres as $ano => $trimestre) {
            foreach ($trimestre as $tri => $valores) {
                $base = [];
                $valores['trimestre'] = $ano;
                $valores['tri'] = $tri;
                $base[0]=$valores;
                $group['trimestres'][$ano][$tri]['LucroLiquidoMensal'] = $this->advancedPieChart_LucroLiquidoMensal( $base );  
                $group['trimestres'][$ano][$tri]['LucroLiquidoAcumulado'] = $this->advancedPieChart_LucroLiquidoAcumulado($base);
                $group['trimestres'][$ano][$tri]['GastosTotais'] = $this->PieChart_GastosTotais($base);
                $group['trimestres'][$ano][$tri]['DeducaoDaReceita'] = $this->PieChart_DeducaoDaReceita($base);
                $group['trimestres'][$ano][$tri]['ReceitasOperacionais'] = $this->VerticalBar_Operacionais($base, 'Receitas_Operacionais');
                $group['trimestres'][$ano][$tri]['CustosOperacionais'] = $this->VerticalBar_Operacionais($base, 'Custos_Operacionais');
                $group['trimestres'][$ano][$tri]['DespesasOperacionais'] = $this->VerticalBar_Operacionais($base, 'Despesas_Operacionais');
                $group['trimestres'][$ano][$tri]['PesoCustoDespesa'] = $this->LineChart_PesoCustoDespesa($base); 

                $group['trimestres'][$ano][$tri]['PontoEquilibrio'] = $this->base_conjunto($base, 'Despesas_Operacionais', 'Lucro_Prejuizo_Operacional', 'Ponto de Equilibrio');          
                $group['trimestres'][$ano][$tri]['ResultadoApurado'] = $this->base_conjunto($base,'Custos_Operacionais', 'Receitas_Operacionais', 'Resultados Apurados');
                $group['trimestres'][$ano][$tri]['AnaliseResultadoLiquido'] = $this->barVerticalNegative_AnaliseResultadoLiquido($base);
            }            
        }


        foreach ($semestres as $ano => $semestre) {
            foreach ($semestre as $sem => $valores) {
                $base = [];
                $valores['semestres'] = $ano;
                $valores['sem'] = $sem;
                $base[0]=$valores;
                $group['semestres'][$ano][$sem]['LucroLiquidoMensal'] = $this->advancedPieChart_LucroLiquidoMensal( $base );  
                $group['semestres'][$ano][$sem]['LucroLiquidoAcumulado'] = $this->advancedPieChart_LucroLiquidoAcumulado($base);
                $group['semestres'][$ano][$sem]['GastosTotais'] = $this->PieChart_GastosTotais($base);
                $group['semestres'][$ano][$sem]['DeducaoDaReceita'] = $this->PieChart_DeducaoDaReceita($base);
                $group['semestres'][$ano][$sem]['ReceitasOperacionais'] = $this->VerticalBar_Operacionais($base, 'Receitas_Operacionais');
                $group['semestres'][$ano][$sem]['CustosOperacionais'] = $this->VerticalBar_Operacionais($base, 'Custos_Operacionais');
                $group['semestres'][$ano][$sem]['DespesasOperacionais'] = $this->VerticalBar_Operacionais($base, 'Despesas_Operacionais');
                $group['semestres'][$ano][$sem]['PesoCustoDespesa'] = $this->LineChart_PesoCustoDespesa($base);  
                $group['semestres'][$ano][$sem]['PontoEquilibrio'] = $this->base_conjunto($base, 'Despesas_Operacionais', 'Lucro_Prejuizo_Operacional', 'Ponto de Equilibrio');       
                $group['semestres'][$ano][$sem]['ResultadoApurado'] = $this->base_conjunto($base,'Custos_Operacionais', 'Receitas_Operacionais', 'Resultados Apurados');
                $group['semestres'][$ano][$sem]['AnaliseResultadoLiquido'] = $this->barVerticalNegative_AnaliseResultadoLiquido($base);

            }     
        }

        foreach($group as $index => $anoSemTri){
            
            $ReceitasOperacionais = [];                
            $CustosOperacionais = [];                
            $DespesasOperacionais = [];   
            $PesoCustoDespesa = [];   
            $PontoEquilibrio = [];   
            $ResultadoApurado = [];   
            
            $PesoCustoDespesa[0]['name'] = 'Peso Custo';
            $PontoEquilibrio[0]['name'] = 'Ponto de Equilibrio';
            $ResultadoApurado[0]['name'] = 'Resultados Apurados';

            foreach($anoSemTri as $i => $resultPart){
                $AnaliseResultadoLiquido = [];
                foreach ($resultPart as $value) {      
                    $ReceitasOperacionais[] = $value['ReceitasOperacionais'][0];  
                    $CustosOperacionais[] = $value['CustosOperacionais'][0];                
                    $DespesasOperacionais[] = $value['DespesasOperacionais'][0];   
                    $PesoCustoDespesa[0]['series'][] = $value['PesoCustoDespesa'][0]->series[0];;   
                    $PontoEquilibrio[0]['series'][] = $value['PontoEquilibrio'][0]->series[0];   
                    $ResultadoApurado[0]['series'][] = $value['ResultadoApurado'][0]->series[0];    
                    $AnaliseResultadoLiquido[] = $value['AnaliseResultadoLiquido'][0];                    
                }
                foreach ($resultPart as $key => $value) {
                    $resultPart[$key]['ReceitasOperacionais'] = $ReceitasOperacionais;
                    $resultPart[$key]['CustosOperacionais'] = $CustosOperacionais;
                    $resultPart[$key]['DespesasOperacionais'] = $DespesasOperacionais;
                    $resultPart[$key]['PesoCustoDespesa'] = $PesoCustoDespesa;
                    $resultPart[$key]['PontoEquilibrio'] = $PontoEquilibrio;
                    $resultPart[$key]['ResultadoApurado'] = $ResultadoApurado;
                    $resultPart[$key]['AnaliseResultadoLiquido'] = $AnaliseResultadoLiquido;
                }

                $group[$index][$i] = $resultPart;
            }
        }
        
        return $group;      

    }

    public function somaValoresTrimestral($trimestres, $ano, $tri, $conj_db, $value){
        if(isset($trimestres[$ano][$tri][$value]))
            return $trimestres[$ano][$tri][$value] += round($conj_db[$value], 2);

        else
            return $trimestres[$ano][$tri][$value] = round($conj_db[$value], 2);  
    }

    public function somaValoresSemestral($semestres, $trimestres, $ano, $sem, $value){
        
        $pri_tri = isset($trimestres[$ano][1][$value]) ? $trimestres[$ano][1][$value]:0;
        $seg_tri = isset($trimestres[$ano][2][$value]) ? $trimestres[$ano][2][$value]:0 ;
        $ter_tri = isset($trimestres[$ano][3][$value]) ? $trimestres[$ano][3][$value]:0;
        $qua_tri = isset($trimestres[$ano][4][$value]) ? $trimestres[$ano][4][$value]:0;

        if(isset($semestres[$ano][$sem][$value])){
            if($sem == 1)
                return $semestres[$ano][$sem][$value] += round($pri_tri + $seg_tri, 2);
            else
                return $semestres[$ano][$sem][$value] += round($ter_tri + $qua_tri, 2);
        }

        else{
            if($sem == 1)
                return $semestres[$ano][$sem][$value] = round($pri_tri + $seg_tri, 2); 
            else
                return $semestres[$ano][$sem][$value] = round($ter_tri + $qua_tri, 2);

        }
    }

    public function selectLabel($data){
        // var_dump($data);die;
        if(isset($data['Mes'])){
            return $data['Mes'] . '/' . $data['Ano'];
        }
        elseif(isset($data['trimestre'])){
            
            return $data['tri'].' Trimestre '.$data['trimestre'];
        }
        else{
            return $data['sem'].' Semestre '.$data['semestres'];
        }
    }

    public function trimestreReferente($mes){
        if ($mes == 'jan' || $mes == 'fev' || $mes == 'mar'){
            return 1;
        }
        elseif($mes == 'abr' || $mes == 'mai' || $mes == 'jun'){
            return 2;   
        }
        elseif($mes == 'jul' || $mes == 'ago' || $mes == 'set'){
            return 3;
        }
        else{                
            return 4;
        }
    }


}

<?php

namespace Charts\Entity;

use Charts\Entity\ObjtSerie;

class ConjuntoObjtSerie 
{
    public $name;
    public $series;
    
    public function __construct($name, $serie)
    {
        $this->name = $name;
        $this->series = $serie;
    }

}

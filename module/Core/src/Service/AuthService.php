<?php

namespace Core\Service;

use Zend\Db\Adapter\Adapter;

class AuthService
{

    protected $db;

    public function __construct(Adapter $db)
    {
        $this->db = $db;
    }

    public function programsByRole($role)
    {
        $sql = 'SELECT p.name, p.url, p.icon, m.name as name_module, 
                m.url as url_module, m.icon as icon_module
                FROM oauth_programs p
		        left join oauth_modules m on m.name = p.module_name WHERE role like :role';
        $query = $this->db->query($sql);
        $result = $query->execute(['role' => $role]);
        $data = [];

        foreach ($result as $r) {
            if ($r['name_module'] != null) {
                $data[$r['name_module']]['children'][] = ['name' => $r['name'], 'url' => $r['url'], 'icon' => $r['icon']];
                $data[$r['name_module']]['name'] = $r['name_module'];
                $data[$r['name_module']]['url'] = $r['url_module'];
                $data[$r['name_module']]['icon'] = $r['icon'];
            } else {
                $data[] = ['name' => $r['name'], 'url' => $r['url'], 'icon' => $r['icon']];
            }
        }

        $dataResponse = [];

        foreach ($data as $d) {
            $dataResponse[] = $d;
        }

        return $dataResponse;
    }

}
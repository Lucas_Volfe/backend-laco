<?php
namespace Core;

use Core\Service\AuthService;

return [
    'service_manager' => [
        'factories' => [
            'AuthService' => function ($sm) {
              $db = $sm->get('sh');
  
              return new AuthService($db);
            }
        ]
    ],
];

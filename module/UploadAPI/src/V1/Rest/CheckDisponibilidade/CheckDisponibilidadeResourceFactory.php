<?php
namespace UploadAPI\V1\Rest\CheckDisponibilidade;

class CheckDisponibilidadeResourceFactory
{
    public function __invoke($services)
    {
        return new CheckDisponibilidadeResource($services->get('CheckDisponibilidadeService'));
    }
}

<?php
namespace UploadAPI\V1\Rest\Uploads;

class UploadsResourceFactory
{
    public function __invoke($services)
    {
        return new UploadsResource($services->get('UploadsService'));
    }
}

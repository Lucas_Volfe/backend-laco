<?php
namespace UploadAPI\V1\Rest\CheckDisponibilidadeBalancoPatrimonial;

class CheckDisponibilidadeBalancoPatrimonialResourceFactory
{
    public function __invoke($services)
    {
        return new CheckDisponibilidadeBalancoPatrimonialResource($services->get('CheckDisponibilidadeBalancoPatrimonialService'));
    }
}

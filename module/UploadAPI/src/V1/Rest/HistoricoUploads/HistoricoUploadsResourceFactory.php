<?php
namespace UploadAPI\V1\Rest\HistoricoUploads;

class HistoricoUploadsResourceFactory
{
    public function __invoke($services)
    {
        return new HistoricoUploadsResource($services->get('HistoricoUploadsService'));
    }
}

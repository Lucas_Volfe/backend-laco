<?php
namespace UploadAPI\V1\Rest\BalancoPatrimonial;

class BalancoPatrimonialResourceFactory
{
    public function __invoke($services)
    {
        return new BalancoPatrimonialResource($services->get('BalancoPatrimonialService'));
    }
}

<?php
namespace ChartsAPI\V1\Rest\LucroLiquidoAcumulado;

class LucroLiquidoAcumuladoResourceFactory
{
    public function __invoke($services)
    {
        return new LucroLiquidoAcumuladoResource($services->get('LucroLiquidoAcumuladoService'));
    }
}

<?php
namespace ChartsAPI\V1\Rest\LucroLiquidoMensal;

class LucroLiquidoMensalResourceFactory
{
    public function __invoke($services)
    {
        return new LucroLiquidoMensalResource($services->get('LucroLiquidoMensalService'));
    }
}

<?php
namespace ChartsAPI\V1\Rest\TodosValoresCharts;

class TodosValoresChartsResourceFactory
{
    public function __invoke($services)
    {
        return new TodosValoresChartsResource($services->get('TodosValoresChartsService'));
    }
}

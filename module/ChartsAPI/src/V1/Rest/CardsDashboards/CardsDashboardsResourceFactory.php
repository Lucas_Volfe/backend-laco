<?php
namespace ChartsAPI\V1\Rest\CardsDashboards;

class CardsDashboardsResourceFactory
{
    public function __invoke($services)
    {
        return new CardsDashboardsResource($services->get('CardsDashboardsService'));
    }
}

<?php
return [
    'service_manager' => [
        'factories' => [
            \ChartsAPI\V1\Rest\LucroLiquidoMensal\LucroLiquidoMensalResource::class => \ChartsAPI\V1\Rest\LucroLiquidoMensal\LucroLiquidoMensalResourceFactory::class,
            \ChartsAPI\V1\Rest\LucroLiquidoAcumulado\LucroLiquidoAcumuladoResource::class => \ChartsAPI\V1\Rest\LucroLiquidoAcumulado\LucroLiquidoAcumuladoResourceFactory::class,
            \ChartsAPI\V1\Rest\TodosValoresCharts\TodosValoresChartsResource::class => \ChartsAPI\V1\Rest\TodosValoresCharts\TodosValoresChartsResourceFactory::class,
            \ChartsAPI\V1\Rest\CardsDashboards\CardsDashboardsResource::class => \ChartsAPI\V1\Rest\CardsDashboards\CardsDashboardsResourceFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'charts-api.rest.lucro-liquido-mensal' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/lucro-liquido-mensal[/:lucro_liquido_mensal_id]',
                    'defaults' => [
                        'controller' => 'ChartsAPI\\V1\\Rest\\LucroLiquidoMensal\\Controller',
                    ],
                ],
            ],
            'charts-api.rest.lucro-liquido-acumulado' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/lucro-liquido-acumulado[/:lucro_liquido_acumulado_id]',
                    'defaults' => [
                        'controller' => 'ChartsAPI\\V1\\Rest\\LucroLiquidoAcumulado\\Controller',
                    ],
                ],
            ],
            'charts-api.rest.todos-valores-charts' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/todos-valores-charts[/:todos_valores_charts_id]',
                    'defaults' => [
                        'controller' => 'ChartsAPI\\V1\\Rest\\TodosValoresCharts\\Controller',
                    ],
                ],
            ],
            'charts-api.rest.cards-dashboards' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/cards-dashboards[/:cards_dashboards_id]',
                    'defaults' => [
                        'controller' => 'ChartsAPI\\V1\\Rest\\CardsDashboards\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'zf-versioning' => [
        'uri' => [
            0 => 'charts-api.rest.lucro-liquido-mensal',
            1 => 'charts-api.rest.lucro-liquido-acumulado',
            2 => 'charts-api.rest.todos-valores-charts',
            3 => 'charts-api.rest.cards-dashboards',
        ],
    ],
    'zf-rest' => [
        'ChartsAPI\\V1\\Rest\\LucroLiquidoMensal\\Controller' => [
            'listener' => \ChartsAPI\V1\Rest\LucroLiquidoMensal\LucroLiquidoMensalResource::class,
            'route_name' => 'charts-api.rest.lucro-liquido-mensal',
            'route_identifier_name' => 'lucro_liquido_mensal_id',
            'collection_name' => 'lucro_liquido_mensal',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
                4 => 'POST',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \ChartsAPI\V1\Rest\LucroLiquidoMensal\LucroLiquidoMensalEntity::class,
            'collection_class' => \ChartsAPI\V1\Rest\LucroLiquidoMensal\LucroLiquidoMensalCollection::class,
            'service_name' => 'LucroLiquidoMensal',
        ],
        'ChartsAPI\\V1\\Rest\\LucroLiquidoAcumulado\\Controller' => [
            'listener' => \ChartsAPI\V1\Rest\LucroLiquidoAcumulado\LucroLiquidoAcumuladoResource::class,
            'route_name' => 'charts-api.rest.lucro-liquido-acumulado',
            'route_identifier_name' => 'lucro_liquido_acumulado_id',
            'collection_name' => 'lucro_liquido_acumulado',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \ChartsAPI\V1\Rest\LucroLiquidoAcumulado\LucroLiquidoAcumuladoEntity::class,
            'collection_class' => \ChartsAPI\V1\Rest\LucroLiquidoAcumulado\LucroLiquidoAcumuladoCollection::class,
            'service_name' => 'LucroLiquidoAcumulado',
        ],
        'ChartsAPI\\V1\\Rest\\TodosValoresCharts\\Controller' => [
            'listener' => \ChartsAPI\V1\Rest\TodosValoresCharts\TodosValoresChartsResource::class,
            'route_name' => 'charts-api.rest.todos-valores-charts',
            'route_identifier_name' => 'todos_valores_charts_id',
            'collection_name' => 'todos_valores_charts',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \ChartsAPI\V1\Rest\TodosValoresCharts\TodosValoresChartsEntity::class,
            'collection_class' => \ChartsAPI\V1\Rest\TodosValoresCharts\TodosValoresChartsCollection::class,
            'service_name' => 'TodosValoresCharts',
        ],
        'ChartsAPI\\V1\\Rest\\CardsDashboards\\Controller' => [
            'listener' => \ChartsAPI\V1\Rest\CardsDashboards\CardsDashboardsResource::class,
            'route_name' => 'charts-api.rest.cards-dashboards',
            'route_identifier_name' => 'cards_dashboards_id',
            'collection_name' => 'cards_dashboards',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \ChartsAPI\V1\Rest\CardsDashboards\CardsDashboardsEntity::class,
            'collection_class' => \ChartsAPI\V1\Rest\CardsDashboards\CardsDashboardsCollection::class,
            'service_name' => 'CardsDashboards',
        ],
    ],
    'zf-content-negotiation' => [
        'controllers' => [
            'ChartsAPI\\V1\\Rest\\LucroLiquidoMensal\\Controller' => 'Json',
            'ChartsAPI\\V1\\Rest\\LucroLiquidoAcumulado\\Controller' => 'Json',
            'ChartsAPI\\V1\\Rest\\TodosValoresCharts\\Controller' => 'Json',
            'ChartsAPI\\V1\\Rest\\CardsDashboards\\Controller' => 'Json',
        ],
        'accept_whitelist' => [
            'ChartsAPI\\V1\\Rest\\LucroLiquidoMensal\\Controller' => [
                0 => 'application/vnd.charts-api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'ChartsAPI\\V1\\Rest\\LucroLiquidoAcumulado\\Controller' => [
                0 => 'application/vnd.charts-api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'ChartsAPI\\V1\\Rest\\TodosValoresCharts\\Controller' => [
                0 => 'application/vnd.charts-api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'ChartsAPI\\V1\\Rest\\CardsDashboards\\Controller' => [
                0 => 'application/vnd.charts-api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'ChartsAPI\\V1\\Rest\\LucroLiquidoMensal\\Controller' => [
                0 => 'application/vnd.charts-api.v1+json',
                1 => 'application/json',
            ],
            'ChartsAPI\\V1\\Rest\\LucroLiquidoAcumulado\\Controller' => [
                0 => 'application/vnd.charts-api.v1+json',
                1 => 'application/json',
            ],
            'ChartsAPI\\V1\\Rest\\TodosValoresCharts\\Controller' => [
                0 => 'application/vnd.charts-api.v1+json',
                1 => 'application/json',
            ],
            'ChartsAPI\\V1\\Rest\\CardsDashboards\\Controller' => [
                0 => 'application/vnd.charts-api.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'zf-hal' => [
        'metadata_map' => [
            \ChartsAPI\V1\Rest\LucroLiquidoMensal\LucroLiquidoMensalEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'charts-api.rest.lucro-liquido-mensal',
                'route_identifier_name' => 'lucro_liquido_mensal_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \ChartsAPI\V1\Rest\LucroLiquidoMensal\LucroLiquidoMensalCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'charts-api.rest.lucro-liquido-mensal',
                'route_identifier_name' => 'lucro_liquido_mensal_id',
                'is_collection' => true,
            ],
            \ChartsAPI\V1\Rest\LucroLiquidoAcumulado\LucroLiquidoAcumuladoEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'charts-api.rest.lucro-liquido-acumulado',
                'route_identifier_name' => 'lucro_liquido_acumulado_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \ChartsAPI\V1\Rest\LucroLiquidoAcumulado\LucroLiquidoAcumuladoCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'charts-api.rest.lucro-liquido-acumulado',
                'route_identifier_name' => 'lucro_liquido_acumulado_id',
                'is_collection' => true,
            ],
            \ChartsAPI\V1\Rest\TodosValoresCharts\TodosValoresChartsEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'charts-api.rest.todos-valores-charts',
                'route_identifier_name' => 'todos_valores_charts_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \ChartsAPI\V1\Rest\TodosValoresCharts\TodosValoresChartsCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'charts-api.rest.todos-valores-charts',
                'route_identifier_name' => 'todos_valores_charts_id',
                'is_collection' => true,
            ],
            \ChartsAPI\V1\Rest\CardsDashboards\CardsDashboardsEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'charts-api.rest.cards-dashboards',
                'route_identifier_name' => 'cards_dashboards_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \ChartsAPI\V1\Rest\CardsDashboards\CardsDashboardsCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'charts-api.rest.cards-dashboards',
                'route_identifier_name' => 'cards_dashboards_id',
                'is_collection' => true,
            ],
        ],
    ],
    'zf-content-validation' => [
        'ChartsAPI\\V1\\Rest\\LucroLiquidoMensal\\Controller' => [
            'input_filter' => 'ChartsAPI\\V1\\Rest\\LucroLiquidoMensal\\Validator',
        ],
        'ChartsAPI\\V1\\Rest\\LucroLiquidoAcumulado\\Controller' => [
            'input_filter' => 'ChartsAPI\\V1\\Rest\\LucroLiquidoAcumulado\\Validator',
        ],
        'ChartsAPI\\V1\\Rest\\TodosValoresCharts\\Controller' => [
            'input_filter' => 'ChartsAPI\\V1\\Rest\\TodosValoresCharts\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'ChartsAPI\\V1\\Rest\\LucroLiquidoMensal\\Validator' => [
            0 => [
                'required' => false,
                'validators' => [],
                'filters' => [],
                'name' => 'total',
                'field_type' => 'float',
            ],
        ],
        'ChartsAPI\\V1\\Rest\\LucroLiquidoAcumulado\\Validator' => [
            0 => [
                'required' => false,
                'validators' => [],
                'filters' => [],
                'name' => 'id',
                'field_type' => 'integer',
            ],
            1 => [
                'required' => false,
                'validators' => [],
                'filters' => [],
                'name' => 'Lucro_Prejuizo_Liquido',
                'field_type' => 'float',
            ],
            2 => [
                'required' => false,
                'validators' => [],
                'filters' => [],
                'name' => 'Receitas_Operacionais',
                'field_type' => 'float',
            ],
            3 => [
                'required' => false,
                'validators' => [],
                'filters' => [],
                'name' => 'Diferenca_Porcentagem',
                'field_type' => 'float',
            ],
            4 => [
                'required' => false,
                'validators' => [],
                'filters' => [],
                'name' => 'Empresa',
                'field_type' => 'string',
            ],
        ],
        'ChartsAPI\\V1\\Rest\\TodosValoresCharts\\Validator' => [
            0 => [
                'required' => false,
                'validators' => [],
                'filters' => [],
                'name' => 'id',
                'field_type' => 'integer',
            ],
            1 => [
                'required' => false,
                'validators' => [],
                'filters' => [],
                'name' => 'Empresa',
                'field_type' => 'integer',
            ],
        ],
    ],
];

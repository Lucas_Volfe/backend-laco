<?php
/**
 * Created by PhpStorm.
 * User: 
 * Date: 
 * Time: 
 */

namespace Cadastros\Service;

use Cadastros\Entity\Contabilidade;
use Doctrine\ORM\EntityManager;

class TakeAccessAccountService
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    
    public function fetch($id)
    {
                
    }

    public function fetchAll($params)
    {       

        $token = (string) $_GET['token'];
        
        $select = $this->em->createQueryBuilder()->select(            
            'tk.user_id', 'tk.expires', 'user.first_name',
            "(case when user.Empresa is NULL THEN NULLIF(1,1) ELSE e.id  END) as empresa ",
            "(case when user.Contabilidade is NULL THEN NULLIF(1,1) ELSE c.id  END) as contabilidade ",
            "(case when c.foto is NULL THEN NULLIF(1,1) ELSE c.foto  END) as foto"
        )
        ->from('Cadastros\Entity\oauth\oauth_users', 'user') 
        ->innerJoin('Cadastros\Entity\oauth\oauth_access_tokens', 'tk')
        ->innerJoin('Cadastros\Entity\Empresa', 'e')
        ->innerJoin('Cadastros\Entity\Contabilidade', 'c')
        ->where('tk.access_token = :token')
        ->andWhere('tk.user_id = user.username')
        ->andWhere("
            (user.Empresa is not NULL AND user.Empresa = e.id) 
            OR 
            (user.Contabilidade is not NULL AND user.Contabilidade = c.id)
            OR
            (user.Contabilidade is NULL AND user.Empresa is NULL)
            "
        )
        ->setParameter('token', $token);

        $resultado = $select->getQuery()->getArrayResult();   

        if($resultado[0]['foto'])
            $resultado[0]['foto'] = base64_encode(stream_get_contents($resultado[0]['foto']));

        $resultado[0]['contabilidade'] = $_GET['id'];

        return $resultado[0];
    }

  
}

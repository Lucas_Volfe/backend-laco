<?php
/**
 * Created by PhpStorm.
 * User: 
 * Date: 
 * Time: 
 */

namespace Cadastros\Service;
use Zend\Crypt\Password\Bcrypt;
use Doctrine\ORM\EntityManager;

use Cadastros\Entity\oauth\oauth_users;

class IdentityService
{
    const ENTITY = 'Cadastros\Entity\oauth\oauth_access_tokens';
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function fetchAll($params)
    {       
        if (isset($_GET["id"])){
            $id = (int) $_GET['id'];
        }        

        $token = (string) $_GET['token'];
        
        $select = $this->em->createQueryBuilder()->select(            
            'tk.user_id', 'tk.expires', 'user.first_name',
            "(case when user.Empresa is NULL THEN NULLIF(1,1) ELSE e.id  END) as empresa ",
            "(case when user.Contabilidade is NULL THEN NULLIF(1,1) ELSE c.id  END) as contabilidade ",            
            "(case when c.foto is NULL THEN NULLIF(1,1) ELSE c.foto  END) as foto",
            "(case when user.Empresa is NULL THEN c.Nome_Fantasia ELSE NULLIF(1,1)  END) as name_contabilidade",
            "(case when user.Contabilidade is NULL THEN e.Nome_Fantasia ELSE NULLIF(1,1) END) as name_empresa"
        )
        ->from('Cadastros\Entity\oauth\oauth_users', 'user') 
        ->innerJoin('Cadastros\Entity\oauth\oauth_access_tokens', 'tk')
        ->innerJoin('Cadastros\Entity\Empresa', 'e')
        ->innerJoin('Cadastros\Entity\Contabilidade', 'c')
        ->where('tk.access_token = :token')
        ->andWhere('tk.user_id = user.username')
        ->andWhere("
            (user.Empresa is not NULL AND user.Empresa = e.id) 
            OR 
            (user.Contabilidade is not NULL AND user.Contabilidade = c.id)
            OR
            (user.Contabilidade is NULL AND user.Empresa is NULL)
            "
        )   
        ->setParameter('token', $token);

        $resultado = $select->getQuery()->getArrayResult();        


        if(isset($id)){
            $select = $this->em->createQueryBuilder()->select('c.Nome_Fantasia', 'c.foto')
            ->from('Cadastros\Entity\Contabilidade', 'c') 
            ->where('c.id = :id')
            ->setParameter('id', $id);
            $result = $select->getQuery()->getArrayResult();       
            $resultado[0]['adminProfile'] = true;
            $resultado[0]['contabilidade'] = $id;
            if($result[0]['foto'])
                $resultado[0]['foto'] = stream_get_contents($result[0]['foto']);
            $resultado[0]['name_contabilidade'] = $result[0]['Nome_Fantasia'];
        }

        return $resultado[0];
    }

}

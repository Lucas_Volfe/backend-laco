<?php
namespace CadastrosAPI\V1\Rest\TakeAccessAccount;

class TakeAccessAccountResourceFactory
{
    public function __invoke($services)
    {
        return new TakeAccessAccountResource($services->get('TakeAccessAccountService'));
    }
}

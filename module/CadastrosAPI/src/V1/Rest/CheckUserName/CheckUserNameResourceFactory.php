<?php
namespace CadastrosAPI\V1\Rest\CheckUserName;

class CheckUserNameResourceFactory
{
    public function __invoke($services)
    {
        return new CheckUserNameResource($services->get('CheckUserNameService'));
    }
}

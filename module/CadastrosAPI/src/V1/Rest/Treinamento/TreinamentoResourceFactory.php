<?php
namespace CadastrosAPI\V1\Rest\Treinamento;

class TreinamentoResourceFactory
{
    public function __invoke($services)
    {
        return new TreinamentoResource($services->get('TreinamentoService'));
    }
}

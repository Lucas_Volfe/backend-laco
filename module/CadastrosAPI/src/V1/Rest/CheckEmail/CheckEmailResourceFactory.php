<?php
namespace CadastrosAPI\V1\Rest\CheckEmail;

class CheckEmailResourceFactory
{
    public function __invoke($services)
    {
        return new CheckEmailResource($services->get('CheckEmailService'));
    }
}

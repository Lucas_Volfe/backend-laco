<?php
namespace CadastrosAPI\V1\Rest\Contabilidade;

class ContabilidadeResourceFactory
{
    public function __invoke($services)
    {
        return new ContabilidadeResource($services->get('ContabilidadeService'));
    }
}

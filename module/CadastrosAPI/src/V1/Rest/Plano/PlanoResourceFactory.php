<?php
namespace CadastrosAPI\V1\Rest\Plano;

class PlanoResourceFactory
{
    public function __invoke($services)
    {
        return new PlanoResource($services->get('PlanoService'));
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: 
 * Date: 
 * Time: 
 */

namespace Upload\Service;

use Upload\Entity\BalancoPatrimonial;
use Doctrine\ORM\EntityManager;

class BalancoPatrimonialService
{
    const ENTITY = 'Upload\Entity\BalancoPatrimonial';
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function save($data)
    {
        $upload = $this->em->find(self::ENTITY, (int) $data['id']);
        $empresa = $this->em->find('Cadastros\Entity\Empresa', $data['Empresa']);   
        
        if (! $upload) {
            $upload = new BalancoPatrimonial();
        }
        $data['Data_Upload'] = new \Datetime( $data['Data_Upload'] );             
        foreach ($data as $key => $value) {               
            ($value == null && $key != 'id') ? $data[$key] = 0 : true ;
        }
        // $tabela = $this->calculosValoresFinais($tabela);                   
    
        $data['Empresa'] = $empresa;
        $upload->setData($data);            
        
        $this->em->persist($upload);     
        $this->em->flush();
    }

    public function fetch($id)
    {
        
        $select = $this->em->createQueryBuilder()->select(
            'bp',
        )->from(self::ENTITY, 'bp')
        ->where('bp.Empresa = :id')
        ->setParameter('id', $id);

        $result = $select->getQuery()->getArrayResult();

        return $result;
    }

    public function fetchAll($params = null)
    {
        $select = $this->em->createQueryBuilder()->select(
            'Uploads.id',
            'Uploads.Mes',
            'Uploads.Ano',
            'Uploads.Lucro_Prejuizo_Liquido',
        )->from(self::ENTITY, 'Uploads');

        $result = $select->getQuery()->getArrayResult();

        return $result;
    }

    public function delete($id)
    {
        $parametro = $this->em->find(self::ENTITY, $id);
        $this->em->remove($parametro);
        $this->em->flush();

        return true;
    }

    public function calculosValoresFinais($data)
    {
        //var_dump($data['Receita_De_Vendas']);die;
        $data['Receitas_Operacionais'] = 
            $data['Receita_De_Vendas'] +
            $data['Receita_De_Fretes_E_Entregas'] +
            $data['Outras_Receitas_1'] +
            $data['Outras_Receitas_2'];

        $data['Deducoes_Da_Receita_Bruta'] =
            $data['Impostos_Sobre_Vendas'] +
            $data['Comissoes_Sobre_Vendas'] +
            $data['Descontos_Incondicionais'] +
            $data['Devolucoes_De_Vendas'] +
            $data['Outras_Deducoes_1'] +
            $data['Outras_Deducoes_2'];

        $data['Receita_Liquida_De_Vendas'] =
            $data['Receitas_Operacionais'] +
            $data['Deducoes_Da_Receita_Bruta'];

        $data['Custos_Operacionais'] =
            $data['Custo_Dos_Produtos_Vendidos'] +
            $data['Custo_Das_Mercadorias_Vendidas'] +
            $data['Custo_Dos_Servicos_Prestados'] +
            $data['Outros_Custos'];

        $data['Lucro_Bruto'] =
            $data['Receita_Liquida_De_Vendas'] +
            $data['Custos_Operacionais'];

        $data['Despesas_Operacionais'] =
            $data['Despesas_Com_Vendas'] +
            $data['Despesas_Administrativas'] +
            $data['Despesas_Tributarias_Gerais'] +
            $data['Outras_Despesas_1'] +
            $data['Outras_Despesas_2'] +
            $data['Outras_Despesas_3'] +
            $data['Outras_Despesas_4'];

        $data['Lucro_Prejuizo_Operacional'] =
            $data['Lucro_Bruto'] +
            $data['Despesas_Operacionais'];

        $data['Receitas_E_Despesas_Financeiras'] =
            $data['Receitas_E_Rendimentos_Financeiros'] +
            $data['Despesas_Financeiras'] +
            $data['Outras_Receitas_Financeiras'] +
            $data['Outras_Despesas_Financeiras'];

        $data['Outras_Receitas_Despesas_Nao_Operacionais'] =
            $data['Outras_Receitas_Nao_Operacionais'] +
            $data['Outras_Despesas_Nao_Operacionais'];

        $data['Lucro_Prejuizo_Liquido'] =
            $data['Lucro_Prejuizo_Operacional'] +
            $data['Receitas_E_Despesas_Financeiras'] +
            $data['Outras_Receitas_Despesas_Nao_Operacionais'];

        $data['Despesas_Com_Investimentos_Emprestimos'] =
            $data['Investimentos_Em_Imobilizado'] +
            $data['Emprestimos_E_Dividas'] +
            $data['Outros_Investimentos_E_Emprestimos'];

        $data['Lucros_Prejuizo_Final'] =
            $data['Lucro_Prejuizo_Liquido'] +
            $data['Despesas_Com_Investimentos_Emprestimos'];

        return $data;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: 
 * Date: 
 * Time: 
 */

namespace Upload\Service;

use Upload\Entity\Uploads;
use Doctrine\ORM\EntityManager;

class UploadsService
{
    const ENTITY = 'Upload\Entity\Uploads';
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    public function saveWithLoop($tabela){


        $upload = $this->em->find(self::ENTITY, (int) $tabela['id']);
        $empresa = $this->em->find('Cadastros\Entity\Empresa', $tabela['Empresa']);   
        if (! $upload) {
            $upload = new Uploads();
        }
        $tabela['Data_Upload'] = new \Datetime( $tabela['Data_Upload'] );             
        foreach ($tabela as $key => $value) {               
            ($value == null && $key != 'id') ? $tabela[$key] = 0 : true ;
        }
        $tabela = $this->calculosValoresFinais($tabela);                   
    
        $empresa->tem_upload = true;
        $tabela['Empresa'] = $empresa;
        $upload->setData($tabela);            
        
        $this->em->persist($empresa);     
        $this->em->persist($upload);     
    }

    public function save($data)
    {
        // var_dump($data);die;
        
        if (count($data) == 1){
            $this->saveWithLoop($data[0]);
        }else{
            foreach ($data as $tabela ){
                $this->saveWithLoop($tabela);           
            }
        }

        
        $this->em->flush();
        
    }

    public function fetch($id)
    {
        
        $upload = $this->em->find(self::ENTITY, (int) $id);
        $empresa = $this->em->find('Cadastros\Entity\Empresa', $upload->Empresa);   
        $contabilidade = $this->em->find('Cadastros\Entity\Contabilidade', $empresa->Contabilidade); 
        $plano = $this->em->find('Cadastros\Entity\Plano', $contabilidade->Plano);

        $empresa = $empresa->getArrayCopy();
        $contabilidade = $contabilidade->getArrayCopy();
        $plano = $plano->getArrayCopy();
        $upload = $upload->getArrayCopy();

        $contabilidade['Plano'] = array_splice($plano,3 ); 
        $empresa['Contabilidade'] = array_splice($contabilidade, 3);
        $upload['Empresa'] = array_splice($empresa, 3);

        return $upload;
    }

    public function fetchAll($params = null)
    {
        $select = $this->em->createQueryBuilder()->select(
            'Uploads.id',
            'Uploads.Mes',
            'Uploads.Ano',
            'Uploads.Lucro_Prejuizo_Liquido',
        )->from(self::ENTITY, 'Uploads');

        $result = $select->getQuery()->getArrayResult();

        return $result;
    }

    public function delete($id)
    {
        $parametro = $this->em->find(self::ENTITY, $id);
        $this->em->remove($parametro);
        $this->em->flush();

        return true;
    }

    public function calculosValoresFinais($data)
    {
        //var_dump($data['Receita_De_Vendas']);die;


        $data['Impostos_Sobre_Vendas'] = $data['Impostos_Sobre_Vendas'] *-1;
        $data['Comissoes_Sobre_Vendas'] = $data['Comissoes_Sobre_Vendas'] *-1;
        $data['Descontos_Incondicionais'] = $data['Descontos_Incondicionais'] *-1;
        $data['Devolucoes_De_Vendas'] = $data['Devolucoes_De_Vendas'] *-1;
        $data['Outras_Deducoes_1'] = $data['Outras_Deducoes_1'] *-1;
        $data['Outras_Deducoes_2'] = $data['Outras_Deducoes_2'] *-1;

        $data['Custo_Dos_Produtos_Vendidos'] = $data['Custo_Dos_Produtos_Vendidos']*-1;
        $data['Custo_Das_Mercadorias_Vendidas'] = $data['Custo_Das_Mercadorias_Vendidas']*-1;
        $data['Custo_Dos_Servicos_Prestados'] = $data['Custo_Dos_Servicos_Prestados']*-1;
        $data['Outros_Custos'] = $data['Outros_Custos']*-1;
        
        $data['Despesas_Financeiras'] = $data['Despesas_Financeiras']*-1;
        $data['Outras_Despesas_Financeiras'] = $data['Outras_Despesas_Financeiras']*-1;
        $data['Outras_Despesas_Nao_Operacionais'] = $data['Outras_Despesas_Nao_Operacionais']*-1;
        $data['Investimentos_Em_Imobilizado'] = $data['Investimentos_Em_Imobilizado']*-1;
        $data['Emprestimos_E_Dividas'] = $data['Emprestimos_E_Dividas']*-1;
        $data['Outros_Investimentos_E_Emprestimos'] = $data['Outros_Investimentos_E_Emprestimos']*-1;

        $data['Despesas_Com_Vendas'] = $data['Despesas_Com_Vendas']*-1;
        $data['Despesas_Administrativas'] = $data['Despesas_Administrativas']*-1;
        $data['Despesas_Tributarias_Gerais'] = $data['Despesas_Tributarias_Gerais']*-1;
        $data['Outras_Despesas_1'] = $data['Outras_Despesas_1']*-1;
        $data['Outras_Despesas_2'] = $data['Outras_Despesas_2']*-1;
        $data['Outras_Despesas_3'] = $data['Outras_Despesas_3']*-1;
        $data['Outras_Despesas_4'] = $data['Outras_Despesas_4']*-1;

        $data['Receitas_Operacionais'] = 
            $data['Receita_De_Vendas'] +
            $data['Receita_De_Fretes_E_Entregas'] +
            $data['Outras_Receitas_1'] +
            $data['Outras_Receitas_2'];

        $data['Deducoes_Da_Receita_Bruta'] =
            $data['Impostos_Sobre_Vendas'] +
            $data['Comissoes_Sobre_Vendas'] +
            $data['Descontos_Incondicionais'] +
            $data['Devolucoes_De_Vendas'] +
            $data['Outras_Deducoes_1'] +
            $data['Outras_Deducoes_2'];

        $data['Receita_Liquida_De_Vendas'] =
            $data['Receitas_Operacionais'] +
            $data['Deducoes_Da_Receita_Bruta'];

        $data['Custos_Operacionais'] =
            $data['Custo_Dos_Produtos_Vendidos'] +
            $data['Custo_Das_Mercadorias_Vendidas'] +
            $data['Custo_Dos_Servicos_Prestados'] +
            $data['Outros_Custos'];

        $data['Lucro_Bruto'] =
            $data['Receita_Liquida_De_Vendas'] +
            $data['Custos_Operacionais'];

        $data['Despesas_Operacionais'] =
            $data['Despesas_Com_Vendas'] +
            $data['Despesas_Administrativas'] +
            $data['Despesas_Tributarias_Gerais'] +
            $data['Outras_Despesas_1'] +
            $data['Outras_Despesas_2'] +
            $data['Outras_Despesas_3'] +
            $data['Outras_Despesas_4'];

        $data['Lucro_Prejuizo_Operacional'] =
            $data['Lucro_Bruto'] +
            $data['Despesas_Operacionais'];

        $data['Receitas_E_Despesas_Financeiras'] =
            $data['Receitas_E_Rendimentos_Financeiros'] +
            $data['Outras_Receitas_Financeiras'] +
            $data['Despesas_Financeiras'] +
            $data['Outras_Despesas_Financeiras'];
            
        $data['Outras_Receitas_E_Despesas_Nao_Operacionais'] =
            $data['Outras_Receitas_Nao_Operacionais'] +
            $data['Outras_Despesas_Nao_Operacionais'];

            
        $data['Lucro_Prejuizo_Liquido'] =
            $data['Lucro_Prejuizo_Operacional'] +
            $data['Receitas_E_Despesas_Financeiras'] +
            $data['Outras_Receitas_E_Despesas_Nao_Operacionais'];
        
        $data['Desembolso_Com_Investimentos_E_Emprestimos'] =
            $data['Investimentos_Em_Imobilizado'] +
            $data['Emprestimos_E_Dividas'] +
            $data['Outros_Investimentos_E_Emprestimos'];

        $data['Lucros_Prejuizo_Final'] =
            $data['Lucro_Prejuizo_Liquido'] +
            $data['Desembolso_Com_Investimentos_E_Emprestimos'];
    
        return $data;
    }

}

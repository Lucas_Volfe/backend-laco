<?php
/**
 * Created by PhpStorm.
 * User: 
 * Date: 
 * Time: 
 */

namespace Upload\Service;

use Upload\Entity\Uploads;
use Doctrine\ORM\EntityManager;

class CheckDisponibilidadeBalancoPatrimonialService
{
    const ENTITY = 'Upload\Entity\BalancoPatrimonial';
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function save($data)
    {
        
       
    }

    public function fetch($empresa)
    {
        $select = $this->em->createQueryBuilder()->select(
            'BalancoPatrimonial.id',
            'BalancoPatrimonial.Mes',
            'BalancoPatrimonial.Ano'
            )
        ->from(self::ENTITY, 'BalancoPatrimonial')
        ->where("BalancoPatrimonial.Empresa = $empresa "); 
        $resultados = $select->getQuery()->getArrayResult();
        
        
        return $resultados;
    }


    public function fetchAll($params = null)
    {
        
    }


    public function delete($id)
    {
       
    }

}

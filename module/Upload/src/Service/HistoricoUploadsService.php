<?php
/**
 * Created by PhpStorm.
 * User: 
 * Date: 
 * Time: 
 */

namespace Upload\Service;

use Upload\Entity\Uploads;
use Doctrine\ORM\EntityManager;

class HistoricoUploadsService
{
    const ENTITY = 'Upload\Entity\Uploads';
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function save($data)
    {
        
       
    }

    public function fetch($empresa)
    {

        $select = $this->em->createQueryBuilder()->select(
            'Uploads'
            )
        ->from(self::ENTITY, 'Uploads')
        ->where("Uploads.Empresa = $empresa "); 
        $resultados = $select->getQuery()->getArrayResult();
        
        foreach ($resultados as $key => $value) {
            $resultados[$key]["Data_Upload"] =  $value["Data_Upload"]->format("d-m-Y");
        } 
        
        return $resultados;
    }


    public function fetchAll($params = null)
    {
        
    }


    public function delete($id)
    {
       
    }

}

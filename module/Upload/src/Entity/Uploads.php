<?php

namespace Upload\Entity;

use Core\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * Uploads
 *
 * @category Upload
 * @package  Entity
 * @author   
 *
 * @ORM\Entity
 * @ORM\Table(name="Uploads_Mensais")
 *
 */

class Uploads extends AbstractEntity
{
    /**
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type = "integer", name = "id")
     *
     * @var integer
     *
     */
    protected $id;
    

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $Ano;
    
    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $Mes;

    /**
     * @ORM\Column(type = "date")
     *
     * @var date
     */
    protected $Data_Upload;

    /**
     * @ORM\ManyToOne(targetEntity="Cadastros\Entity\Empresa", inversedBy="id")
     */
    protected $Empresa;

   /* ------------ Receitas Operacionais --------------- */

    /**
     * @ORM\Column(type="string")
     *
     * @var float
     */
    protected $Receitas_Operacionais;


    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Receita_De_Vendas;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Receita_De_Fretes_E_Entregas;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Outras_Receitas_1;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Outras_Receitas_2;

    /* ------------ Fim Receitas Operacionais --------------- */

    /* ------------ Deduções da Receita Bruta --------------- */

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Deducoes_Da_Receita_Bruta;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Impostos_Sobre_Vendas;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Comissoes_Sobre_Vendas;

     /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Descontos_Incondicionais;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Devolucoes_De_Vendas;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Outras_Deducoes_1;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Outras_Deducoes_2;

    /* ------------ Fim Deduções da Receita Bruta --------------- */

    /* ------------ Receita LÌquida de Vendas --------------- */

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Receita_Liquida_De_Vendas;

    /* ------------ Fim Receita LÌquida de Vendas --------------- */

    /* ------------ Custos Operacionais --------------- */

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Custos_Operacionais;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Custo_Das_Mercadorias_Vendidas;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Custo_Dos_Produtos_Vendidos;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Custo_Dos_Servicos_Prestados;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Outros_Custos;

    /* ------------ Fim Custos Operacionais --------------- */

    /* ------------ Lucro Bruto --------------- */

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Lucro_Bruto;

    /* ------------ Fim Lucro Bruto --------------- */

    /* ------------ Despesas Operacionais --------------- */

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Despesas_Operacionais;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Despesas_Com_Vendas;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Despesas_Administrativas;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Despesas_Tributarias_Gerais;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Outras_Despesas_1;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Outras_Despesas_2;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Outras_Despesas_3;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Outras_Despesas_4;

    /* ------------ Fim Despesas Operacionais --------------- */

    /* ------------ Lucro / PrejuÌzo Operacional --------------- */

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Lucro_Prejuizo_Operacional;

    /* ------------ Fim Lucro / PrejuÌzo Operacional --------------- */

    /* ------------ Receitas e Despesas Financeiras --------------- */

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Receitas_E_Despesas_Financeiras;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Receitas_E_Rendimentos_Financeiros;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Despesas_Financeiras;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Outras_Receitas_Financeiras;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Outras_Despesas_Financeiras;

    /* ------------ Fim Receitas e Despesas Financeiras --------------- */

    /* ------------ Outras Receitas e Despesas Não Operacionais --------------- */

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Outras_Receitas_E_Despesas_Nao_Operacionais;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Outras_Receitas_Nao_Operacionais;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Outras_Despesas_Nao_Operacionais;

    /* ------------ Fim Outras Receitas e Despesas Não Operacionais --------------- */

    /* ------------ Lucro / PrejuÌzo LÌquido --------------- */

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Lucro_Prejuizo_Liquido;

    /* ------------ Fim Lucro / PrejuÌzo LÌquido --------------- */

    /* ------------ Desembolso com Investimentos e Empréstimos --------------- */

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Desembolso_Com_Investimentos_E_Emprestimos;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Investimentos_Em_Imobilizado;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Emprestimos_E_Dividas;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Outros_Investimentos_E_Emprestimos;

    /* ------------ Desembolso com Investimentos e Empréstimos --------------- */

    /* ------------ Lucro / PrejuÌzo Final --------------- */

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $Lucro_Prejuizo_Final;

    /* ------------ Fim Lucro / PrejuÌzo Final --------------- */

   
}

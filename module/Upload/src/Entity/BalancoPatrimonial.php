<?php

namespace Upload\Entity;

use Core\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * Uploads
 *
 * @category Upload
 * @package  Entity
 * @author   
 *
 * @ORM\Entity
 * @ORM\Table(name="Balanco_Patrimonial")
 *
 */

class BalancoPatrimonial extends AbstractEntity
{
    /**
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type = "integer", name = "id")
     *
     * @var integer
     *
     */
    protected $id;
    

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $Ano;
    
    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $Mes;

    /**
     * @ORM\Column(type = "date")
     *
     * @var date
     */
    protected $Data_Upload;

    /**
     * @ORM\ManyToOne(targetEntity="Cadastros\Entity\Empresa", inversedBy="id")
     */
    protected $Empresa;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $circulante_Ativo;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $disponivel;
    
    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $bens_Numerarios;


    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $depositos_Bancarios_A_Vista;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $aplicacoes_De_Liquidez_Imediata;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $clientes;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $duplicatas_A_Receber;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $duplicatas_Descontadas;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $outros_Creditos;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $titulos_A_Receber;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $adiantamentos_A_Funcionarios;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $tributos_A_Recuperar;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $estoques;

     /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $estoque_Diversos;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $nao_Circulante_Ativo;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $realizavel_A_Longo_Prazo;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $outros_Creditos_Longo_Prazo;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $creditos_De_Coligadas_E_Controladas;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $investimentos;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $outros_Investimentos;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $imobilizado;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $imoveis;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $bens_Em_Operacao;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $depreciacao_Amortizacao_Exaustao_Acumulada;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $total_Do_Ativo;

    /* PASSIVO*/

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $circulante_Passivo;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $emprestimos;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $financiamentos_Sistema_Financeiro_Nacional;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $fornecedores;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $fornecedores_Nacionais;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $obrigacoes_Tributarias_Circulante;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $impostos_E_Contribuicoes_A_Recolher;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $tributos_Retidos_A_Recolher;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $obrigacoes_Trabalhistas_E_Prividenciarias;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $obrigacoes_Com_O_Pessoal;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $obrigacoes_Previdenciarias;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $provisoes;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $outras_Obrigacoes;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $contas_A_Pagar;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $contas_Correntes;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $dividendos_Participacoes_Juros_Capital_Proprio;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $juros_Sobre_O_Capital_Proprio;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $nao_Circulante_Passivo;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $obrigacoes_A_Longo_Prazo;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $instituicoes_Financeiras;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $instituicoes_Financeiras_Nao_circulante;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $financiamentos;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $obrigacoes_Tributarias_Nao_Circulante;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $impostos_E_Contribuicoes;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $patrimonio_Liquido;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $capital_Social;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $capital_Subscrito;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $lucros_E_Projuizos_Acumulados_Soma;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $lucros_E_Projuizos_Acumulados;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $lucros_E_Prejuizos_Exercicio;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    protected $total_Do_Patrimonio_Liquido_E_Passivo;


}

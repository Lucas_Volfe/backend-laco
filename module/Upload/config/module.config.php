<?php

namespace Upload;

use Upload\Service\UploadsService;
use Upload\Service\BalancoPatrimonialService;
use Upload\Service\CheckDisponibilidadeService;
use Upload\Service\HistoricoUploadsService;
use Upload\Service\CheckDisponibilidadeBalancoPatrimonialService;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'doctrine' => [
        'driver' => [
            'upload_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'Upload\Entity' => 'upload_entities'
                ]
            ]
        ]
    ],
    'service_manager' => [
        'factories' => [    
            'UploadsService' => function ($sm) {
                return new UploadsService($sm->get('Doctrine\ORM\EntityManager'));
            },
            'BalancoPatrimonialService' => function ($sm) {
                return new BalancoPatrimonialService($sm->get('Doctrine\ORM\EntityManager'));
            },
            'CheckDisponibilidadeService' => function ($sm) {
                return new CheckDisponibilidadeService($sm->get('Doctrine\ORM\EntityManager'));
            },
            'HistoricoUploadsService' => function ($sm) {
                return new HistoricoUploadsService($sm->get('Doctrine\ORM\EntityManager'));
            },
            'CheckDisponibilidadeBalancoPatrimonialService' => function ($sm) {
                return new CheckDisponibilidadeBalancoPatrimonialService($sm->get('Doctrine\ORM\EntityManager'));
            }
        ]
    ]
];

<?php

namespace Tables;

use Tables\Service\ContabilidadeTablesService;
use Tables\Service\TodasEmpresasPorContabilidadeService;
use Tables\Service\UmRegistroEmpresaPorContabilidadeService;
use Tables\Service\UmRegistroUploadPorEmpresaService;
use Tables\Service\DashboardsAdminService;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'doctrine' => [
        'driver' => [
            'tables_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'Tables\Entity' => 'tables_entities'
                ]
            ]
        ]
    ],
    'service_manager' => [
        'factories' => [        
            'ContabilidadeTablesService' => function ($sm) {
                return new ContabilidadeTablesService($sm->get('Doctrine\ORM\EntityManager'));
            },
            'TodasEmpresasPorContabilidadeService' => function ($sm) {
                return new TodasEmpresasPorContabilidadeService($sm->get('Doctrine\ORM\EntityManager'));
            },
            'UmRegistroEmpresaPorContabilidadeService' => function ($sm) {
                return new UmRegistroEmpresaPorContabilidadeService($sm->get('Doctrine\ORM\EntityManager'));
            },
            'UmRegistroUploadPorEmpresaService' => function ($sm) {
                return new UmRegistroUploadPorEmpresaService($sm->get('Doctrine\ORM\EntityManager'));
            },
            'DashboardsAdminService' => function ($sm) {
                return new DashboardsAdminService($sm->get('Doctrine\ORM\EntityManager'));
            }
        ]
    ]
];

<?php
/**
 * Created by PhpStorm.
 * User: 
 * Date: 
 * Time: 
 */

namespace Tables\Service;

use Tables\Entity\ContabilidadeTables;
use Tables\Entity\Table;
use Tables\Entity\Meses;
use Cadastros\Entity\Empresa;
use Upload\Entity\Uploads;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\Join;

class UmRegistroEmpresaPorContabilidadeService
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
   
    public function fetch($contabilidade)
    {

        $select = $this->em->createQueryBuilder()->select(
            'e'
        )
        ->from('Cadastros\Entity\Empresa', 'e') 
        ->andWhere('e.Contabilidade = :contabilidade')
        ->setParameter('contabilidade', $contabilidade);
        $empresas = $select->getQuery()->getArrayResult(); 

        $lastUploads=[];
        foreach ($empresas as $key => $value) {  
            if($value['foto']){
                $value['foto'] = null;
            }
            if($value['tem_upload']){
                $base=[];    
                $select = $this->em->createQueryBuilder()->select(
                    'u.Data_Upload'
                )
                ->from('Upload\Entity\Uploads', 'u') 
                ->andWhere('u.Empresa = :empresa')
                ->orderBy('u.Data_Upload', 'DESC')
                ->setParameter('empresa', $value['id']);
                $resultados = $select->getQuery()->getArrayResult();  
                $value['Data_Upload'] = Date_format($resultados[0]['Data_Upload'], 'd/m/Y');
                array_push($lastUploads, $value);
            }else{
                array_push($lastUploads, $value);
            }

        }
        return $lastUploads;
    }
}

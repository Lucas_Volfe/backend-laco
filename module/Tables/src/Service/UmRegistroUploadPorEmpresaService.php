<?php
/**
 * Created by PhpStorm.
 * User: 
 * Date: 
 * Time: 
 */

namespace Tables\Service;

use Tables\Entity\ContabilidadeTables;
use Tables\Entity\Table;
use Tables\Entity\Meses;
use Cadastros\Entity\Empresa;
use Upload\Entity\Uploads;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\Join;

class UmRegistroUploadPorEmpresaService
{
    private $em;
    private $mesFiltradosPeloUltimoUpload;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
   
    public function fetch($contabilidade)
    {
        $tables = [];

        $select = $this->em->createQueryBuilder()->select(
            'e', 'u.Mes', 'u.Ano'
        )
        ->from('Cadastros\Entity\Empresa', 'e') 
        ->innerJoin('Upload\Entity\Uploads', 'u')
        ->where('e.id = u.Empresa')
        ->andWhere('e.Contabilidade = :contabilidade')
        ->setParameter('contabilidade', $contabilidade);
        $resultados = $select->getQuery()->getArrayResult();
        $empresas=[];
        if ( count($resultados) > 0){
            foreach ($resultados as $value) {
                if( !array_key_exists($value[0]['id'], $empresas) ) {
                    $table = new Table();               
                    $table->empresa_id = $value[0]['id'];
                    $table->nome_fantasia = $value[0]['Nome_Fantasia'];
                    $table->cnpj = $value[0]['Cnpj'];        
                    $table->upload = $value['Mes'].'/'.$value['Ano'];
                    $empresas[ intval($value[0]['id']) ] = $table;
                    //array_push($empresas, $table);
                }
            }
            $return=[];
            foreach( $empresas as $v){
                array_push($return, $v);
            }           
            return $return;  
        }else {
            return [null];
        }
    }  
}

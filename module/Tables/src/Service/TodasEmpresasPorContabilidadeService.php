<?php
/**
 * Created by PhpStorm.
 * User: 
 * Date: 
 * Time: 
 */

namespace Tables\Service;

use Tables\Entity\ContabilidadeTables;
use Tables\Entity\Table;
use Tables\Entity\Meses;
use Cadastros\Entity\Empresa;
use Upload\Entity\Uploads;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\Join;

class TodasEmpresasPorContabilidadeService
{
    private $em;
    private $mesFiltradosPeloUltimoUpload;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
   
    public function fetch($contabilidade)
    {
        $tables = [];

        $select = $this->em->createQueryBuilder()->select(
            'e', 'u.Mes', 'u.Ano'
        )
        ->from('Cadastros\Entity\Empresa', 'e') 
        ->innerJoin('Upload\Entity\Uploads', 'u')
        ->where('e.id = u.Empresa')
        ->andWhere('e.Contabilidade = :contabilidade')
        ->setParameter('contabilidade', $contabilidade);
        $resultados = $select->getQuery()->getArrayResult();        
        
        if ( count($resultados) > 0){

            foreach ($resultados as $value) {
                $table = new Table();  
                $table->id = $value[0]['id'];             
                $table->Nome_Fantasia = $value[0]['Nome_Fantasia'];
                $table->Cnpj = $value[0]['Cnpj'];        
                $table->Upload = $value['Mes'].'/'.$value['Ano'];
                  
                array_push($tables, $table);
            }

           
            return $tables;  
        }else {
            return [null];
        }
    }    

     
}

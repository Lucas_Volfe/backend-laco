<?php
/**
 * Created by PhpStorm.
 * User: 
 * Date: 
 * Time: 
 */

namespace Tables\Service;

use Tables\Entity\ContabilidadeTables;
use Tables\Entity\Table;
use Tables\Entity\Meses;
use Cadastros\Entity\Empresa;
use Upload\Entity\Uploads;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\Join;

class ContabilidadeTablesService
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
   
    public function fetch($contabilidade)
    {
        $tables = [];
        $meses = new Meses();

        $select = $this->em->createQueryBuilder()->select(
            'e', 'u.Data_Upload'
        )
        ->from('Cadastros\Entity\Empresa', 'e') 
        ->innerJoin('Upload\Entity\Uploads', 'u')
        ->where('e.id = u.Empresa')
        ->andWhere('e.Contabilidade = :contabilidade')
        ->setParameter('contabilidade', $contabilidade);
        $resultados = $select->getQuery()->getArrayResult();        
        
        if ( count($resultados) > 0){
            foreach ($resultados as $value) {
                $table = new Table();
                foreach ($value as $v){
                    if ( gettype($v) == 'array' )
                    {
                        $table->empresa = $v['Nome_Fantasia'];
                        $table->cnpj = $v['Cnpj'];
                    }else
                    {
                        $table->upload = $meses->meses[date_format($v, 'm')];
                    }
                }
                array_push($tables, $table);
            }
            return $tables;  
        } else {
            return [null];
        }
    }    
}

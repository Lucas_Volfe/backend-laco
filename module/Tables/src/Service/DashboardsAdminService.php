<?php
/**
 * Created by PhpStorm.
 * User: 
 * Date: 
 * Time: 
 */

namespace Tables\Service;

use Tables\Entity\ContabilidadeTables;
use Tables\Entity\Table;
use Tables\Entity\Meses;
use Cadastros\Entity\Empresa;
use Upload\Entity\Uploads;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\Join;

class DashboardsAdminService
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
   
    public function fetchAll($params)
    {
        
        $select = $this->em->createQueryBuilder()->select(
            //'count(e.id)',
            'e.status', 'e.data_inscricao',
        )->from('Cadastros\Entity\Empresa', 'e'); 
        $empresas = $select->getQuery()->getArrayResult();

        $select = $this->em->createQueryBuilder()->select(
            'c.status', 'c.data_inscricao'
        )->from('Cadastros\Entity\Contabilidade', 'c'); 
        $contabilidades = $select->getQuery()->getArrayResult();

        $results = [];
        $ativos=0;
        $testes=0;
        $desabilitados=0;

        $datesLabels[0] = '...';        
        $datesLabels[1] = date("m/Y",strtotime("-4 month"));
        $datesLabels[2] = date("m/Y",strtotime("-3 month"));
        $datesLabels[3] = date("m/Y",strtotime("-2 month"));
        $datesLabels[4] = date("m/Y",strtotime("-1 month"));
        $datesLabels[5] = date("m/Y");
        
        $datesValues[0] = 0;        
        $datesValues[1] = 0;
        $datesValues[2] = 0;
        $datesValues[3] = 0;
        $datesValues[4] = 0;
        $datesValues[5] = 0;

        foreach ($empresas as $key => $value) {
            if($value['status'] == 'Ativo'){
                $ativos++;
            }else if($value['status'] == 'Teste'){
                $testes++;
            }else if($value['status'] == 'Desabilitado'){
                $desabilitados++; 
            }

            $diff = date_diff( new \DateTime(), $value['data_inscricao'], 'd/m/Y' );
            $total_diff = intval($diff->format("%R%a"));

            if ($total_diff > 180){
                $datesValues[0]++;
            }else if($total_diff > 150){
                $datesValues[1]++;
            }else if($total_diff > 120){
                $datesValues[2]++;
            }else if($total_diff > 90){
                $datesValues[3]++;
            }else if($total_diff > 60){
                $datesValues[4]++;
            }else{
                $datesValues[5]++;
            }
        
        }
        $results['empresas_ativas'] = $ativos;
        $results['empresas_teste'] = $testes;
        $results['empresas_desabilitadas'] = $desabilitados;

        foreach ($contabilidades as $key => $value) {
            if($value['status'] == 'Ativo'){
                $ativos++;
            }else if($value['status'] == 'Teste'){
                $testes++;
            }else if($value['status'] == 'Desabilitado'){ 
                $desabilitados++;
            }

            $diff = date_diff( new \DateTime(), $value['data_inscricao'], 'd/m/Y' );
            $total_diff = intval($diff->format("%R%a"));

            if ($total_diff > 180){
                $datesValues[0]++;
            }else if($total_diff > 150){
                $datesValues[1]++;
            }else if($total_diff > 120){
                $datesValues[2]++;
            }else if($total_diff > 90){
                $datesValues[3]++;
            }else if($total_diff > 60){
                $datesValues[4]++;
            }else{
                $datesValues[5]++;
            }
        }
        $results['contabilidades_ativas'] = $ativos - $results['empresas_ativas'];
        $results['contabilidades_teste'] = $testes - $results['empresas_teste'];
        $results['contabilidades_desabilitadas'] = $desabilitados - $results['empresas_desabilitadas'];
        $results['total_clientes'] = count($empresas) + count($contabilidades);
        $results['line_chart_values'] = $datesValues;
        $results['line_chart_labels'] = $datesLabels;
        
        return $results;   
    }    
}


/*

$select = $this->em->createQueryBuilder()->select(
            //'count(e.id)',
            'e.status',
        )->from('Cadastros\Entity\Empresa', 'e'); 
        $empresas = $select->getQuery()->getArrayResult();

        $select = $this->em->createQueryBuilder()->select(
            'c.status',
        )->from('Cadastros\Entity\Contabilidade', 'c'); 
        $contabilidades = $select->getQuery()->getArrayResult();


        $results = [];
        $ativos=0;
        $testes=0;
        $desabilitados=0;

        foreach ($empresas as $key => $value) {
            if($value['status'] == 'Ativo'){
                $ativos++;
            }else if($value['status'] == 'Teste'){
                $testes++;
            }else if($value['status'] == 'Desabilitado'){
                $desabilitados++; 
            }
        }
        $results['empresas_ativas'] = $ativos;
        $results['empresas_teste'] = $testes;
        $results['empresas_desabilitadas'] = $desabilitados;

        foreach ($contabilidades as $key => $value) {
            if($value['status'] == 'Ativo'){
                $ativos++;
            }else if($value['status'] == 'Teste'){
                $testes++;
            }else if($value['status'] == 'Desabilitado'){ 
                $desabilitados++;
            }
        }
        $results['contabilidades_ativas'] = $ativos - $results['empresas_ativas'];
        $results['contabilidades_teste'] = $testes - $results['empresas_teste'];
        $results['contabilidades_desabilitadas'] = $desabilitados - $results['empresas_desabilitadas'];
        $results['total_clientes'] = count($empresas) + count($contabilidades);
        
        return $results;   */
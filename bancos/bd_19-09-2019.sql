-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 20/09/2019 às 00:00
-- Versão do servidor: 5.7.27
-- Versão do PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `wwlaco_site`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `Contabilidade`
--

CREATE TABLE `Contabilidade` (
  `id` int(11) NOT NULL,
  `Cnpj` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Nome_Fantasia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Razao_Social` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Plano_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `Contabilidade`
--

INSERT INTO `Contabilidade` (`id`, `Cnpj`, `Nome_Fantasia`, `Razao_Social`, `Plano_id`) VALUES
(1, 'Cnpj', 'Nome_Fantasia', 'Razao_Social', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `Empresa`
--

CREATE TABLE `Empresa` (
  `id` int(11) NOT NULL,
  `Cnpj` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Nome_Fantasia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Razao_Social` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Contabilidade_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `Empresa`
--

INSERT INTO `Empresa` (`id`, `Cnpj`, `Nome_Fantasia`, `Razao_Social`, `Contabilidade_id`) VALUES
(1, 'Cnpj', 'Nome_Fantasia', 'Razao_Social', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `LucroLiquidoAcumulado`
--

CREATE TABLE `LucroLiquidoAcumulado` (
  `id` int(11) NOT NULL,
  `Empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Total` double NOT NULL,
  `Lucro_Prejuizo_Liquido` double NOT NULL,
  `Receitas_Operacionais` double NOT NULL,
  `Diferenca_Porcentagem` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `LucroLiquidoMensal`
--

CREATE TABLE `LucroLiquidoMensal` (
  `id` int(11) NOT NULL,
  `Empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Mes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Ano` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Lucro_Prejuizo_Liquido` double NOT NULL,
  `Receitas_Operacionais` double NOT NULL,
  `Diferenca_Porcentagem` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE `migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
('20190912130546'),
('20190919194518');

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `access_token` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expires` datetime DEFAULT NULL,
  `scope` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_authorization_codes`
--

CREATE TABLE `oauth_authorization_codes` (
  `authorization_code` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `redirect_uri` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expires` datetime NOT NULL,
  `scope` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_token` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `client_id` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `client_secret` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `redirect_uri` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `grant_types` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scope` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_jwt`
--

CREATE TABLE `oauth_jwt` (
  `client_id` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_key` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `refresh_token` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expires` datetime NOT NULL,
  `scope` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_scopes`
--

CREATE TABLE `oauth_scopes` (
  `scope` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_id` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_default` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_users`
--

CREATE TABLE `oauth_users` (
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Contabilidade_id` int(11) DEFAULT NULL,
  `Empresa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `oauth_users`
--

INSERT INTO `oauth_users` (`username`, `password`, `first_name`, `last_name`, `email`, `Contabilidade_id`, `Empresa_id`) VALUES
('teste123', '$2y$10$S3dS7vhIxQE9.Ww9oCnknebX5XyUoXS3mpJtaXfiyLQe.mcxVoYEy', 'x', NULL, 'aaa@aaa.com.br', 1, 1),
('teste1234', '$2y$10$KvXrxT.4MbT1Ts/mo0PjvuIE2RVSokJT1njN1dpuU/ZzIEd7C5Lxa', 'x', NULL, 'aaa@aaa.com.br', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `Plano`
--

CREATE TABLE `Plano` (
  `id` int(11) NOT NULL,
  `Nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Descricao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Limite_De_Empresas` int(11) NOT NULL,
  `Preco` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `Plano`
--

INSERT INTO `Plano` (`id`, `Nome`, `Descricao`, `Limite_De_Empresas`, `Preco`) VALUES
(1, 'Nome', 'Descricao', 1, 99);

-- --------------------------------------------------------

--
-- Estrutura para tabela `Treinamento`
--

CREATE TABLE `Treinamento` (
  `id` int(11) NOT NULL,
  `Titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Descricao` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `Treinamento`
--

INSERT INTO `Treinamento` (`id`, `Titulo`, `Status`, `Descricao`) VALUES
(2, 'Titulo', 'Status', 'Descricao'),
(3, 'Titulo', 'Status', 'Descricao');

-- --------------------------------------------------------

--
-- Estrutura para tabela `Uploads_Mensais`
--

CREATE TABLE `Uploads_Mensais` (
  `id` int(11) NOT NULL,
  `Ano` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Mes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Data_Upload` date NOT NULL,
  `Receitas_Operacionais` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Receita_De_Vendas` double NOT NULL,
  `Receita_De_Fretes_E_Entregas` double NOT NULL,
  `Outras_Receitas_1` double NOT NULL,
  `Outras_Receitas_2` double NOT NULL,
  `Deducoes_Da_Receita_Bruta` double NOT NULL,
  `Impostos_Sobre_Vendas` double NOT NULL,
  `Comissoes_Sobre_Vendas` double NOT NULL,
  `Descontos_Incondicionais` double NOT NULL,
  `Devolucoes_De_Vendas` double NOT NULL,
  `Outras_Deducoes_1` double NOT NULL,
  `Outras_Deducoes_2` double NOT NULL,
  `Receita_Liquida_De_Vendas` double NOT NULL,
  `Custos_Operacionais` double NOT NULL,
  `Custo_Das_Mercadorias_Vendidas` double NOT NULL,
  `Custo_Dos_Produtos_Vendidos` double NOT NULL,
  `Custo_Dos_Servicos_Prestados` double NOT NULL,
  `Outros_Custos` double NOT NULL,
  `Lucro_Bruto` double NOT NULL,
  `Despesas_Operacionais` double NOT NULL,
  `Despesas_Com_Vendas` double NOT NULL,
  `Despesas_Administrativas` double NOT NULL,
  `Despesas_Tributarias_Gerais` double NOT NULL,
  `Outras_Despesas_1` double NOT NULL,
  `Outras_Despesas_2` double NOT NULL,
  `Outras_Despesas_3` double NOT NULL,
  `Outras_Despesas_4` double NOT NULL,
  `Lucro_Prejuizo_Operacional` double NOT NULL,
  `Receitas_E_Despesas_Financeiras` double NOT NULL,
  `Receitas_E_Rendimentos_Financeiros` double NOT NULL,
  `Despesas_Financeiras` double NOT NULL,
  `Outras_Receitas_Financeiras` double NOT NULL,
  `Outras_Despesas_Financeiras` double NOT NULL,
  `Outras_Receitas_E_Despesas_Nao_Operacionais` double NOT NULL,
  `Outras_Receitas_Nao_Operacionais` double NOT NULL,
  `Outras_Despesas_Nao_Operacionais` double NOT NULL,
  `Lucro_Prejuizo_Liquido` double NOT NULL,
  `Desembolso_Com_Investimentos_E_Emprestimos` double NOT NULL,
  `Investimentos_Em_Imobilizado` double NOT NULL,
  `Emprestimos_E_Dividas` double NOT NULL,
  `Outros_Investimentos_E_Emprestimos` double NOT NULL,
  `Lucro_Prejuizo_Final` double NOT NULL,
  `Empresa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `Uploads_Mensais`
--

INSERT INTO `Uploads_Mensais` (`id`, `Ano`, `Mes`, `Data_Upload`, `Receitas_Operacionais`, `Receita_De_Vendas`, `Receita_De_Fretes_E_Entregas`, `Outras_Receitas_1`, `Outras_Receitas_2`, `Deducoes_Da_Receita_Bruta`, `Impostos_Sobre_Vendas`, `Comissoes_Sobre_Vendas`, `Descontos_Incondicionais`, `Devolucoes_De_Vendas`, `Outras_Deducoes_1`, `Outras_Deducoes_2`, `Receita_Liquida_De_Vendas`, `Custos_Operacionais`, `Custo_Das_Mercadorias_Vendidas`, `Custo_Dos_Produtos_Vendidos`, `Custo_Dos_Servicos_Prestados`, `Outros_Custos`, `Lucro_Bruto`, `Despesas_Operacionais`, `Despesas_Com_Vendas`, `Despesas_Administrativas`, `Despesas_Tributarias_Gerais`, `Outras_Despesas_1`, `Outras_Despesas_2`, `Outras_Despesas_3`, `Outras_Despesas_4`, `Lucro_Prejuizo_Operacional`, `Receitas_E_Despesas_Financeiras`, `Receitas_E_Rendimentos_Financeiros`, `Despesas_Financeiras`, `Outras_Receitas_Financeiras`, `Outras_Despesas_Financeiras`, `Outras_Receitas_E_Despesas_Nao_Operacionais`, `Outras_Receitas_Nao_Operacionais`, `Outras_Despesas_Nao_Operacionais`, `Lucro_Prejuizo_Liquido`, `Desembolso_Com_Investimentos_E_Emprestimos`, `Investimentos_Em_Imobilizado`, `Emprestimos_E_Dividas`, `Outros_Investimentos_E_Emprestimos`, `Lucro_Prejuizo_Final`, `Empresa_id`) VALUES
(1, '18', 'jul', '2019-09-12', '398608.59', 398608.59, 0, 0, 0, 49094.53, 25353.14, 2723.25, 0, 16538.34, 4479.8, 0, 349514.06, 219135.55, 145391.78, 0, 73743.77, 0, 130378.51, 92898.51, 0, 0, 0, 92898.51, 0, 0, 0, 37480, -5643.16, 7048.77, 12691.93, 0, 0, -19.85, 924.73, 944.58, 31816.99, 13362.3, 491.08, 12871.22, 0, 18454.69, 1),
(2, '18', 'ago', '2019-09-12', '340462.75', 340462.75, 0, 0, 0, 52171.05, 33205.55, 3774.6, 0, 13946.56, 1244.34, 0, 288291.7, 179814.22, 146346.66, 0, 33467.56, 0, 108477.48, 93893.49, 0, 0, 0, 93893.49, 0, 0, 0, 14583.99, -11273.95, 2823.01, 14096.96, 0, 0, 338.15, 592.67, 254.52, 3648.19, 43449.39, 491.08, 42958.31, 0, -39801.2, NULL),
(3, '18', 'set', '2019-09-12', '413880.67', 413880.67, 0, 0, 0, 62294.8, 31012.48, 4108.88, 0, 26131.74, 1041.7, 0, 351585.87, 219983.09, 145073.67, 0, 74909.42, 0, 131602.78, 142394.29, 0, 0, 0, 142394.29, 0, 0, 0, -10791.51, -15158.01, 1554.04, 16712.05, 0, 0, -303.49, 50, 353.49, -26253.01, 13532.01, 491.08, 13040.93, 0, -39785.02, NULL),
(4, '18\r', 'out', '2019-09-12', '501951.13', 501951.13, 0, 0, 0, 92082.48, 37243.38, 2415.12, 0, 49043.76, 3380.22, 0, 409868.65, 224778.35, 201302.43, 0, 23475.92, 0, 185090.3, 108724.86, 0, 0, 0, 108724.86, 0, 0, 0, 76365.44, -9927.77, 4156.16, 14083.93, 0, 0, -550.39, 65.51, 615.9, 65887.28, 38610.35, 491.08, 38119.27, 0, 27276.93, NULL);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `Contabilidade`
--
ALTER TABLE `Contabilidade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_4501CB7F63FDE49A` (`Plano_id`);

--
-- Índices de tabela `Empresa`
--
ALTER TABLE `Empresa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_776A63CC66E5D40B` (`Contabilidade_id`);

--
-- Índices de tabela `LucroLiquidoAcumulado`
--
ALTER TABLE `LucroLiquidoAcumulado`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `LucroLiquidoMensal`
--
ALTER TABLE `LucroLiquidoMensal`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`version`);

--
-- Índices de tabela `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`access_token`);

--
-- Índices de tabela `oauth_authorization_codes`
--
ALTER TABLE `oauth_authorization_codes`
  ADD PRIMARY KEY (`authorization_code`);

--
-- Índices de tabela `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Índices de tabela `oauth_jwt`
--
ALTER TABLE `oauth_jwt`
  ADD PRIMARY KEY (`client_id`);

--
-- Índices de tabela `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`refresh_token`);

--
-- Índices de tabela `oauth_scopes`
--
ALTER TABLE `oauth_scopes`
  ADD PRIMARY KEY (`scope`);

--
-- Índices de tabela `oauth_users`
--
ALTER TABLE `oauth_users`
  ADD PRIMARY KEY (`username`),
  ADD KEY `IDX_93804FF866E5D40B` (`Contabilidade_id`),
  ADD KEY `IDX_93804FF81D431A41` (`Empresa_id`);

--
-- Índices de tabela `Plano`
--
ALTER TABLE `Plano`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `Treinamento`
--
ALTER TABLE `Treinamento`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `Uploads_Mensais`
--
ALTER TABLE `Uploads_Mensais`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5418943E1D431A41` (`Empresa_id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `Contabilidade`
--
ALTER TABLE `Contabilidade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `Empresa`
--
ALTER TABLE `Empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `LucroLiquidoAcumulado`
--
ALTER TABLE `LucroLiquidoAcumulado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `LucroLiquidoMensal`
--
ALTER TABLE `LucroLiquidoMensal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `Plano`
--
ALTER TABLE `Plano`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `Treinamento`
--
ALTER TABLE `Treinamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `Uploads_Mensais`
--
ALTER TABLE `Uploads_Mensais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `Contabilidade`
--
ALTER TABLE `Contabilidade`
  ADD CONSTRAINT `FK_4501CB7F63FDE49A` FOREIGN KEY (`Plano_id`) REFERENCES `Plano` (`id`);

--
-- Restrições para tabelas `Empresa`
--
ALTER TABLE `Empresa`
  ADD CONSTRAINT `FK_776A63CC66E5D40B` FOREIGN KEY (`Contabilidade_id`) REFERENCES `Contabilidade` (`id`);

--
-- Restrições para tabelas `oauth_users`
--
ALTER TABLE `oauth_users`
  ADD CONSTRAINT `FK_93804FF81D431A41` FOREIGN KEY (`Empresa_id`) REFERENCES `Empresa` (`id`),
  ADD CONSTRAINT `FK_93804FF866E5D40B` FOREIGN KEY (`Contabilidade_id`) REFERENCES `Contabilidade` (`id`);

--
-- Restrições para tabelas `Uploads_Mensais`
--
ALTER TABLE `Uploads_Mensais`
  ADD CONSTRAINT `FK_5418943E1D431A41` FOREIGN KEY (`Empresa_id`) REFERENCES `Empresa` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

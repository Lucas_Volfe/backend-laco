-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 02/10/2019 às 14:08
-- Versão do servidor: 5.7.27
-- Versão do PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `wwlaco_site`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `Contabilidade`
--

CREATE TABLE `Contabilidade` (
  `id` int(11) NOT NULL,
  `Nome_Fantasia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Razao_Social` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Cnpj` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Plano_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `Contabilidade`
--

INSERT INTO `Contabilidade` (`id`, `Nome_Fantasia`, `Razao_Social`, `Cnpj`, `Plano_id`) VALUES
(1, 'contabilidade', 'razao social', '9999', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `Empresa`
--

CREATE TABLE `Empresa` (
  `id` int(11) NOT NULL,
  `Nome_Fantasia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Razao_Social` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Cnpj` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Contabilidade_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `Empresa`
--

INSERT INTO `Empresa` (`id`, `Nome_Fantasia`, `Razao_Social`, `Cnpj`, `Contabilidade_id`) VALUES
(1, 'empresa', 'razp', '8978987987', 1),
(2, 'xxxxxx', 'sdasdasd', '567356853', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `Logs`
--

CREATE TABLE `Logs` (
  `id` int(11) NOT NULL,
  `Data_Execucao` date NOT NULL,
  `Original` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:object)',
  `Alteracao` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:object)',
  `Tipo_Log` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Usuario_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `Logs`
--

INSERT INTO `Logs` (`id`, `Data_Execucao`, `Original`, `Alteracao`, `Tipo_Log`, `Usuario_id`) VALUES
(1, '2019-09-26', 'O:34:\"Cadastros\\Entity\\oauth\\oauth_users\":8:{s:5:\"\0*\0id\";i:1;s:11:\"\0*\0username\";s:4:\"user\";s:11:\"\0*\0password\";s:60:\"$2y$10$G4D/ycncYKQQ3hO2/.GFZeHDIVh3TribcKiCtBznuROeN76ywxVue\";s:13:\"\0*\0first_name\";s:3:\"xxx\";s:8:\"\0*\0email\";s:14:\"aaa@aaa.com.br\";s:10:\"\0*\0Empresa\";O:24:\"Cadastros\\Entity\\Empresa\":5:{s:5:\"\0*\0id\";i:1;s:16:\"\0*\0Contabilidade\";O:61:\"DoctrineORMModule\\Proxy\\__CG__\\Cadastros\\Entity\\Contabilidade\":6:{s:17:\"__isInitialized__\";b:0;s:5:\"\0*\0id\";i:1;s:8:\"\0*\0Plano\";N;s:16:\"\0*\0Nome_Fantasia\";N;s:15:\"\0*\0Razao_Social\";N;s:7:\"\0*\0Cnpj\";N;}s:16:\"\0*\0Nome_Fantasia\";s:7:\"empresa\";s:15:\"\0*\0Razao_Social\";s:4:\"razp\";s:7:\"\0*\0Cnpj\";s:10:\"8978987987\";}s:7:\"\0*\0Role\";N;s:13:\"Contabilidade\";N;}', 'N;', 'post', NULL),
(2, '2019-10-01', 'O:34:\"Cadastros\\Entity\\oauth\\oauth_users\":8:{s:5:\"\0*\0id\";i:3;s:11:\"\0*\0username\";s:3:\"123\";s:11:\"\0*\0password\";s:60:\"$2y$10$Dh0MATuPoQBI3tzNru5sX.xGDzx1kpmzqiaX6sD4joUGCfB1Y49W.\";s:13:\"\0*\0first_name\";N;s:8:\"\0*\0email\";s:5:\"123@2\";s:10:\"\0*\0Empresa\";O:24:\"Cadastros\\Entity\\Empresa\":5:{s:5:\"\0*\0id\";i:1;s:16:\"\0*\0Contabilidade\";O:61:\"DoctrineORMModule\\Proxy\\__CG__\\Cadastros\\Entity\\Contabilidade\":6:{s:17:\"__isInitialized__\";b:0;s:5:\"\0*\0id\";i:1;s:8:\"\0*\0Plano\";N;s:16:\"\0*\0Nome_Fantasia\";N;s:15:\"\0*\0Razao_Social\";N;s:7:\"\0*\0Cnpj\";N;}s:16:\"\0*\0Nome_Fantasia\";s:7:\"empresa\";s:15:\"\0*\0Razao_Social\";s:4:\"razp\";s:7:\"\0*\0Cnpj\";s:10:\"8978987987\";}s:7:\"\0*\0Role\";N;s:13:\"Contabilidade\";N;}', 'N;', 'post', NULL),
(3, '2019-10-01', 'O:34:\"Cadastros\\Entity\\oauth\\oauth_users\":8:{s:5:\"\0*\0id\";i:4;s:11:\"\0*\0username\";s:3:\"123\";s:11:\"\0*\0password\";s:60:\"$2y$10$z5IdLyKL6jFKXQGB/LAmLeRNUmvwjlSLWdFVYX.KoEYstuWoNQCpu\";s:13:\"\0*\0first_name\";N;s:8:\"\0*\0email\";s:6:\"sdfs@d\";s:10:\"\0*\0Empresa\";O:24:\"Cadastros\\Entity\\Empresa\":5:{s:5:\"\0*\0id\";i:1;s:16:\"\0*\0Contabilidade\";O:61:\"DoctrineORMModule\\Proxy\\__CG__\\Cadastros\\Entity\\Contabilidade\":6:{s:17:\"__isInitialized__\";b:0;s:5:\"\0*\0id\";i:1;s:8:\"\0*\0Plano\";N;s:16:\"\0*\0Nome_Fantasia\";N;s:15:\"\0*\0Razao_Social\";N;s:7:\"\0*\0Cnpj\";N;}s:16:\"\0*\0Nome_Fantasia\";s:7:\"empresa\";s:15:\"\0*\0Razao_Social\";s:4:\"razp\";s:7:\"\0*\0Cnpj\";s:10:\"8978987987\";}s:7:\"\0*\0Role\";N;s:13:\"Contabilidade\";N;}', 'N;', 'post', NULL),
(4, '2019-10-01', 'O:34:\"Cadastros\\Entity\\oauth\\oauth_users\":8:{s:5:\"\0*\0id\";i:5;s:11:\"\0*\0username\";s:2:\"12\";s:11:\"\0*\0password\";s:60:\"$2y$10$hvXR6TdliOhgXYo47KEwDuKM.52RuzWc3/wKBcAnKrSOyFlmzrvRu\";s:13:\"\0*\0first_name\";N;s:8:\"\0*\0email\";s:0:\"\";s:10:\"\0*\0Empresa\";O:24:\"Cadastros\\Entity\\Empresa\":5:{s:5:\"\0*\0id\";i:1;s:16:\"\0*\0Contabilidade\";O:61:\"DoctrineORMModule\\Proxy\\__CG__\\Cadastros\\Entity\\Contabilidade\":6:{s:17:\"__isInitialized__\";b:0;s:5:\"\0*\0id\";i:1;s:8:\"\0*\0Plano\";N;s:16:\"\0*\0Nome_Fantasia\";N;s:15:\"\0*\0Razao_Social\";N;s:7:\"\0*\0Cnpj\";N;}s:16:\"\0*\0Nome_Fantasia\";s:7:\"empresa\";s:15:\"\0*\0Razao_Social\";s:4:\"razp\";s:7:\"\0*\0Cnpj\";s:10:\"8978987987\";}s:7:\"\0*\0Role\";N;s:13:\"Contabilidade\";N;}', 'N;', 'post', NULL),
(5, '2019-10-01', 'O:34:\"Cadastros\\Entity\\oauth\\oauth_users\":8:{s:5:\"\0*\0id\";i:6;s:11:\"\0*\0username\";s:2:\"22\";s:11:\"\0*\0password\";s:60:\"$2y$10$nImtimjTsDZaOMLRfEKOj.Ytw2YALc48xoy5C.aKK/2lqQR5vqzlu\";s:13:\"\0*\0first_name\";N;s:8:\"\0*\0email\";s:0:\"\";s:10:\"\0*\0Empresa\";O:24:\"Cadastros\\Entity\\Empresa\":5:{s:5:\"\0*\0id\";i:1;s:16:\"\0*\0Contabilidade\";O:61:\"DoctrineORMModule\\Proxy\\__CG__\\Cadastros\\Entity\\Contabilidade\":6:{s:17:\"__isInitialized__\";b:0;s:5:\"\0*\0id\";i:1;s:8:\"\0*\0Plano\";N;s:16:\"\0*\0Nome_Fantasia\";N;s:15:\"\0*\0Razao_Social\";N;s:7:\"\0*\0Cnpj\";N;}s:16:\"\0*\0Nome_Fantasia\";s:7:\"empresa\";s:15:\"\0*\0Razao_Social\";s:4:\"razp\";s:7:\"\0*\0Cnpj\";s:10:\"8978987987\";}s:7:\"\0*\0Role\";N;s:13:\"Contabilidade\";N;}', 'N;', 'post', NULL),
(6, '2019-10-01', 'O:34:\"Cadastros\\Entity\\oauth\\oauth_users\":8:{s:5:\"\0*\0id\";i:7;s:11:\"\0*\0username\";s:2:\"22\";s:11:\"\0*\0password\";s:60:\"$2y$10$KAzlcDQQWXD9uq.OdsDwPeYTZKHL1gVcuUcemo.K2yOUHHX8IBD.G\";s:13:\"\0*\0first_name\";N;s:8:\"\0*\0email\";s:0:\"\";s:10:\"\0*\0Empresa\";O:24:\"Cadastros\\Entity\\Empresa\":5:{s:5:\"\0*\0id\";i:1;s:16:\"\0*\0Contabilidade\";O:61:\"DoctrineORMModule\\Proxy\\__CG__\\Cadastros\\Entity\\Contabilidade\":6:{s:17:\"__isInitialized__\";b:0;s:5:\"\0*\0id\";i:1;s:8:\"\0*\0Plano\";N;s:16:\"\0*\0Nome_Fantasia\";N;s:15:\"\0*\0Razao_Social\";N;s:7:\"\0*\0Cnpj\";N;}s:16:\"\0*\0Nome_Fantasia\";s:7:\"empresa\";s:15:\"\0*\0Razao_Social\";s:4:\"razp\";s:7:\"\0*\0Cnpj\";s:10:\"8978987987\";}s:7:\"\0*\0Role\";N;s:13:\"Contabilidade\";N;}', 'N;', 'post', NULL),
(7, '2019-10-01', 'O:34:\"Cadastros\\Entity\\oauth\\oauth_users\":8:{s:5:\"\0*\0id\";i:8;s:11:\"\0*\0username\";s:0:\"\";s:11:\"\0*\0password\";s:60:\"$2y$10$5RPpq3fUzf6kA069h4JgDexrwJAWyjOOGVSk/.yPHQzYFn/NVRerG\";s:13:\"\0*\0first_name\";N;s:8:\"\0*\0email\";s:0:\"\";s:10:\"\0*\0Empresa\";O:24:\"Cadastros\\Entity\\Empresa\":5:{s:5:\"\0*\0id\";i:1;s:16:\"\0*\0Contabilidade\";O:61:\"DoctrineORMModule\\Proxy\\__CG__\\Cadastros\\Entity\\Contabilidade\":6:{s:17:\"__isInitialized__\";b:0;s:5:\"\0*\0id\";i:1;s:8:\"\0*\0Plano\";N;s:16:\"\0*\0Nome_Fantasia\";N;s:15:\"\0*\0Razao_Social\";N;s:7:\"\0*\0Cnpj\";N;}s:16:\"\0*\0Nome_Fantasia\";s:7:\"empresa\";s:15:\"\0*\0Razao_Social\";s:4:\"razp\";s:7:\"\0*\0Cnpj\";s:10:\"8978987987\";}s:7:\"\0*\0Role\";N;s:13:\"Contabilidade\";N;}', 'N;', 'post', NULL),
(8, '2019-10-01', 'O:34:\"Cadastros\\Entity\\oauth\\oauth_users\":8:{s:5:\"\0*\0id\";i:9;s:11:\"\0*\0username\";s:4:\"1233\";s:11:\"\0*\0password\";s:60:\"$2y$10$JubEDJ2T2zJk2SDAvqzdHeEaEK0ZDGssSB6oUb4hqubjieyeiMqRK\";s:13:\"\0*\0first_name\";N;s:8:\"\0*\0email\";s:5:\"123@2\";s:10:\"\0*\0Empresa\";O:24:\"Cadastros\\Entity\\Empresa\":5:{s:5:\"\0*\0id\";i:1;s:16:\"\0*\0Contabilidade\";O:61:\"DoctrineORMModule\\Proxy\\__CG__\\Cadastros\\Entity\\Contabilidade\":6:{s:17:\"__isInitialized__\";b:0;s:5:\"\0*\0id\";i:1;s:8:\"\0*\0Plano\";N;s:16:\"\0*\0Nome_Fantasia\";N;s:15:\"\0*\0Razao_Social\";N;s:7:\"\0*\0Cnpj\";N;}s:16:\"\0*\0Nome_Fantasia\";s:7:\"empresa\";s:15:\"\0*\0Razao_Social\";s:4:\"razp\";s:7:\"\0*\0Cnpj\";s:10:\"8978987987\";}s:7:\"\0*\0Role\";N;s:13:\"Contabilidade\";N;}', 'N;', 'post', NULL),
(9, '2019-10-01', 'O:34:\"Cadastros\\Entity\\oauth\\oauth_users\":7:{s:5:\"\0*\0id\";i:10;s:11:\"\0*\0username\";s:6:\"teste1\";s:11:\"\0*\0password\";s:60:\"$2y$10$eZU22NqOedKEkizF5AX6UO5poPaG/rOqUrpP0R7Sh8.DYN45slc62\";s:13:\"\0*\0first_name\";N;s:8:\"\0*\0email\";s:7:\"tets@sa\";s:10:\"\0*\0Empresa\";O:24:\"Cadastros\\Entity\\Empresa\":5:{s:5:\"\0*\0id\";i:1;s:16:\"\0*\0Contabilidade\";O:61:\"DoctrineORMModule\\Proxy\\__CG__\\Cadastros\\Entity\\Contabilidade\":6:{s:17:\"__isInitialized__\";b:0;s:5:\"\0*\0id\";i:1;s:8:\"\0*\0Plano\";N;s:16:\"\0*\0Nome_Fantasia\";N;s:15:\"\0*\0Razao_Social\";N;s:7:\"\0*\0Cnpj\";N;}s:16:\"\0*\0Nome_Fantasia\";s:7:\"empresa\";s:15:\"\0*\0Razao_Social\";s:4:\"razp\";s:7:\"\0*\0Cnpj\";s:10:\"8978987987\";}s:7:\"\0*\0Role\";O:34:\"Cadastros\\Entity\\oauth\\oauth_roles\":2:{s:5:\"\0*\0id\";i:1;s:7:\"\0*\0role\";s:5:\"admin\";}}', 'N;', 'post', NULL),
(10, '2019-10-01', 'O:34:\"Cadastros\\Entity\\oauth\\oauth_users\":7:{s:5:\"\0*\0id\";i:11;s:11:\"\0*\0username\";s:5:\"12333\";s:11:\"\0*\0password\";s:60:\"$2y$10$wGfrlsIdg3mFpntUwLxO8emp0bWU5LkIsRMNJr/9xoYVrD1aqF8by\";s:13:\"\0*\0first_name\";N;s:8:\"\0*\0email\";s:11:\"12321322@22\";s:10:\"\0*\0Empresa\";O:24:\"Cadastros\\Entity\\Empresa\":5:{s:5:\"\0*\0id\";i:1;s:16:\"\0*\0Contabilidade\";O:61:\"DoctrineORMModule\\Proxy\\__CG__\\Cadastros\\Entity\\Contabilidade\":6:{s:17:\"__isInitialized__\";b:0;s:5:\"\0*\0id\";i:1;s:8:\"\0*\0Plano\";N;s:16:\"\0*\0Nome_Fantasia\";N;s:15:\"\0*\0Razao_Social\";N;s:7:\"\0*\0Cnpj\";N;}s:16:\"\0*\0Nome_Fantasia\";s:7:\"empresa\";s:15:\"\0*\0Razao_Social\";s:4:\"razp\";s:7:\"\0*\0Cnpj\";s:10:\"8978987987\";}s:7:\"\0*\0Role\";O:34:\"Cadastros\\Entity\\oauth\\oauth_roles\":2:{s:5:\"\0*\0id\";i:2;s:7:\"\0*\0role\";s:13:\"contabilidade\";}}', 'N;', 'post', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `LucroLiquidoAcumulado`
--

CREATE TABLE `LucroLiquidoAcumulado` (
  `id` int(11) NOT NULL,
  `Empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Total` double NOT NULL,
  `Lucro_Prejuizo_Liquido` double NOT NULL,
  `Receitas_Operacionais` double NOT NULL,
  `Diferenca_Porcentagem` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `LucroLiquidoMensal`
--

CREATE TABLE `LucroLiquidoMensal` (
  `id` int(11) NOT NULL,
  `Empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Mes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Ano` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Lucro_Prejuizo_Liquido` double NOT NULL,
  `Receitas_Operacionais` double NOT NULL,
  `Diferenca_Porcentagem` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE `migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
('20190926183121'),
('20190927191045');

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `access_token` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expires` datetime DEFAULT NULL,
  `scope` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`access_token`, `client_id`, `user_id`, `expires`, `scope`) VALUES
('02944c2bd60de702e9e53d07e9513b79e0de431c', 'testclient', 'user', '2019-09-27 13:26:12', NULL),
('07237a29d7ce4a9a702252e4338f3e0bf7330443', 'testclient', 'user', '2019-09-30 20:52:07', NULL),
('083945ae6f77512440abd4dacfd960d6b1e37d1f', 'testclient', 'user', '2019-10-01 18:50:42', NULL),
('0e9b2100436cf9e0e8e06f6ba2d48d84b0262b40', 'testclient', 'user', '2019-10-01 19:14:50', NULL),
('0ede672e77adb27b906684ee09748ab9c531ccdf', 'testclient', 'user', '2019-09-30 23:31:50', NULL),
('121f03fba4e9259cd019a1e8406f4865b3146c5e', 'testclient', 'user', '2019-10-02 01:58:05', NULL),
('1238ba1050c6b6e3896efc18131f046f465b0f03', 'testclient', 'user', '2019-09-30 16:55:23', NULL),
('1426a79047823e2cb581838cdcefedcd65e8bb86', 'testclient', 'user', '2019-10-02 04:33:38', NULL),
('1458ac0aefdb09d50fcca188acb04911ac8fed36', 'testclient', 'user', '2019-09-30 20:35:36', NULL),
('1989b3d9c3f88525d85b72c9f00e4c443ed1c486', 'testclient', 'user', '2019-09-30 20:37:01', NULL),
('1ea48f173f4bb2b7aa54886fb0d3eab36b5db015', 'testclient', 'user', '2019-09-30 23:39:43', NULL),
('1ff06a14d2bc36f31ade16586d6f5545d4bd51fd', 'testclient', 'user', '2019-09-30 23:32:25', NULL),
('270211f23cd2e245244ac2a7083bc1f7356c87df', 'testclient', 'user', '2019-10-02 02:46:23', NULL),
('276605add0032ed92b44d2f664f27bf3087186da', 'testclient', 'user', '2019-10-01 00:12:51', NULL),
('2b8324d175872b396a6f85e2f76c5b933147f029', 'testclient', 'user', '2019-09-30 20:37:56', NULL),
('2ee68fd5652fda044abe5436439b320e879e0fe5', 'testclient', 'teste', '2019-09-28 01:42:15', NULL),
('31cc87a28c4a7d9deb44a2f8fb47c41ab1c3c605', 'testclient', 'teste', '2019-09-28 02:54:56', NULL),
('35c6602065482dba50af5c7d2a6a120ff11f1076', 'testclient', 'user', '2019-10-01 19:13:16', NULL),
('3b7772a37942c47c97be0f51a5e9741f1657544d', 'testclient', 'teste', '2019-10-02 04:14:42', NULL),
('4316ab0bc6e00f6aa46f7400a3d58916360cb1ec', 'testclient', 'user', '2019-10-02 02:43:02', NULL),
('497b672556fb911447590d775de679fb655fa879', 'testclient', 'user', '2019-10-01 00:07:54', NULL),
('4a0457111d95805909868098fab538298168b52f', 'testclient', 'user', '2019-10-01 03:20:04', NULL),
('57ace40c0cb7eba1854d883cf9c0cffe21c31858', 'testclient', 'teste', '2019-10-01 12:46:56', NULL),
('5832c4551953548053a58f2f2db6a86418fa0938', 'testclient', 'user', '2019-09-30 20:54:16', NULL),
('5a57ccaa1c1f362b1662fac41531dc08d547fecc', 'testclient', 'teste', '2019-09-27 20:03:15', NULL),
('5faaefd9a767ed838a711d4e7d1523d0582a7f17', 'testclient', 'user', '2019-09-30 13:53:10', NULL),
('65cbc47f3fc0c2315426f415d48a6249961a5e48', 'testclient', 'user', '2019-10-01 19:20:16', NULL),
('6b37601ea7f0e598397067ee675d9cdedc377e2e', 'testclient', 'user', '2019-09-27 14:44:14', NULL),
('6c099f3cb255c9108b7d2893f866ad96a337467b', 'testclient', 'user', '2019-09-30 19:26:34', NULL),
('7089c0bfd6a92bbfb88a4d67c514c12fd50aaa25', 'testclient', 'user', '2019-09-27 14:44:21', NULL),
('7217c0c1ef71841410e90e71551d09bbffddc618', 'testclient', 'user', '2019-10-01 18:30:13', NULL),
('774c78db802f89ee71f18577d0f01b1a55aa9c87', 'testclient', 'user', '2019-09-30 19:53:28', NULL),
('7ca0d69a13bb893ce91cc519ca8f3f51f9cd0416', 'testclient', 'user', '2019-09-30 20:33:59', NULL),
('7f62ff383705ba3470ea6aba4b60dd13fe64daf9', 'testclient', 'user ', '2019-10-02 03:10:13', NULL),
('8592f7e04cd7ace6e3f046e91f4a1a20a91b511f', 'testclient', 'user', '2019-09-27 14:51:13', NULL),
('8db77b2f9a175f0424207d9e27f9f3b2f3bb57ea', 'testclient', 'user', '2019-09-30 20:17:16', NULL),
('9097ad1d3a4262848b45e57494ce1305ab0eca46', 'testclient', 'user', '2019-09-30 19:55:01', NULL),
('93196e06b009c76dc9ab57a9ff9fffab84b96510', 'testclient', 'user', '2019-10-01 15:26:57', NULL),
('936e652fa589be10205b7476cfaccf736a5277e9', 'testclient', 'user', '2019-09-30 14:54:36', NULL),
('9403077f23de6d6484be22192d56983d550053ca', 'testclient', 'user', '2019-10-01 00:13:08', NULL),
('94848433860ade116121bd5a4a08aa4f995d322c', 'testclient', 'user', '2019-09-27 14:00:50', NULL),
('95ffd56132a107ebc40a00ac1252bff803010bc2', 'testclient', 'user', '2019-09-30 23:34:50', NULL),
('986c8687d8bcf807523657057515810e484f6780', 'testclient', 'user', '2019-09-30 20:16:33', NULL),
('a82590ed8bef5c3352f241466d426015bd2b5af2', 'testclient', 'user', '2019-10-01 03:34:47', NULL),
('a8423fccf38a972ac9e3145e713e7e9406da732b', 'testclient', 'teste', '2019-10-01 15:26:21', NULL),
('b4f556d54729381954a4fb1736d334b913f9b7e0', 'testclient', 'user', '2019-09-30 20:29:48', NULL),
('b5cf607beddd6384098b985282bf2cdc2b4d73e5', 'testclient', 'user', '2019-10-01 00:03:56', NULL),
('b77cf52ecda1efafaf7fc641eb00e10bd8271ad9', 'testclient', 'user', '2019-09-30 23:32:42', NULL),
('b82a773d65a15eeff4accac9a4e112c10edf39d7', 'testclient', 'user', '2019-09-26 21:18:27', NULL),
('bc2af76b18f2bc61b039b7a83668071db59c6f38', 'testclient', 'user', '2019-09-30 19:53:30', NULL),
('c24af8acf083dcb8af8945015418119b991df25b', 'testclient', 'teste', '2019-10-02 04:35:05', NULL),
('c26b4d86261c98271685b6f2fdf3b9cf1e85051a', 'testclient', 'user', '2019-09-27 19:45:18', NULL),
('c72a4af37bc731942bcb28eb3aa55d7e1e05f5a4', 'testclient', 'user', '2019-09-30 20:40:35', NULL),
('cf3041babbb8cb753a46108237bc2a7ac0cc2419', 'testclient', 'user', '2019-09-28 02:57:37', NULL),
('d08f9002bfc4060ae2e86a34f76af643cecb24cf', 'testclient', 'user', '2019-09-28 02:55:33', NULL),
('d98802d746c2be4dae15a4e2d25f902c2728eb90', 'testclient', 'user', '2019-10-01 18:56:33', NULL),
('da04b6fb3bb5bb12192d65c313bdf678c1ef106b', 'testclient', 'user', '2019-09-27 14:42:18', NULL),
('db8845e7e84bdb4d9c1729713911442fd5f15b2b', 'testclient', 'user', '2019-09-30 20:15:04', NULL),
('dc2efcbb17e98ace42fd50a7f37f164094c8ee7e', 'testclient', 'user', '2019-09-30 18:14:02', NULL),
('e3e4d3516d42923bee482977f4b8958dcf5f677c', 'testclient', 'user', '2019-09-30 19:18:07', NULL),
('e545209d23e32996cb29c4d321f1624301f48883', 'testclient', 'user', '2019-09-30 23:35:19', NULL),
('e6f7dd2cdb7f33bd838034bede7014876116159d', 'testclient', 'user', '2019-09-27 12:24:19', NULL),
('ea831429c6835c41196e2e041163c6ab9ae702d4', 'testclient', 'user', '2019-09-30 20:10:19', NULL),
('eba0b35d942355dff2b386a1138b82ef097c5ef2', 'testclient', 'user', '2019-10-01 19:18:49', NULL),
('ec566cadabe2ce3d3e766d0aff6d937e9dda68c3', 'testclient', 'user', '2019-09-30 18:15:26', NULL),
('ef871deeeae95f570ecc8cca4d111c6245cc4e42', 'testclient', 'user', '2019-09-30 20:13:59', NULL),
('f10baddffa896c7466a887a4b842a42976312443', 'testclient', 'teste', '2019-10-01 04:36:28', NULL),
('f56d5a76578328c9c6a6188810e7e11498246a23', 'testclient', 'user', '2019-09-30 20:16:55', NULL),
('f5cb454c6d81fe3732aee3a61cfde2d59a490ed5', 'testclient', 'user', '2019-09-30 18:15:50', NULL),
('f8b346d5d8dced5ef8bfeb2c371eccc9df6e3c20', 'testclient', 'user', '2019-09-30 16:27:01', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_authorization_codes`
--

CREATE TABLE `oauth_authorization_codes` (
  `authorization_code` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `redirect_uri` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expires` datetime NOT NULL,
  `scope` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_token` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `client_id` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `client_secret` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `redirect_uri` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `grant_types` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scope` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `oauth_clients`
--

INSERT INTO `oauth_clients` (`client_id`, `client_secret`, `redirect_uri`, `grant_types`, `scope`, `user_id`) VALUES
('testclient', '$2y$10$5ICo6mbnWLsptjCZVfMu1e7p04FYpgiZydEG1KD4MI8Q2fcwuCu8e', '/oauth/receivecode', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_jwt`
--

CREATE TABLE `oauth_jwt` (
  `client_id` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_key` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_programs`
--

CREATE TABLE `oauth_programs` (
  `id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `oauth_programs`
--

INSERT INTO `oauth_programs` (`id`, `path`) VALUES
(1, 'CadastrosAPI\\V1\\Rest\\Contabilidade\\Controller::collection'),
(2, 'CadastrosAPI\\V1\\Rest\\Plano\\Controller::collection'),
(3, 'CadastrosAPI\\V1\\Rest\\CheckNameUser\\Controller::collection');

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `refresh_token` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expires` datetime NOT NULL,
  `scope` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`refresh_token`, `client_id`, `user_id`, `expires`, `scope`) VALUES
('0129da68fb4993559308e14fc59e1a644c569379', 'testclient', 'user', '2019-10-11 13:51:13', NULL),
('0c228c6fbaee710093b69e423a764852349f20f3', 'testclient', 'user', '2019-10-11 18:45:18', NULL),
('0ef4f5b75c3a6b5872c0d2f925035dbdd7b60c3f', 'testclient', 'teste', '2019-10-15 14:26:21', NULL),
('12eef0c24f5c53c87fb3af509be134537a542864', 'testclient', 'user', '2019-10-11 13:44:14', NULL),
('14e9ce0cf00e19288002f598eff13d8ad3b717f1', 'testclient', 'user', '2019-10-14 18:55:01', NULL),
('15847c8f4ab62c2cef96a4057c75059f73d480a3', 'testclient', 'user', '2019-10-15 18:18:49', NULL),
('163af7705f222ef742a55b428a4a8371dafd29fe', 'testclient', 'user', '2019-10-14 23:07:54', NULL),
('186fbce839d888670d68f9b27bdfa44955b0862e', 'testclient', 'user', '2019-10-14 17:14:02', NULL),
('1b1dcc81cef76824b2ddc0017358764fb1fe2379', 'testclient', 'user', '2019-10-15 18:13:16', NULL),
('1cfbfd208ae7927bbdfecfc732b580046eb354ac', 'testclient', 'user', '2019-10-15 17:30:13', NULL),
('240e9b01bb7aaa09716a3fe7460c7e42f164f244', 'testclient', 'user', '2019-10-14 13:54:36', NULL),
('27facf32190a5c4164e8823562a1a9c64cfa9a46', 'testclient', 'user', '2019-10-14 19:16:33', NULL),
('2c9e15c333e26a12eddfe3efe381931040e3a32c', 'testclient', 'user', '2019-10-14 22:31:50', NULL),
('33953eae6fbc846c094aa7f4665fa95fc1edc41c', 'testclient', 'user', '2019-10-14 19:37:56', NULL),
('343873c1736e50c0b07b6a7ca6f0fbd536baf335', 'testclient', 'user', '2019-10-14 18:53:28', NULL),
('347e422eb4f561a065a18e9fe4753196caa5fc60', 'testclient', 'user', '2019-10-11 12:26:12', NULL),
('379dbd7c8705bd73f77bf2178528c745032a0580', 'testclient', 'teste', '2019-10-16 03:14:42', NULL),
('3ba10ab8343eeb85dd28795e147210747a1ec648', 'testclient', 'user', '2019-10-14 19:15:04', NULL),
('3c589d3b1ad1599e44b93953d42b1db14e5bdb31', 'testclient', 'user', '2019-10-15 17:50:43', NULL),
('400d88e52e90f2893b1e5ecd64c3da29f134b26e', 'testclient', 'teste', '2019-10-16 03:35:05', NULL),
('4340f941be05555c40e93671f889644b38480059', 'testclient', 'teste', '2019-10-15 03:36:28', NULL),
('438d9b5d8ad925e22462835c9331c3602e003684', 'testclient', 'user', '2019-10-15 02:34:47', NULL),
('49c3881a961d223c984794d46c64f0cb65b9d082', 'testclient', 'user', '2019-10-10 20:18:27', NULL),
('4a0c2f083989bb15f4a41178ed262b420ad0fb55', 'testclient', 'user', '2019-10-11 11:24:19', NULL),
('51d4547d30b242043ccb5af3a5881dfba29724cd', 'testclient', 'user', '2019-10-14 19:29:48', NULL),
('5301bda7088cfdd3c6d5de00adfd015c3d566c65', 'testclient', 'user', '2019-10-15 18:20:16', NULL),
('55398b2f7f568274b2cd9debe919906f8e648e6e', 'testclient', 'user', '2019-10-16 01:43:02', NULL),
('56f11e46f994629e690ac95c18a97a186dfc54a5', 'testclient', 'user', '2019-10-14 22:32:25', NULL),
('5bec7c991d6df86dc503998a0e216eaab6be2c23', 'testclient', 'user', '2019-10-14 12:53:10', NULL),
('5c1d4cde7d9a5c3be4c57fef8bfede946657853c', 'testclient', 'user', '2019-10-16 01:46:23', NULL),
('630d5c66aa9ad4b35f555d4e9e8013e70e2e05cc', 'testclient', 'user', '2019-10-14 17:15:26', NULL),
('63df869e50ed151fb7b12423d9c71f2e63a20934', 'testclient', 'user', '2019-10-16 03:33:38', NULL),
('64216777ceb74d27eff7ffedd768023547b03d50', 'testclient', 'user', '2019-10-15 02:20:04', NULL),
('68b3aaabb174a7bb932979e22d8b895d20ae53ab', 'testclient', 'user', '2019-10-12 01:57:37', NULL),
('710c5d4a7aea5cfacbc4855a8fee2b66fdd7783f', 'testclient', 'user', '2019-10-14 18:26:34', NULL),
('71fa0118347f1daaf59ac4d3e9327d78cef754ec', 'testclient', 'user', '2019-10-14 23:12:51', NULL),
('72eb2f39e2899155fe4523475148f0793f879ae2', 'testclient', 'user', '2019-10-15 18:14:50', NULL),
('739728f6c7ddcd4e4fb2b3a2467793a301a5c891', 'testclient', 'user', '2019-10-15 17:56:33', NULL),
('74e367bed8a034d3fc2d63d31ec5e94a6ed4ebd1', 'testclient', 'user', '2019-10-14 22:35:19', NULL),
('753186677519bb0256943e44dc952f1705251404', 'testclient', 'user', '2019-10-14 19:10:19', NULL),
('775f8b54bf0120760f9941653cc79c55816045d2', 'testclient', 'user', '2019-10-14 19:33:59', NULL),
('847417d445ad979f13b076a96ca9deb00a8926bc', 'testclient', 'user', '2019-10-12 01:55:33', NULL),
('854fa9e025c13b478afefe2df099922f6af4e2f0', 'testclient', 'user', '2019-10-11 13:00:50', NULL),
('85d3fe0b39f0eca86809dde78df4f968016013cc', 'testclient', 'user', '2019-10-14 18:18:07', NULL),
('85e33d770b5fa117c104b7665bbb1b3dbc45c9c8', 'testclient', 'user', '2019-10-14 19:35:36', NULL),
('90490d34e1541bb8bebecbc7610eedd0d2dfc22e', 'testclient', 'user', '2019-10-11 13:44:21', NULL),
('9c23246edbf647c580d138bb7aecf3518ec34cd4', 'testclient', 'user', '2019-10-14 23:13:08', NULL),
('a722e495374dedaa51c14f79837afd07cec4b78b', 'testclient', 'user', '2019-10-14 22:34:50', NULL),
('aa5fafddedb967107d9e2de9e0f709dd65c8e0b7', 'testclient', 'user', '2019-10-14 18:53:30', NULL),
('aba7589987c47a9252fa42ea7001c9822be923d0', 'testclient', 'user', '2019-10-11 13:42:18', NULL),
('b01d4e663ab6fa5e618ac6da003d61756a4a57b6', 'testclient', 'user', '2019-10-14 19:40:35', NULL),
('b6c88176c174ffd51d0507d5e4b8301ec54d22fd', 'testclient', 'user', '2019-10-14 22:32:42', NULL),
('b6d87915964e162e8956433f40f3fed5777e1e84', 'testclient', 'user', '2019-10-14 19:52:07', NULL),
('ba0a307f435641212760937c064788a9d9ff69cc', 'testclient', 'user', '2019-10-14 15:55:23', NULL),
('baf0bfa01bd3fed54529c490ebcfc3fe4b521434', 'testclient', 'user', '2019-10-14 19:16:55', NULL),
('bc2375a24a417fdc3b13dc3c10bd56026aa8ddee', 'testclient', 'user', '2019-10-14 23:03:56', NULL),
('bf2c12889765556b62a8d73e1eee03492319a0d9', 'testclient', 'user', '2019-10-14 19:54:16', NULL),
('c4ba2fbd191c8dea29eff86c5ddd531ff00b429a', 'testclient', 'user', '2019-10-14 15:27:01', NULL),
('c55a2d75b72b0997fc82db6a4290e4fe90ce66d5', 'testclient', 'user', '2019-10-14 19:13:59', NULL),
('c5f0726d0d40e1ec7e3592e68d7abf504dd1d5c6', 'testclient', 'user', '2019-10-15 14:26:57', NULL),
('cb34b311db1d2be5f4572f590255dace6601aa92', 'testclient', 'teste', '2019-10-12 00:42:15', NULL),
('d367e5218dc9255933c6c3fb9241b01b19205c8d', 'testclient', 'user', '2019-10-14 22:39:43', NULL),
('d4140f3841e231b7ad6f5980b7ec774d7a083458', 'testclient', 'user', '2019-10-14 17:15:50', NULL),
('d4e201d955669c76cc32d167b488a850df522cab', 'testclient', 'teste', '2019-10-15 11:46:56', NULL),
('d657a2b3282c7727890cd93d1bdfb26ff5797fd3', 'testclient', 'user', '2019-10-14 19:37:01', NULL),
('dc74e41b0c16b5a6e8c6f66e6a171088503104d8', 'testclient', 'user', '2019-10-16 00:58:05', NULL),
('e4e1bf29977b724d815494ac38c151e4ebaa482f', 'testclient', 'teste', '2019-10-11 19:03:15', NULL),
('ecb12f3ea6c74cd1e3a75c7578143dc344eb3252', 'testclient', 'user', '2019-10-14 19:17:16', NULL),
('f21bb4702ae7954862705becaba122ce67dd8774', 'testclient', 'teste', '2019-10-12 01:54:56', NULL),
('fe5c91685b1a13f2dab05f21b382e21919103b9e', 'testclient', 'user ', '2019-10-16 02:10:13', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_roles`
--

CREATE TABLE `oauth_roles` (
  `id` int(11) NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `oauth_roles`
--

INSERT INTO `oauth_roles` (`id`, `role`) VALUES
(1, 'admin'),
(2, 'contabilidade'),
(3, 'empresa');

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_scopes`
--

CREATE TABLE `oauth_scopes` (
  `scope` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_id` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_default` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `oauth_users`
--

CREATE TABLE `oauth_users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Empresa_id` int(11) DEFAULT NULL,
  `Role_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `oauth_users`
--

INSERT INTO `oauth_users` (`id`, `username`, `password`, `first_name`, `email`, `Empresa_id`, `Role_id`) VALUES
(1, 'user', '$2y$10$G4D/ycncYKQQ3hO2/.GFZeHDIVh3TribcKiCtBznuROeN76ywxVue', 'xxx', 'aaa@aaa.com.br', 1, 1),
(2, 'teste', '$2y$10$G4D/ycncYKQQ3hO2/.GFZeHDIVh3TribcKiCtBznuROeN76ywxVue', NULL, 'a', NULL, 2),
(3, '123', '$2y$10$Dh0MATuPoQBI3tzNru5sX.xGDzx1kpmzqiaX6sD4joUGCfB1Y49W.', NULL, '123@2', 1, NULL),
(4, '123', '$2y$10$z5IdLyKL6jFKXQGB/LAmLeRNUmvwjlSLWdFVYX.KoEYstuWoNQCpu', NULL, 'sdfs@d', 1, NULL),
(5, '12', '$2y$10$hvXR6TdliOhgXYo47KEwDuKM.52RuzWc3/wKBcAnKrSOyFlmzrvRu', NULL, '', 1, NULL),
(6, '22', '$2y$10$nImtimjTsDZaOMLRfEKOj.Ytw2YALc48xoy5C.aKK/2lqQR5vqzlu', NULL, '', 1, NULL),
(7, '22', '$2y$10$KAzlcDQQWXD9uq.OdsDwPeYTZKHL1gVcuUcemo.K2yOUHHX8IBD.G', NULL, '', 1, NULL),
(8, '', '$2y$10$5RPpq3fUzf6kA069h4JgDexrwJAWyjOOGVSk/.yPHQzYFn/NVRerG', NULL, '', 1, NULL),
(9, '1233', '$2y$10$JubEDJ2T2zJk2SDAvqzdHeEaEK0ZDGssSB6oUb4hqubjieyeiMqRK', NULL, '123@2', 1, NULL),
(10, 'teste1', '$2y$10$eZU22NqOedKEkizF5AX6UO5poPaG/rOqUrpP0R7Sh8.DYN45slc62', NULL, 'tets@sa', 1, 1),
(11, '12333', '$2y$10$wGfrlsIdg3mFpntUwLxO8emp0bWU5LkIsRMNJr/9xoYVrD1aqF8by', NULL, '12321322@22', 1, 2);

-- --------------------------------------------------------

--
-- Estrutura para tabela `Plano`
--

CREATE TABLE `Plano` (
  `id` int(11) NOT NULL,
  `Nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Descricao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Limite_De_Empresas` int(11) NOT NULL,
  `Preco` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `Plano`
--

INSERT INTO `Plano` (`id`, `Nome`, `Descricao`, `Limite_De_Empresas`, `Preco`) VALUES
(1, 'plus', 'descricao', 1, 998);

-- --------------------------------------------------------

--
-- Estrutura para tabela `Treinamento`
--

CREATE TABLE `Treinamento` (
  `id` int(11) NOT NULL,
  `Titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Descricao` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Uploads_Mensais`
--

CREATE TABLE `Uploads_Mensais` (
  `id` int(11) NOT NULL,
  `Ano` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Mes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Data_Upload` date NOT NULL,
  `Receitas_Operacionais` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Receita_De_Vendas` double NOT NULL,
  `Receita_De_Fretes_E_Entregas` double NOT NULL,
  `Outras_Receitas_1` double NOT NULL,
  `Outras_Receitas_2` double NOT NULL,
  `Deducoes_Da_Receita_Bruta` double NOT NULL,
  `Impostos_Sobre_Vendas` double NOT NULL,
  `Comissoes_Sobre_Vendas` double NOT NULL,
  `Descontos_Incondicionais` double NOT NULL,
  `Devolucoes_De_Vendas` double NOT NULL,
  `Outras_Deducoes_1` double NOT NULL,
  `Outras_Deducoes_2` double NOT NULL,
  `Receita_Liquida_De_Vendas` double NOT NULL,
  `Custos_Operacionais` double NOT NULL,
  `Custo_Das_Mercadorias_Vendidas` double NOT NULL,
  `Custo_Dos_Produtos_Vendidos` double NOT NULL,
  `Custo_Dos_Servicos_Prestados` double NOT NULL,
  `Outros_Custos` double NOT NULL,
  `Lucro_Bruto` double NOT NULL,
  `Despesas_Operacionais` double NOT NULL,
  `Despesas_Com_Vendas` double NOT NULL,
  `Despesas_Administrativas` double NOT NULL,
  `Despesas_Tributarias_Gerais` double NOT NULL,
  `Outras_Despesas_1` double NOT NULL,
  `Outras_Despesas_2` double NOT NULL,
  `Outras_Despesas_3` double NOT NULL,
  `Outras_Despesas_4` double NOT NULL,
  `Lucro_Prejuizo_Operacional` double NOT NULL,
  `Receitas_E_Despesas_Financeiras` double NOT NULL,
  `Receitas_E_Rendimentos_Financeiros` double NOT NULL,
  `Despesas_Financeiras` double NOT NULL,
  `Outras_Receitas_Financeiras` double NOT NULL,
  `Outras_Despesas_Financeiras` double NOT NULL,
  `Outras_Receitas_E_Despesas_Nao_Operacionais` double NOT NULL,
  `Outras_Receitas_Nao_Operacionais` double NOT NULL,
  `Outras_Despesas_Nao_Operacionais` double NOT NULL,
  `Lucro_Prejuizo_Liquido` double NOT NULL,
  `Desembolso_Com_Investimentos_E_Emprestimos` double NOT NULL,
  `Investimentos_Em_Imobilizado` double NOT NULL,
  `Emprestimos_E_Dividas` double NOT NULL,
  `Outros_Investimentos_E_Emprestimos` double NOT NULL,
  `Lucro_Prejuizo_Final` double NOT NULL,
  `Empresa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `Contabilidade`
--
ALTER TABLE `Contabilidade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_4501CB7F63FDE49A` (`Plano_id`);

--
-- Índices de tabela `Empresa`
--
ALTER TABLE `Empresa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_776A63CC66E5D40B` (`Contabilidade_id`);

--
-- Índices de tabela `Logs`
--
ALTER TABLE `Logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_50BD69629465404E` (`Usuario_id`);

--
-- Índices de tabela `LucroLiquidoAcumulado`
--
ALTER TABLE `LucroLiquidoAcumulado`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `LucroLiquidoMensal`
--
ALTER TABLE `LucroLiquidoMensal`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`version`);

--
-- Índices de tabela `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`access_token`);

--
-- Índices de tabela `oauth_authorization_codes`
--
ALTER TABLE `oauth_authorization_codes`
  ADD PRIMARY KEY (`authorization_code`);

--
-- Índices de tabela `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Índices de tabela `oauth_jwt`
--
ALTER TABLE `oauth_jwt`
  ADD PRIMARY KEY (`client_id`);

--
-- Índices de tabela `oauth_programs`
--
ALTER TABLE `oauth_programs`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`refresh_token`);

--
-- Índices de tabela `oauth_roles`
--
ALTER TABLE `oauth_roles`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `oauth_scopes`
--
ALTER TABLE `oauth_scopes`
  ADD PRIMARY KEY (`scope`);

--
-- Índices de tabela `oauth_users`
--
ALTER TABLE `oauth_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_93804FF81D431A41` (`Empresa_id`),
  ADD KEY `IDX_93804FF819BE1B30` (`Role_id`);

--
-- Índices de tabela `Plano`
--
ALTER TABLE `Plano`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `Treinamento`
--
ALTER TABLE `Treinamento`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `Uploads_Mensais`
--
ALTER TABLE `Uploads_Mensais`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5418943E1D431A41` (`Empresa_id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `Contabilidade`
--
ALTER TABLE `Contabilidade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `Empresa`
--
ALTER TABLE `Empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `Logs`
--
ALTER TABLE `Logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `LucroLiquidoAcumulado`
--
ALTER TABLE `LucroLiquidoAcumulado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `LucroLiquidoMensal`
--
ALTER TABLE `LucroLiquidoMensal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `oauth_programs`
--
ALTER TABLE `oauth_programs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `oauth_roles`
--
ALTER TABLE `oauth_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `oauth_users`
--
ALTER TABLE `oauth_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de tabela `Plano`
--
ALTER TABLE `Plano`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `Treinamento`
--
ALTER TABLE `Treinamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `Uploads_Mensais`
--
ALTER TABLE `Uploads_Mensais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `Contabilidade`
--
ALTER TABLE `Contabilidade`
  ADD CONSTRAINT `FK_4501CB7F63FDE49A` FOREIGN KEY (`Plano_id`) REFERENCES `Plano` (`id`);

--
-- Restrições para tabelas `Empresa`
--
ALTER TABLE `Empresa`
  ADD CONSTRAINT `FK_776A63CC66E5D40B` FOREIGN KEY (`Contabilidade_id`) REFERENCES `Contabilidade` (`id`);

--
-- Restrições para tabelas `Logs`
--
ALTER TABLE `Logs`
  ADD CONSTRAINT `FK_50BD69629465404E` FOREIGN KEY (`Usuario_id`) REFERENCES `oauth_users` (`id`);

--
-- Restrições para tabelas `oauth_users`
--
ALTER TABLE `oauth_users`
  ADD CONSTRAINT `FK_93804FF819BE1B30` FOREIGN KEY (`Role_id`) REFERENCES `oauth_roles` (`id`),
  ADD CONSTRAINT `FK_93804FF81D431A41` FOREIGN KEY (`Empresa_id`) REFERENCES `Empresa` (`id`);

--
-- Restrições para tabelas `Uploads_Mensais`
--
ALTER TABLE `Uploads_Mensais`
  ADD CONSTRAINT `FK_5418943E1D431A41` FOREIGN KEY (`Empresa_id`) REFERENCES `Empresa` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

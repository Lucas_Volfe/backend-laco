--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

-- Started on 2019-09-10 13:59:56 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 214 (class 1259 OID 24715)
-- Name: uploads_mensais; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.uploads_mensais (
    id integer NOT NULL,
    ano character varying(255) NOT NULL,
    mes character varying(255) NOT NULL,
    data_upload date NOT NULL,
    empresa integer NOT NULL,
    receitas_operacionais character varying(255) NOT NULL,
    receita_de_vendas double precision NOT NULL,
    receita_de_fretes_e_entregas double precision NOT NULL,
    outras_receitas_1 double precision NOT NULL,
    outras_receitas_2 double precision NOT NULL,
    deducoes_da_receita_bruta double precision NOT NULL,
    impostos_sobre_vendas double precision NOT NULL,
    comissoes_sobre_vendas double precision NOT NULL,
    descontos_incondicionais double precision NOT NULL,
    devolucoes_de_vendas double precision NOT NULL,
    outras_deducoes_1 double precision NOT NULL,
    outras_deducoes_2 double precision NOT NULL,
    receita_liquida_de_vendas double precision NOT NULL,
    custos_operacionais double precision NOT NULL,
    custo_das_mercadorias_vendidas double precision NOT NULL,
    custo_dos_produtos_vendidos double precision NOT NULL,
    custo_dos_servicos_prestados double precision NOT NULL,
    outros_custos double precision NOT NULL,
    lucro_bruto double precision NOT NULL,
    despesas_operacionais double precision NOT NULL,
    despesas_com_vendas double precision NOT NULL,
    despesas_administrativas double precision NOT NULL,
    despesas_tributarias_gerais double precision NOT NULL,
    outras_despesas_1 double precision NOT NULL,
    outras_despesas_2 double precision NOT NULL,
    outras_despesas_3 double precision NOT NULL,
    outras_despesas_4 double precision NOT NULL,
    lucro_prejuizo_operacional double precision NOT NULL,
    receitas_e_despesas_financeiras double precision NOT NULL,
    receitas_e_rendimentos_financeiros double precision NOT NULL,
    despesas_financeiras double precision NOT NULL,
    outras_receitas_financeiras double precision NOT NULL,
    outras_despesas_financeiras double precision NOT NULL,
    outras_receitas_e_despesas_nao_operacionais double precision NOT NULL,
    outras_receitas_nao_operacionais double precision NOT NULL,
    outras_despesas_nao_operacionais double precision NOT NULL,
    lucro_prejuizo_liquido double precision NOT NULL,
    desembolso_com_investimentos_e_emprestimos double precision NOT NULL,
    investimentos_em_imobilizado double precision NOT NULL,
    emprestimos_e_dividas double precision NOT NULL,
    outros_investimentos_e_emprestimos double precision NOT NULL,
    lucro_prejuizo_final double precision NOT NULL
);


ALTER TABLE public.uploads_mensais OWNER TO postgres;

--
-- TOC entry 3950 (class 0 OID 24715)
-- Dependencies: 214
-- Data for Name: uploads_mensais; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.uploads_mensais (id, ano, mes, data_upload, empresa, receitas_operacionais, receita_de_vendas, receita_de_fretes_e_entregas, outras_receitas_1, outras_receitas_2, deducoes_da_receita_bruta, impostos_sobre_vendas, comissoes_sobre_vendas, descontos_incondicionais, devolucoes_de_vendas, outras_deducoes_1, outras_deducoes_2, receita_liquida_de_vendas, custos_operacionais, custo_das_mercadorias_vendidas, custo_dos_produtos_vendidos, custo_dos_servicos_prestados, outros_custos, lucro_bruto, despesas_operacionais, despesas_com_vendas, despesas_administrativas, despesas_tributarias_gerais, outras_despesas_1, outras_despesas_2, outras_despesas_3, outras_despesas_4, lucro_prejuizo_operacional, receitas_e_despesas_financeiras, receitas_e_rendimentos_financeiros, despesas_financeiras, outras_receitas_financeiras, outras_despesas_financeiras, outras_receitas_e_despesas_nao_operacionais, outras_receitas_nao_operacionais, outras_despesas_nao_operacionais, lucro_prejuizo_liquido, desembolso_com_investimentos_e_emprestimos, investimentos_em_imobilizado, emprestimos_e_dividas, outros_investimentos_e_emprestimos, lucro_prejuizo_final) FROM stdin;
155	18	jul	2019-09-10	99999	398608.59	398608.590000000026	0	0	0	49094.5299999999988	25353.1399999999994	2723.25	0	16538.3400000000001	4479.80000000000018	0	349514.059999999998	219135.549999999988	145391.779999999999	0	73743.7700000000041	0	130378.509999999995	92898.5099999999948	0	0	0	92898.5099999999948	0	0	0	37480	-5643.15999999999985	7048.77000000000044	12691.9300000000003	0	0	-19.8500000000000014	924.730000000000018	944.580000000000041	31816.9900000000016	13362.2999999999993	491.079999999999984	12871.2199999999993	0	18454.6899999999987
156	18	ago	2019-09-10	99999	340462.75	340462.75	0	0	0	52171.0500000000029	33205.5500000000029	3774.59999999999991	0	13946.5599999999995	1244.33999999999992	0	288291.700000000012	179814.220000000001	146346.660000000003	0	33467.5599999999977	0	108477.479999999996	93893.4900000000052	0	0	0	93893.4900000000052	0	0	0	14583.9899999999998	-11273.9500000000007	2823.01000000000022	14096.9599999999991	0	0	338.149999999999977	592.669999999999959	254.52000000000001	3648.19000000000005	43449.3899999999994	491.079999999999984	42958.3099999999977	0	-39801.1999999999971
157	18	set	2019-09-10	99999	413880.67	413880.669999999984	0	0	0	62294.8000000000029	31012.4799999999996	4108.88000000000011	0	26131.7400000000016	1041.70000000000005	0	351585.869999999995	219983.089999999997	145073.670000000013	0	74909.4199999999983	0	131602.779999999999	142394.290000000008	0	0	0	142394.290000000008	0	0	0	-10791.5100000000002	-15158.0100000000002	1554.03999999999996	16712.0499999999993	0	0	-303.490000000000009	50	353.490000000000009	-26253.0099999999984	13532.0100000000002	491.079999999999984	13040.9300000000003	0	-39785.0199999999968
158	18\r	out	2019-09-10	99999	501951.13	501951.130000000005	0	0	0	92082.4799999999959	37243.3799999999974	2415.11999999999989	0	49043.760000000002	3380.2199999999998	0	409868.650000000023	224778.350000000006	201302.429999999993	0	23475.9199999999983	0	185090.299999999988	108724.860000000001	0	0	0	108724.860000000001	0	0	0	76365.4400000000023	-9927.77000000000044	4156.15999999999985	14083.9300000000003	0	0	-550.389999999999986	65.5100000000000051	615.899999999999977	65887.2799999999988	38610.3499999999985	491.079999999999984	38119.2699999999968	0	27276.9300000000003
\.


--
-- TOC entry 3828 (class 2606 OID 24722)
-- Name: uploads_mensais uploads_mensais_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.uploads_mensais
    ADD CONSTRAINT uploads_mensais_pkey PRIMARY KEY (id);


-- Completed on 2019-09-10 13:59:56 -03

--
-- PostgreSQL database dump complete
--


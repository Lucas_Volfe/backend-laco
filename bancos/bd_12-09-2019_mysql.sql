-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: wwlaco_site
-- ------------------------------------------------------
-- Server version	5.7.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Contabilidade`
--

DROP TABLE IF EXISTS `Contabilidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Contabilidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `NomeFantasia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `RazaoSocial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Cnpj` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Contabilidade`
--

LOCK TABLES `Contabilidade` WRITE;
/*!40000 ALTER TABLE `Contabilidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `Contabilidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Empresa`
--

DROP TABLE IF EXISTS `Empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `NomeFantasia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `RazaoSocial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Cnpj` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Empresa`
--

LOCK TABLES `Empresa` WRITE;
/*!40000 ALTER TABLE `Empresa` DISABLE KEYS */;
/*!40000 ALTER TABLE `Empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Uploads_Mensais`
--

DROP TABLE IF EXISTS `Uploads_Mensais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Uploads_Mensais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Ano` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Mes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Data_Upload` date NOT NULL,
  `Empresa` int(11) NOT NULL,
  `Receitas_Operacionais` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Receita_De_Vendas` double NOT NULL,
  `Receita_De_Fretes_E_Entregas` double NOT NULL,
  `Outras_Receitas_1` double NOT NULL,
  `Outras_Receitas_2` double NOT NULL,
  `Deducoes_Da_Receita_Bruta` double NOT NULL,
  `Impostos_Sobre_Vendas` double NOT NULL,
  `Comissoes_Sobre_Vendas` double NOT NULL,
  `Descontos_Incondicionais` double NOT NULL,
  `Devolucoes_De_Vendas` double NOT NULL,
  `Outras_Deducoes_1` double NOT NULL,
  `Outras_Deducoes_2` double NOT NULL,
  `Receita_Liquida_De_Vendas` double NOT NULL,
  `Custos_Operacionais` double NOT NULL,
  `Custo_Das_Mercadorias_Vendidas` double NOT NULL,
  `Custo_Dos_Produtos_Vendidos` double NOT NULL,
  `Custo_Dos_Servicos_Prestados` double NOT NULL,
  `Outros_Custos` double NOT NULL,
  `Lucro_Bruto` double NOT NULL,
  `Despesas_Operacionais` double NOT NULL,
  `Despesas_Com_Vendas` double NOT NULL,
  `Despesas_Administrativas` double NOT NULL,
  `Despesas_Tributarias_Gerais` double NOT NULL,
  `Outras_Despesas_1` double NOT NULL,
  `Outras_Despesas_2` double NOT NULL,
  `Outras_Despesas_3` double NOT NULL,
  `Outras_Despesas_4` double NOT NULL,
  `Lucro_Prejuizo_Operacional` double NOT NULL,
  `Receitas_E_Despesas_Financeiras` double NOT NULL,
  `Receitas_E_Rendimentos_Financeiros` double NOT NULL,
  `Despesas_Financeiras` double NOT NULL,
  `Outras_Receitas_Financeiras` double NOT NULL,
  `Outras_Despesas_Financeiras` double NOT NULL,
  `Outras_Receitas_E_Despesas_Nao_Operacionais` double NOT NULL,
  `Outras_Receitas_Nao_Operacionais` double NOT NULL,
  `Outras_Despesas_Nao_Operacionais` double NOT NULL,
  `Lucro_Prejuizo_Liquido` double NOT NULL,
  `Desembolso_Com_Investimentos_E_Emprestimos` double NOT NULL,
  `Investimentos_Em_Imobilizado` double NOT NULL,
  `Emprestimos_E_Dividas` double NOT NULL,
  `Outros_Investimentos_E_Emprestimos` double NOT NULL,
  `Lucro_Prejuizo_Final` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Uploads_Mensais`
--

LOCK TABLES `Uploads_Mensais` WRITE;
/*!40000 ALTER TABLE `Uploads_Mensais` DISABLE KEYS */;
INSERT INTO `Uploads_Mensais` VALUES (1,'18','jul','2019-09-12',99999,'398608.59',398608.59,0,0,0,49094.53,25353.14,2723.25,0,16538.34,4479.8,0,349514.06,219135.55,145391.78,0,73743.77,0,130378.51,92898.51,0,0,0,92898.51,0,0,0,37480,-5643.16,7048.77,12691.93,0,0,-19.85,924.73,944.58,31816.99,13362.3,491.08,12871.22,0,18454.69),(2,'18','ago','2019-09-12',99999,'340462.75',340462.75,0,0,0,52171.05,33205.55,3774.6,0,13946.56,1244.34,0,288291.7,179814.22,146346.66,0,33467.56,0,108477.48,93893.49,0,0,0,93893.49,0,0,0,14583.99,-11273.95,2823.01,14096.96,0,0,338.15,592.67,254.52,3648.19,43449.39,491.08,42958.31,0,-39801.2),(3,'18','set','2019-09-12',99999,'413880.67',413880.67,0,0,0,62294.8,31012.48,4108.88,0,26131.74,1041.7,0,351585.87,219983.09,145073.67,0,74909.42,0,131602.78,142394.29,0,0,0,142394.29,0,0,0,-10791.51,-15158.01,1554.04,16712.05,0,0,-303.49,50,353.49,-26253.01,13532.01,491.08,13040.93,0,-39785.02),(4,'18\r','out','2019-09-12',99999,'501951.13',501951.13,0,0,0,92082.48,37243.38,2415.12,0,49043.76,3380.22,0,409868.65,224778.35,201302.43,0,23475.92,0,185090.3,108724.86,0,0,0,108724.86,0,0,0,76365.44,-9927.77,4156.16,14083.93,0,0,-550.39,65.51,615.9,65887.28,38610.35,491.08,38119.27,0,27276.93);
/*!40000 ALTER TABLE `Uploads_Mensais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acumulado`
--

DROP TABLE IF EXISTS `acumulado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acumulado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Total` double NOT NULL,
  `Lucro_Prejuizo_Liquido` double NOT NULL,
  `Receitas_Operacionais` double NOT NULL,
  `Diferenca_Porcentagem` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acumulado`
--

LOCK TABLES `acumulado` WRITE;
/*!40000 ALTER TABLE `acumulado` DISABLE KEYS */;
/*!40000 ALTER TABLE `acumulado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lucros_Mensais`
--

DROP TABLE IF EXISTS `lucros_Mensais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lucros_Mensais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Mes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Ano` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Lucro_Prejuizo_Liquido` double NOT NULL,
  `Receitas_Operacionais` double NOT NULL,
  `Diferenca_Porcentagem` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lucros_Mensais`
--

LOCK TABLES `lucros_Mensais` WRITE;
/*!40000 ALTER TABLE `lucros_Mensais` DISABLE KEYS */;
/*!40000 ALTER TABLE `lucros_Mensais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('20190912130546');
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-12 10:27:50

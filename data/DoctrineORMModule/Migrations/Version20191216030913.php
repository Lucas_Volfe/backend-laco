<?php declare(strict_types=1);

namespace DoctrineORMModule\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191216030913 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE Balanco_Patrimonial (id INT AUTO_INCREMENT NOT NULL, Ano VARCHAR(255) NOT NULL, Mes VARCHAR(255) NOT NULL, Data_Upload DATE NOT NULL, bens_Numerarios DOUBLE PRECISION NOT NULL, depositos_Bancarios_A_Vista DOUBLE PRECISION NOT NULL, aplicacoes_De_Liquidez_Imediata DOUBLE PRECISION NOT NULL, duplicatas_A_Receber DOUBLE PRECISION NOT NULL, duplicatas_Descontadas DOUBLE PRECISION NOT NULL, titulos_A_Receber DOUBLE PRECISION NOT NULL, adiantamentos_A_Funcionarios DOUBLE PRECISION NOT NULL, tributos_A_Recuperar DOUBLE PRECISION NOT NULL, estoque_Diversos DOUBLE PRECISION NOT NULL, creditos_De_Coligadas_E_Controladas DOUBLE PRECISION NOT NULL, outros_Investimentos DOUBLE PRECISION NOT NULL, imoveis DOUBLE PRECISION NOT NULL, depreciacao_Amortizacao_Exaustao_Acumulada DOUBLE PRECISION NOT NULL, emprestimos DOUBLE PRECISION NOT NULL, financiamentos_Sistema_Financeiro_Nacional DOUBLE PRECISION NOT NULL, tributos_Retirados_A_Recolher DOUBLE PRECISION NOT NULL, obrigacoes_Com_O_Pessoal DOUBLE PRECISION NOT NULL, obrigacoes_Previdenciarias DOUBLE PRECISION NOT NULL, provisoes DOUBLE PRECISION NOT NULL, contas_A_Pagar DOUBLE PRECISION NOT NULL, contas_Correntes DOUBLE PRECISION NOT NULL, juros_Sobre_O_Capital_Proprio DOUBLE PRECISION NOT NULL, financiamentos DOUBLE PRECISION NOT NULL, impostos_E_Contribuicoes DOUBLE PRECISION NOT NULL, capital_Subscrito DOUBLE PRECISION NOT NULL, lucros_E_Projuizos_Acumulados DOUBLE PRECISION NOT NULL, lucros_E_Prejuizos_Exercicio DOUBLE PRECISION NOT NULL, fornecedores_Nacionais DOUBLE PRECISION NOT NULL, impostos_E_Contribuicoes_A_Recolher DOUBLE PRECISION NOT NULL, tributos_Retidos_A_Recolher DOUBLE PRECISION NOT NULL, Empresa_id INT DEFAULT NULL, INDEX IDX_B2FE07461D431A41 (Empresa_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Balanco_Patrimonial ADD CONSTRAINT FK_B2FE07461D431A41 FOREIGN KEY (Empresa_id) REFERENCES Empresa (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE Balanco_Patrimonial');
    }
}

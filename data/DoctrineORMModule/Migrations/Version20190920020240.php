<?php declare(strict_types=1);

namespace DoctrineORMModule\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190920020240 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Plano CHANGE Limite_De_Empresas Limite_De_Empresas VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE oauth_users DROP FOREIGN KEY FK_93804FF866E5D40B');
        $this->addSql('DROP INDEX IDX_93804FF866E5D40B ON oauth_users');
        $this->addSql('ALTER TABLE oauth_users DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE oauth_users ADD id INT AUTO_INCREMENT NOT NULL, DROP Contabilidade_id');
        $this->addSql('ALTER TABLE oauth_users ADD PRIMARY KEY (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Plano CHANGE Limite_De_Empresas Limite_De_Empresas INT NOT NULL');
        $this->addSql('ALTER TABLE oauth_users MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE oauth_users DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE oauth_users ADD Contabilidade_id INT DEFAULT NULL, DROP id');
        $this->addSql('ALTER TABLE oauth_users ADD CONSTRAINT FK_93804FF866E5D40B FOREIGN KEY (Contabilidade_id) REFERENCES Contabilidade (id)');
        $this->addSql('CREATE INDEX IDX_93804FF866E5D40B ON oauth_users (Contabilidade_id)');
        $this->addSql('ALTER TABLE oauth_users ADD PRIMARY KEY (username)');
    }
}

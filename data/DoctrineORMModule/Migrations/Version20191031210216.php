<?php declare(strict_types=1);

namespace DoctrineORMModule\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191031210216 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE LucroLiquidoAcumulado');
        $this->addSql('DROP TABLE LucroLiquidoMensal');
        $this->addSql('ALTER TABLE Treinamento CHANGE Status Status TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE LucroLiquidoAcumulado (id INT AUTO_INCREMENT NOT NULL, Empresa VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, Total DOUBLE PRECISION NOT NULL, Lucro_Prejuizo_Liquido DOUBLE PRECISION NOT NULL, Receitas_Operacionais DOUBLE PRECISION NOT NULL, Diferenca_Porcentagem DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE LucroLiquidoMensal (id INT AUTO_INCREMENT NOT NULL, Empresa VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, Mes VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, Ano VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, Lucro_Prejuizo_Liquido DOUBLE PRECISION NOT NULL, Receitas_Operacionais DOUBLE PRECISION NOT NULL, Diferenca_Porcentagem DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE Treinamento CHANGE Status Status VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
    }
}

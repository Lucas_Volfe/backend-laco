<?php declare(strict_types=1);

namespace DoctrineORMModule\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190904115238 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE upload_mensais ADD Receita_De_Vendas DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Receita_De_Fretes_E_Entregas DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Outras_Receitas_1 DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Outras_Receitas_2 DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Deducoes_Da_Receita_Bruta DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Impostos_Sobre_Vendas DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Comissoes_Sobre_Vendas DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Descontos_Incondicionais DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Devolucoes_De_Vendas DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Outras_Deducoes_1 DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Outras_Deducoes_2 DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Receita_Liquida_De_Vendas DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Custos_Operacionais DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Custo_Das_Mercadorias_Vendidas DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Custo_Dos_Produtos_Vendidos DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Custo_Dos_Servicos_Prestados DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Outros_Custos DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Lucro_Bruto DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Despesas_Operacionais DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Despesas_Com_Vendas DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Despesas_Administrativas DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Despesas_Tributarias_Gerais DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Outras_Despesas_1 DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Outras_Despesas_2 DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Outras_Despesas_3 DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Outras_Despesas_4 DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Lucro_Prejuizo_Operacional DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Receitas_E_Despesas_Financeiras DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Receitas_E_Rendimentos_Financeiros DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Despesas_Financeiras DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Outras_Receitas_Financeiras DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Outras_Despesas_Financeiras DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Outras_Receitas_E_Despesas_Nao_Operacionais DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Outras_Receitas_Nao_Operacionais DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Outras_Despesas_Nao_Operacionais DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Lucro_Prejuizo_Liquido DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Desembolso_Com_Investimentos_E_Emprestimos DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Investimentos_Em_Imobilizado DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Emprestimos_E_Dividas DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Outros_Investimentos_E_Emprestimos DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ADD Lucro_Prejuizo_Final DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE upload_mensais ALTER empresa TYPE INT');
        $this->addSql('ALTER TABLE upload_mensais ALTER empresa DROP DEFAULT');
        $this->addSql('ALTER TABLE upload_mensais ALTER data_upload SET NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Receita_De_Vendas');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Receita_De_Fretes_E_Entregas');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Outras_Receitas_1');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Outras_Receitas_2');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Deducoes_Da_Receita_Bruta');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Impostos_Sobre_Vendas');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Comissoes_Sobre_Vendas');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Descontos_Incondicionais');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Devolucoes_De_Vendas');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Outras_Deducoes_1');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Outras_Deducoes_2');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Receita_Liquida_De_Vendas');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Custos_Operacionais');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Custo_Das_Mercadorias_Vendidas');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Custo_Dos_Produtos_Vendidos');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Custo_Dos_Servicos_Prestados');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Outros_Custos');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Lucro_Bruto');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Despesas_Operacionais');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Despesas_Com_Vendas');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Despesas_Administrativas');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Despesas_Tributarias_Gerais');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Outras_Despesas_1');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Outras_Despesas_2');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Outras_Despesas_3');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Outras_Despesas_4');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Lucro_Prejuizo_Operacional');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Receitas_E_Despesas_Financeiras');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Receitas_E_Rendimentos_Financeiros');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Despesas_Financeiras');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Outras_Receitas_Financeiras');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Outras_Despesas_Financeiras');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Outras_Receitas_E_Despesas_Nao_Operacionais');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Outras_Receitas_Nao_Operacionais');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Outras_Despesas_Nao_Operacionais');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Lucro_Prejuizo_Liquido');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Desembolso_Com_Investimentos_E_Emprestimos');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Investimentos_Em_Imobilizado');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Emprestimos_E_Dividas');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Outros_Investimentos_E_Emprestimos');
        $this->addSql('ALTER TABLE Upload_Mensais DROP Lucro_Prejuizo_Final');
        $this->addSql('ALTER TABLE Upload_Mensais ALTER Data_Upload DROP NOT NULL');
        $this->addSql('ALTER TABLE Upload_Mensais ALTER Empresa TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE Upload_Mensais ALTER Empresa DROP DEFAULT');
    }
}

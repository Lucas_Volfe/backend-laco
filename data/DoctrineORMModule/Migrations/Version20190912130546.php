<?php declare(strict_types=1);

namespace DoctrineORMModule\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190912130546 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE acumulado (id INT AUTO_INCREMENT NOT NULL, Empresa VARCHAR(255) NOT NULL, Total DOUBLE PRECISION NOT NULL, Lucro_Prejuizo_Liquido DOUBLE PRECISION NOT NULL, Receitas_Operacionais DOUBLE PRECISION NOT NULL, Diferenca_Porcentagem DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lucros_Mensais (id INT AUTO_INCREMENT NOT NULL, Empresa VARCHAR(255) NOT NULL, Mes VARCHAR(255) NOT NULL, Ano VARCHAR(255) NOT NULL, Lucro_Prejuizo_Liquido DOUBLE PRECISION NOT NULL, Receitas_Operacionais DOUBLE PRECISION NOT NULL, Diferenca_Porcentagem DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Uploads_Mensais (id INT AUTO_INCREMENT NOT NULL, Ano VARCHAR(255) NOT NULL, Mes VARCHAR(255) NOT NULL, Data_Upload DATE NOT NULL, Empresa INT NOT NULL, Receitas_Operacionais VARCHAR(255) NOT NULL, Receita_De_Vendas DOUBLE PRECISION NOT NULL, Receita_De_Fretes_E_Entregas DOUBLE PRECISION NOT NULL, Outras_Receitas_1 DOUBLE PRECISION NOT NULL, Outras_Receitas_2 DOUBLE PRECISION NOT NULL, Deducoes_Da_Receita_Bruta DOUBLE PRECISION NOT NULL, Impostos_Sobre_Vendas DOUBLE PRECISION NOT NULL, Comissoes_Sobre_Vendas DOUBLE PRECISION NOT NULL, Descontos_Incondicionais DOUBLE PRECISION NOT NULL, Devolucoes_De_Vendas DOUBLE PRECISION NOT NULL, Outras_Deducoes_1 DOUBLE PRECISION NOT NULL, Outras_Deducoes_2 DOUBLE PRECISION NOT NULL, Receita_Liquida_De_Vendas DOUBLE PRECISION NOT NULL, Custos_Operacionais DOUBLE PRECISION NOT NULL, Custo_Das_Mercadorias_Vendidas DOUBLE PRECISION NOT NULL, Custo_Dos_Produtos_Vendidos DOUBLE PRECISION NOT NULL, Custo_Dos_Servicos_Prestados DOUBLE PRECISION NOT NULL, Outros_Custos DOUBLE PRECISION NOT NULL, Lucro_Bruto DOUBLE PRECISION NOT NULL, Despesas_Operacionais DOUBLE PRECISION NOT NULL, Despesas_Com_Vendas DOUBLE PRECISION NOT NULL, Despesas_Administrativas DOUBLE PRECISION NOT NULL, Despesas_Tributarias_Gerais DOUBLE PRECISION NOT NULL, Outras_Despesas_1 DOUBLE PRECISION NOT NULL, Outras_Despesas_2 DOUBLE PRECISION NOT NULL, Outras_Despesas_3 DOUBLE PRECISION NOT NULL, Outras_Despesas_4 DOUBLE PRECISION NOT NULL, Lucro_Prejuizo_Operacional DOUBLE PRECISION NOT NULL, Receitas_E_Despesas_Financeiras DOUBLE PRECISION NOT NULL, Receitas_E_Rendimentos_Financeiros DOUBLE PRECISION NOT NULL, Despesas_Financeiras DOUBLE PRECISION NOT NULL, Outras_Receitas_Financeiras DOUBLE PRECISION NOT NULL, Outras_Despesas_Financeiras DOUBLE PRECISION NOT NULL, Outras_Receitas_E_Despesas_Nao_Operacionais DOUBLE PRECISION NOT NULL, Outras_Receitas_Nao_Operacionais DOUBLE PRECISION NOT NULL, Outras_Despesas_Nao_Operacionais DOUBLE PRECISION NOT NULL, Lucro_Prejuizo_Liquido DOUBLE PRECISION NOT NULL, Desembolso_Com_Investimentos_E_Emprestimos DOUBLE PRECISION NOT NULL, Investimentos_Em_Imobilizado DOUBLE PRECISION NOT NULL, Emprestimos_E_Dividas DOUBLE PRECISION NOT NULL, Outros_Investimentos_E_Emprestimos DOUBLE PRECISION NOT NULL, Lucro_Prejuizo_Final DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Empresa (id INT AUTO_INCREMENT NOT NULL, NomeFantasia VARCHAR(255) NOT NULL, RazaoSocial VARCHAR(255) NOT NULL, Cnpj VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Contabilidade (id INT AUTO_INCREMENT NOT NULL, NomeFantasia VARCHAR(255) NOT NULL, RazaoSocial VARCHAR(255) NOT NULL, Cnpj VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE acumulado');
        $this->addSql('DROP TABLE lucros_Mensais');
        $this->addSql('DROP TABLE Uploads_Mensais');
        $this->addSql('DROP TABLE Empresa');
        $this->addSql('DROP TABLE Contabilidade');
    }
}

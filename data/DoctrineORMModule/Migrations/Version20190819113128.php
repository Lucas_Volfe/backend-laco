<?php declare(strict_types=1);

namespace DoctrineORMModule\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190819113128 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE meses ADD Deducoes_2 DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE meses ADD DespesasComVendas DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE meses ADD OutrasReceitas_3 DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE meses ADD OutrasReceitas_4 DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE meses ADD DespesasTributariasGerais DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE meses DROP despesascomerciais');
        $this->addSql('ALTER TABLE meses DROP despesasoperacionais');
        $this->addSql('ALTER TABLE meses DROP inssefgts');
        $this->addSql('ALTER TABLE meses DROP perdasestoque');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE Meses ADD despesascomerciais DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD despesasoperacionais DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD inssefgts DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD perdasestoque DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses DROP Deducoes_2');
        $this->addSql('ALTER TABLE Meses DROP DespesasComVendas');
        $this->addSql('ALTER TABLE Meses DROP OutrasReceitas_3');
        $this->addSql('ALTER TABLE Meses DROP OutrasReceitas_4');
        $this->addSql('ALTER TABLE Meses DROP DespesasTributariasGerais');
    }
}

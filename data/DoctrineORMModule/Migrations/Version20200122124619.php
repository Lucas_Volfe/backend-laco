<?php declare(strict_types=1);

namespace DoctrineORMModule\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200122124619 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Balanco_Patrimonial ADD disponivel DOUBLE PRECISION NOT NULL, ADD clientes DOUBLE PRECISION NOT NULL, ADD outros_Creditos DOUBLE PRECISION NOT NULL, ADD estoques DOUBLE PRECISION NOT NULL, ADD nao_Circulante_Ativo DOUBLE PRECISION NOT NULL, ADD realizavel_A_Longo_Prazo DOUBLE PRECISION NOT NULL, ADD outros_Creditos_Longo_Prazo DOUBLE PRECISION NOT NULL, ADD investimentos DOUBLE PRECISION NOT NULL, ADD imobilizado DOUBLE PRECISION NOT NULL, ADD bens_Em_Operacao DOUBLE PRECISION NOT NULL, ADD total_Do_Ativo DOUBLE PRECISION NOT NULL, ADD circulante_Passivo DOUBLE PRECISION NOT NULL, ADD instituições_Financeiras DOUBLE PRECISION NOT NULL, ADD fornecedores DOUBLE PRECISION NOT NULL, ADD obrigacoes_Tributarias_Circulante DOUBLE PRECISION NOT NULL, ADD obrigacoes_Trabalhistas_E_Prividenciarias DOUBLE PRECISION NOT NULL, ADD outras_Obrigacoes DOUBLE PRECISION NOT NULL, ADD dividendos_Participacoes_Juros_Capital_Proprio DOUBLE PRECISION NOT NULL, ADD nao_Circulante_Passivo DOUBLE PRECISION NOT NULL, ADD obrigacoes_A_Longo_Prazo DOUBLE PRECISION NOT NULL, ADD instituicoes_Financeiras DOUBLE PRECISION NOT NULL, ADD obrigacoes_Tributarias_Nao_Circulante DOUBLE PRECISION NOT NULL, ADD patrimonio_Liquido DOUBLE PRECISION NOT NULL, ADD capital_Social DOUBLE PRECISION NOT NULL, ADD lucros_E_Projuizos_Acumulados_Soma DOUBLE PRECISION NOT NULL, ADD total_Do_Patrimonio_Liquido_E_Passivo DOUBLE PRECISION NOT NULL, CHANGE tributos_retirados_a_recolher circulante_Ativo DOUBLE PRECISION NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Balanco_Patrimonial ADD tributos_Retirados_A_Recolher DOUBLE PRECISION NOT NULL, DROP circulante_Ativo, DROP disponivel, DROP clientes, DROP outros_Creditos, DROP estoques, DROP nao_Circulante_Ativo, DROP realizavel_A_Longo_Prazo, DROP outros_Creditos_Longo_Prazo, DROP investimentos, DROP imobilizado, DROP bens_Em_Operacao, DROP total_Do_Ativo, DROP circulante_Passivo, DROP instituições_Financeiras, DROP fornecedores, DROP obrigacoes_Tributarias_Circulante, DROP obrigacoes_Trabalhistas_E_Prividenciarias, DROP outras_Obrigacoes, DROP dividendos_Participacoes_Juros_Capital_Proprio, DROP nao_Circulante_Passivo, DROP obrigacoes_A_Longo_Prazo, DROP instituicoes_Financeiras, DROP obrigacoes_Tributarias_Nao_Circulante, DROP patrimonio_Liquido, DROP capital_Social, DROP lucros_E_Projuizos_Acumulados_Soma, DROP total_Do_Patrimonio_Liquido_E_Passivo');
    }
}

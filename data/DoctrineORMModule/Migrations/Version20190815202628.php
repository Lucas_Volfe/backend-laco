<?php declare(strict_types=1);

namespace DoctrineORMModule\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190815202628 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE Meses_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE Meses (id INT NOT NULL, ano VARCHAR(255) NOT NULL, mes VARCHAR(255) NOT NULL, ComissoesSobreVendas DOUBLE PRECISION NOT NULL, CustoDasVendasDeProdutos DOUBLE PRECISION NOT NULL, CustoDosProdutosVendidos DOUBLE PRECISION NOT NULL, CustoDosServicosPrestados DOUBLE PRECISION NOT NULL, CustosOperacionais DOUBLE PRECISION NOT NULL, DeducoesDaReceitaBruta DOUBLE PRECISION NOT NULL, Deducoes_1 DOUBLE PRECISION NOT NULL, DescontosIncondicionais DOUBLE PRECISION NOT NULL, DespesasAdministrativas DOUBLE PRECISION NOT NULL, DespesasComInvestimentosEmprestimos DOUBLE PRECISION NOT NULL, DespesasComerciais DOUBLE PRECISION NOT NULL, DespesasFinanceiras DOUBLE PRECISION NOT NULL, DespesasOperacionais DOUBLE PRECISION NOT NULL, DespesasOperacionais_somas DOUBLE PRECISION NOT NULL, DevolucoesDeVendas DOUBLE PRECISION NOT NULL, INSSeFGTS DOUBLE PRECISION NOT NULL, ImpostosSobreVendas DOUBLE PRECISION NOT NULL, InvestimentosEmImobilizado DOUBLE PRECISION NOT NULL, LucroBruto DOUBLE PRECISION NOT NULL, LucroPrejuizoLiquido DOUBLE PRECISION NOT NULL, LucroPrejuizoOperacional DOUBLE PRECISION NOT NULL, LucrosPrejuizoFinal DOUBLE PRECISION NOT NULL, OutrasDespesasNaoOperacionais DOUBLE PRECISION NOT NULL, OutrasDespesas_3 DOUBLE PRECISION NOT NULL, OutrasDespesas_4 DOUBLE PRECISION NOT NULL, OutrasReceitasDespesasNaoOperacionais DOUBLE PRECISION NOT NULL, OutrasReceitasNaoOperacionais DOUBLE PRECISION NOT NULL, OutrasReceitas_1 DOUBLE PRECISION NOT NULL, OutrasReceitas_2 DOUBLE PRECISION NOT NULL, OutrosCustos DOUBLE PRECISION NOT NULL, OutrosInvestimentosEmprestimos DOUBLE PRECISION NOT NULL, OutrosReceitasDespesasFinanceiras DOUBLE PRECISION NOT NULL, PerdasEstoque DOUBLE PRECISION NOT NULL, ReceitaDeFretesEntregas DOUBLE PRECISION NOT NULL, ReceitaDeVendas DOUBLE PRECISION NOT NULL, ReceitaLiquidaDeVendas DOUBLE PRECISION NOT NULL, ReceitasDespesasFinanceiras DOUBLE PRECISION NOT NULL, ReceitasOperacionais DOUBLE PRECISION NOT NULL, ReceitasRendimentosFinanceiros DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE Meses_id_seq CASCADE');
        $this->addSql('DROP TABLE Meses');
    }
}

<?php declare(strict_types=1);

namespace DoctrineORMModule\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190919194518 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE LucroLiquidoAcumulado (id INT AUTO_INCREMENT NOT NULL, Empresa VARCHAR(255) NOT NULL, Total DOUBLE PRECISION NOT NULL, Lucro_Prejuizo_Liquido DOUBLE PRECISION NOT NULL, Receitas_Operacionais DOUBLE PRECISION NOT NULL, Diferenca_Porcentagem DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE LucroLiquidoMensal (id INT AUTO_INCREMENT NOT NULL, Empresa VARCHAR(255) NOT NULL, Mes VARCHAR(255) NOT NULL, Ano VARCHAR(255) NOT NULL, Lucro_Prejuizo_Liquido DOUBLE PRECISION NOT NULL, Receitas_Operacionais DOUBLE PRECISION NOT NULL, Diferenca_Porcentagem DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Treinamento (id INT AUTO_INCREMENT NOT NULL, Titulo VARCHAR(255) NOT NULL, Status VARCHAR(255) NOT NULL, Descricao VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Plano (id INT AUTO_INCREMENT NOT NULL, Nome VARCHAR(255) NOT NULL, Descricao VARCHAR(255) NOT NULL, Limite_De_Empresas INT NOT NULL, Preco DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE oauth_scopes (scope VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, first_name VARCHAR(2000) DEFAULT NULL, client_id VARCHAR(80) DEFAULT NULL, is_default INT NOT NULL, PRIMARY KEY(scope)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE oauth_refresh_tokens (refresh_token VARCHAR(40) NOT NULL, client_id VARCHAR(80) NOT NULL, user_id VARCHAR(255) DEFAULT NULL, expires DATETIME NOT NULL, scope VARCHAR(2000) DEFAULT NULL, PRIMARY KEY(refresh_token)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE oauth_users (username VARCHAR(255) NOT NULL, password VARCHAR(2000) NOT NULL, first_name VARCHAR(255) DEFAULT NULL, last_name VARCHAR(255) DEFAULT NULL, email VARCHAR(255) NOT NULL, Contabilidade_id INT DEFAULT NULL, Empresa_id INT DEFAULT NULL, INDEX IDX_93804FF866E5D40B (Contabilidade_id), INDEX IDX_93804FF81D431A41 (Empresa_id), PRIMARY KEY(username)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE oauth_jwt (client_id VARCHAR(80) NOT NULL, subject VARCHAR(80) DEFAULT NULL, public_key VARCHAR(2000) DEFAULT NULL, PRIMARY KEY(client_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE oauth_authorization_codes (authorization_code VARCHAR(40) NOT NULL, client_id VARCHAR(80) NOT NULL, user_id VARCHAR(255) DEFAULT NULL, redirect_uri VARCHAR(2000) DEFAULT NULL, expires DATETIME NOT NULL, scope VARCHAR(2000) DEFAULT NULL, id_token VARCHAR(2000) DEFAULT NULL, PRIMARY KEY(authorization_code)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE oauth_access_tokens (access_token VARCHAR(40) NOT NULL, client_id VARCHAR(80) NOT NULL, user_id VARCHAR(255) NOT NULL, expires DATETIME DEFAULT NULL, scope VARCHAR(2000) DEFAULT NULL, PRIMARY KEY(access_token)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE oauth_clients (client_id VARCHAR(80) NOT NULL, client_secret VARCHAR(80) NOT NULL, redirect_uri VARCHAR(2000) NOT NULL, grant_types VARCHAR(80) DEFAULT NULL, scope VARCHAR(2000) DEFAULT NULL, user_id VARCHAR(255) DEFAULT NULL, PRIMARY KEY(client_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE oauth_users ADD CONSTRAINT FK_93804FF866E5D40B FOREIGN KEY (Contabilidade_id) REFERENCES Contabilidade (id)');
        $this->addSql('ALTER TABLE oauth_users ADD CONSTRAINT FK_93804FF81D431A41 FOREIGN KEY (Empresa_id) REFERENCES Empresa (id)');
        $this->addSql('DROP TABLE acumulado');
        $this->addSql('DROP TABLE lucros_Mensais');
        $this->addSql('ALTER TABLE Uploads_Mensais ADD Empresa_id INT DEFAULT NULL, DROP Empresa');
        $this->addSql('ALTER TABLE Uploads_Mensais ADD CONSTRAINT FK_5418943E1D431A41 FOREIGN KEY (Empresa_id) REFERENCES Empresa (id)');
        $this->addSql('CREATE INDEX IDX_5418943E1D431A41 ON Uploads_Mensais (Empresa_id)');
        $this->addSql('ALTER TABLE Empresa ADD Nome_Fantasia VARCHAR(255) NOT NULL, ADD Razao_Social VARCHAR(255) NOT NULL, ADD Contabilidade_id INT DEFAULT NULL, DROP NomeFantasia, DROP RazaoSocial');
        $this->addSql('ALTER TABLE Empresa ADD CONSTRAINT FK_776A63CC66E5D40B FOREIGN KEY (Contabilidade_id) REFERENCES Contabilidade (id)');
        $this->addSql('CREATE INDEX IDX_776A63CC66E5D40B ON Empresa (Contabilidade_id)');
        $this->addSql('ALTER TABLE Contabilidade ADD Nome_Fantasia VARCHAR(255) NOT NULL, ADD Razao_Social VARCHAR(255) NOT NULL, ADD Plano_id INT DEFAULT NULL, DROP NomeFantasia, DROP RazaoSocial');
        $this->addSql('ALTER TABLE Contabilidade ADD CONSTRAINT FK_4501CB7F63FDE49A FOREIGN KEY (Plano_id) REFERENCES Plano (id)');
        $this->addSql('CREATE INDEX IDX_4501CB7F63FDE49A ON Contabilidade (Plano_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Contabilidade DROP FOREIGN KEY FK_4501CB7F63FDE49A');
        $this->addSql('CREATE TABLE acumulado (id INT AUTO_INCREMENT NOT NULL, Empresa VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, Total DOUBLE PRECISION NOT NULL, Lucro_Prejuizo_Liquido DOUBLE PRECISION NOT NULL, Receitas_Operacionais DOUBLE PRECISION NOT NULL, Diferenca_Porcentagem DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE lucros_Mensais (id INT AUTO_INCREMENT NOT NULL, Empresa VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, Mes VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, Ano VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, Lucro_Prejuizo_Liquido DOUBLE PRECISION NOT NULL, Receitas_Operacionais DOUBLE PRECISION NOT NULL, Diferenca_Porcentagem DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE LucroLiquidoAcumulado');
        $this->addSql('DROP TABLE LucroLiquidoMensal');
        $this->addSql('DROP TABLE Treinamento');
        $this->addSql('DROP TABLE Plano');
        $this->addSql('DROP TABLE oauth_scopes');
        $this->addSql('DROP TABLE oauth_refresh_tokens');
        $this->addSql('DROP TABLE oauth_users');
        $this->addSql('DROP TABLE oauth_jwt');
        $this->addSql('DROP TABLE oauth_authorization_codes');
        $this->addSql('DROP TABLE oauth_access_tokens');
        $this->addSql('DROP TABLE oauth_clients');
        $this->addSql('DROP INDEX IDX_4501CB7F63FDE49A ON Contabilidade');
        $this->addSql('ALTER TABLE Contabilidade ADD NomeFantasia VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD RazaoSocial VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP Nome_Fantasia, DROP Razao_Social, DROP Plano_id');
        $this->addSql('ALTER TABLE Empresa DROP FOREIGN KEY FK_776A63CC66E5D40B');
        $this->addSql('DROP INDEX IDX_776A63CC66E5D40B ON Empresa');
        $this->addSql('ALTER TABLE Empresa ADD NomeFantasia VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD RazaoSocial VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP Nome_Fantasia, DROP Razao_Social, DROP Contabilidade_id');
        $this->addSql('ALTER TABLE Uploads_Mensais DROP FOREIGN KEY FK_5418943E1D431A41');
        $this->addSql('DROP INDEX IDX_5418943E1D431A41 ON Uploads_Mensais');
        $this->addSql('ALTER TABLE Uploads_Mensais ADD Empresa INT NOT NULL, DROP Empresa_id');
    }
}

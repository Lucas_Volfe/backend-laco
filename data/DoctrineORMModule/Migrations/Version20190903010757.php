<?php declare(strict_types=1);

namespace DoctrineORMModule\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190903010757 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE meses ADD OutrasDespesasFinanceiras DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses RENAME COLUMN outrosreceitasedespesasfinanceiras TO OutrasReceitasFinanceiras');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE Meses ADD outrosreceitasedespesasfinanceiras DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE Meses DROP OutrasReceitasFinanceiras');
        $this->addSql('ALTER TABLE Meses DROP OutrasDespesasFinanceiras');
    }
}

<?php declare(strict_types=1);

namespace DoctrineORMModule\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190828015414 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA oauth');
        $this->addSql('CREATE SEQUENCE Empresa_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE Contabilidade_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE Empresa (id INT NOT NULL, NomeFantasia VARCHAR(255) NOT NULL, RazaoSocial VARCHAR(255) NOT NULL, Cnpj VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE Contabilidade (id INT NOT NULL, NomeFantasia VARCHAR(255) NOT NULL, RazaoSocial VARCHAR(255) NOT NULL, Cnpj VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE oauth.oauth_access_tokens (access_token VARCHAR(40) NOT NULL, client_id VARCHAR(80) NOT NULL, user_id VARCHAR(255) NOT NULL, expires TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, scope VARCHAR(2000) DEFAULT NULL, PRIMARY KEY(access_token))');
        $this->addSql('CREATE TABLE oauth.oauth_refresh_tokens (refresh_token VARCHAR(40) NOT NULL, client_id VARCHAR(80) NOT NULL, user_id VARCHAR(255) DEFAULT NULL, expires TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, scope VARCHAR(2000) DEFAULT NULL, PRIMARY KEY(refresh_token))');
        $this->addSql('CREATE TABLE oauth.oauth_jwt (client_id VARCHAR(80) NOT NULL, subject VARCHAR(80) DEFAULT NULL, public_key VARCHAR(2000) DEFAULT NULL, PRIMARY KEY(client_id))');
        $this->addSql('CREATE TABLE oauth.oauth_scopes (scope VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, first_name VARCHAR(2000) DEFAULT NULL, client_id VARCHAR(80) DEFAULT NULL, is_default INT NOT NULL, PRIMARY KEY(scope))');
        $this->addSql('CREATE TABLE oauth.oauth_users (username VARCHAR(255) NOT NULL, password VARCHAR(2000) NOT NULL, first_name VARCHAR(255) DEFAULT NULL, last_name VARCHAR(255) DEFAULT NULL, email VARCHAR(255) NOT NULL, PRIMARY KEY(username))');
        $this->addSql('CREATE TABLE oauth.oauth_clients (client_id VARCHAR(80) NOT NULL, client_secret VARCHAR(80) NOT NULL, redirect_uri VARCHAR(2000) NOT NULL, grant_types VARCHAR(80) DEFAULT NULL, scope VARCHAR(2000) DEFAULT NULL, user_id VARCHAR(255) DEFAULT NULL, PRIMARY KEY(client_id))');
        $this->addSql('CREATE TABLE oauth.oauth_authorization_codes (authorization_code VARCHAR(40) NOT NULL, client_id VARCHAR(80) NOT NULL, user_id VARCHAR(255) DEFAULT NULL, redirect_uri VARCHAR(2000) DEFAULT NULL, expires TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, scope VARCHAR(2000) DEFAULT NULL, id_token VARCHAR(2000) DEFAULT NULL, PRIMARY KEY(authorization_code))');
        $this->addSql('DROP TABLE oauth_access_tokens');
        $this->addSql('DROP TABLE oauth_authorization_codes');
        $this->addSql('DROP TABLE oauth_clients');
        $this->addSql('DROP TABLE oauth_jwt');
        $this->addSql('DROP TABLE oauth_refresh_tokens');
        $this->addSql('DROP TABLE oauth_scopes');
        $this->addSql('DROP TABLE oauth_users');
        $this->addSql('ALTER TABLE meses ADD ReceitaDeFretesEEntregas DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses ADD OutrasReceitas1 DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses ADD OutrasReceitas2 DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses ADD OutrasDeducoes1 DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses ADD OutrasDeducoes2 DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses ADD CustoDasMercadoriasVendidas DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses ADD DespesasOperacionais DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses ADD OutrasDespesas1 DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses ADD OutrasDespesas2 DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses ADD OutrasDespesas3 DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses ADD OutrasDespesas4 DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses ADD ReceitasEDespesasFinanceiras DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses ADD ReceitasERendimentosFinanceiros DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses ADD OutrosReceitasEDespesasFinanceiras DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses ADD OutrasReceitasEDespesasNaoOperacionais DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses ADD DesembolsoComInvestimentosEEmprestimos DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses ADD EmprestimosEDividas DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses ADD OutrosInvestimentosEEmprestimos DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses ADD LucroPrejuizoFinal DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meses DROP custodasvendasdeprodutos');
        $this->addSql('ALTER TABLE meses DROP deducoes_1');
        $this->addSql('ALTER TABLE meses DROP despesascominvestimentosemprestimos');
        $this->addSql('ALTER TABLE meses DROP despesasoperacionais_somas');
        $this->addSql('ALTER TABLE meses DROP lucrosprejuizofinal');
        $this->addSql('ALTER TABLE meses DROP outrasdespesas_3');
        $this->addSql('ALTER TABLE meses DROP outrasdespesas_4');
        $this->addSql('ALTER TABLE meses DROP outrasreceitasdespesasnaooperacionais');
        $this->addSql('ALTER TABLE meses DROP outrasreceitas_1');
        $this->addSql('ALTER TABLE meses DROP outrasreceitas_2');
        $this->addSql('ALTER TABLE meses DROP outrosinvestimentosemprestimos');
        $this->addSql('ALTER TABLE meses DROP outrosreceitasdespesasfinanceiras');
        $this->addSql('ALTER TABLE meses DROP receitadefretesentregas');
        $this->addSql('ALTER TABLE meses DROP receitasdespesasfinanceiras');
        $this->addSql('ALTER TABLE meses DROP receitasrendimentosfinanceiros');
        $this->addSql('ALTER TABLE meses DROP deducoes_2');
        $this->addSql('ALTER TABLE meses DROP outrasreceitas_3');
        $this->addSql('ALTER TABLE meses DROP outrasreceitas_4');
        $this->addSql('ALTER TABLE meses ALTER comissoessobrevendas DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER custodosprodutosvendidos DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER custodosservicosprestados DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER custosoperacionais DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER deducoesdareceitabruta DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER descontosincondicionais DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER despesasadministrativas DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER despesasfinanceiras DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER devolucoesdevendas DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER impostossobrevendas DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER investimentosemimobilizado DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER lucrobruto DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER lucroprejuizoliquido DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER lucroprejuizooperacional DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER outrasdespesasnaooperacionais DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER outrasreceitasnaooperacionais DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER outroscustos DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER receitadevendas DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER receitaliquidadevendas DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER receitasoperacionais TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE meses ALTER receitasoperacionais DROP DEFAULT');
        $this->addSql('ALTER TABLE meses ALTER despesascomvendas DROP NOT NULL');
        $this->addSql('ALTER TABLE meses ALTER despesastributariasgerais DROP NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE Empresa_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE Contabilidade_id_seq CASCADE');
        $this->addSql('CREATE TABLE oauth_access_tokens (access_token VARCHAR(40) NOT NULL, client_id VARCHAR(80) NOT NULL, user_id VARCHAR(255) DEFAULT NULL, expires TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, scope VARCHAR(2000) DEFAULT NULL, PRIMARY KEY(access_token))');
        $this->addSql('CREATE TABLE oauth_authorization_codes (authorization_code VARCHAR(40) NOT NULL, client_id VARCHAR(80) NOT NULL, user_id VARCHAR(255) DEFAULT NULL, redirect_uri VARCHAR(2000) DEFAULT NULL, expires TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, scope VARCHAR(2000) DEFAULT NULL, id_token VARCHAR(2000) DEFAULT NULL, PRIMARY KEY(authorization_code))');
        $this->addSql('CREATE TABLE oauth_clients (client_id VARCHAR(80) NOT NULL, client_secret VARCHAR(80) NOT NULL, redirect_uri VARCHAR(2000) NOT NULL, grant_types VARCHAR(80) DEFAULT NULL, scope VARCHAR(2000) DEFAULT NULL, user_id VARCHAR(255) DEFAULT NULL, PRIMARY KEY(client_id))');
        $this->addSql('CREATE TABLE oauth_jwt (client_id VARCHAR(80) NOT NULL, subject VARCHAR(80) DEFAULT NULL, public_key VARCHAR(2000) DEFAULT NULL, PRIMARY KEY(client_id))');
        $this->addSql('CREATE TABLE oauth_refresh_tokens (refresh_token VARCHAR(40) NOT NULL, client_id VARCHAR(80) NOT NULL, user_id VARCHAR(255) DEFAULT NULL, expires TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, scope VARCHAR(2000) DEFAULT NULL, PRIMARY KEY(refresh_token))');
        $this->addSql('CREATE TABLE oauth_scopes (type VARCHAR(255) DEFAULT \'supported\' NOT NULL, scope VARCHAR(2000) DEFAULT NULL, client_id VARCHAR(80) DEFAULT NULL, is_default SMALLINT DEFAULT NULL)');
        $this->addSql('CREATE TABLE oauth_users (username VARCHAR(255) NOT NULL, password VARCHAR(2000) DEFAULT NULL, first_name VARCHAR(255) DEFAULT NULL, last_name VARCHAR(255) DEFAULT NULL, PRIMARY KEY(username))');
        $this->addSql('DROP TABLE Empresa');
        $this->addSql('DROP TABLE Contabilidade');
        $this->addSql('DROP TABLE oauth.oauth_access_tokens');
        $this->addSql('DROP TABLE oauth.oauth_refresh_tokens');
        $this->addSql('DROP TABLE oauth.oauth_jwt');
        $this->addSql('DROP TABLE oauth.oauth_scopes');
        $this->addSql('DROP TABLE oauth.oauth_users');
        $this->addSql('DROP TABLE oauth.oauth_clients');
        $this->addSql('DROP TABLE oauth.oauth_authorization_codes');
        $this->addSql('ALTER TABLE Meses ADD custodasvendasdeprodutos DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD deducoes_1 DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD despesascominvestimentosemprestimos DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD despesasoperacionais_somas DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD lucrosprejuizofinal DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD outrasdespesas_3 DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD outrasdespesas_4 DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD outrasreceitasdespesasnaooperacionais DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD outrasreceitas_1 DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD outrasreceitas_2 DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD outrosinvestimentosemprestimos DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD outrosreceitasdespesasfinanceiras DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD receitadefretesentregas DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD receitasdespesasfinanceiras DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD receitasrendimentosfinanceiros DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD deducoes_2 DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD outrasreceitas_3 DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses ADD outrasreceitas_4 DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE Meses DROP ReceitaDeFretesEEntregas');
        $this->addSql('ALTER TABLE Meses DROP OutrasReceitas1');
        $this->addSql('ALTER TABLE Meses DROP OutrasReceitas2');
        $this->addSql('ALTER TABLE Meses DROP OutrasDeducoes1');
        $this->addSql('ALTER TABLE Meses DROP OutrasDeducoes2');
        $this->addSql('ALTER TABLE Meses DROP CustoDasMercadoriasVendidas');
        $this->addSql('ALTER TABLE Meses DROP DespesasOperacionais');
        $this->addSql('ALTER TABLE Meses DROP OutrasDespesas1');
        $this->addSql('ALTER TABLE Meses DROP OutrasDespesas2');
        $this->addSql('ALTER TABLE Meses DROP OutrasDespesas3');
        $this->addSql('ALTER TABLE Meses DROP OutrasDespesas4');
        $this->addSql('ALTER TABLE Meses DROP ReceitasEDespesasFinanceiras');
        $this->addSql('ALTER TABLE Meses DROP ReceitasERendimentosFinanceiros');
        $this->addSql('ALTER TABLE Meses DROP OutrosReceitasEDespesasFinanceiras');
        $this->addSql('ALTER TABLE Meses DROP OutrasReceitasEDespesasNaoOperacionais');
        $this->addSql('ALTER TABLE Meses DROP DesembolsoComInvestimentosEEmprestimos');
        $this->addSql('ALTER TABLE Meses DROP EmprestimosEDividas');
        $this->addSql('ALTER TABLE Meses DROP OutrosInvestimentosEEmprestimos');
        $this->addSql('ALTER TABLE Meses DROP LucroPrejuizoFinal');
        $this->addSql('ALTER TABLE Meses ALTER ReceitasOperacionais TYPE DOUBLE PRECISION');
        $this->addSql('ALTER TABLE Meses ALTER ReceitasOperacionais DROP DEFAULT');
        $this->addSql('ALTER TABLE Meses ALTER ReceitaDeVendas SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER DeducoesDaReceitaBruta SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER ImpostosSobreVendas SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER ComissoesSobreVendas SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER DescontosIncondicionais SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER DevolucoesDeVendas SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER ReceitaLiquidaDeVendas SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER CustosOperacionais SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER CustoDosProdutosVendidos SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER CustoDosServicosPrestados SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER OutrosCustos SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER LucroBruto SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER DespesasComVendas SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER DespesasAdministrativas SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER DespesasTributariasGerais SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER LucroPrejuizoOperacional SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER DespesasFinanceiras SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER OutrasReceitasNaoOperacionais SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER OutrasDespesasNaoOperacionais SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER LucroPrejuizoLiquido SET NOT NULL');
        $this->addSql('ALTER TABLE Meses ALTER InvestimentosEmImobilizado SET NOT NULL');
    }
}

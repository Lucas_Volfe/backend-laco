<?php declare(strict_types=1);

namespace DoctrineORMModule\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190924025414 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Logs CHANGE Original Original VARCHAR(21844) NOT NULL, CHANGE Alteracao Alteracao VARCHAR(21844) NOT NULL, CHANGE Tipo_Log Tipo_Log VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Logs CHANGE Original Original VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE Alteracao Alteracao VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE Tipo_Log Tipo_Log INT NOT NULL');
    }
}
